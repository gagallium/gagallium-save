<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0"><channel><title>Gagallium: type systems</title><link>http://gallium.inria.fr/~scherer/gagallium/kw_type+systems.html</link><description>A draft blog at Gallium</description><managingEditor>gabriel.scherer@inria.fr</managingEditor><pubDate>14 Sep 2012 08:00:00 +0000</pubDate><lastBuildDate>14 Sep 2012 08:00:00 +0000</lastBuildDate><generator>Stog</generator><item><title>ICFP 2012, Sunday: the HOPE Workshop</title><link>http://gallium.inria.fr/~scherer/gagallium/icfp-sunday/index.html</link><description>&lt;p&gt;This is a continuation of highlights from ICFP. I won't cover all
days, and it's not in chronological order. On Sunday, I attended the
HOPE workshop, Higher-Order Programming with Effects; I would very
much have liked to attend LFMTP and WGP as well, but you can only be
in one place... Here are a few highlights of the talks.&lt;/p&gt;




&lt;h1&gt;Handlers in Action, by Ohad Kammar&lt;/h1&gt;

&lt;p&gt;(Joint work with Sam Lindley and Nicolas Oury)&lt;/p&gt;

&lt;p&gt;Ohad presented their work on using the &amp;quot;effect handlers&amp;quot; approach to
programming with effects, introduced by the Eff language of Andrej
Bauer and Matija Pretnar, and also related to the Frank experiment by
Conor McBride.&lt;/p&gt;

&lt;p&gt;A rough idea of an &amp;quot;effects handler&amp;quot; for the working OCamler is that
it looks like a try..with block, but in stead of handling only
exceptions in the &amp;quot;with&amp;quot; part, it handles all sort of effects that may
be &amp;quot;raised&amp;quot; by the inner code: asking to read a reference, asking
a random number, etc. The handler's task is to describe what this
effect should actually do, given a (delimited) continuation allowing
to continue the program at the point the effect was raised. &lt;/p&gt;

&lt;p&gt;The programmer is therefore free to define its own effects locally,
override the behavior of surrounding effects, and decide in which
order the effects are performed by layering different handlers.&lt;/p&gt;

&lt;p&gt;Their talk gave an operational semantics for this effect handler
system. An operational semantics is a way to define the execution of
a program as a series of reduction of atomic &amp;quot;execution steps&amp;quot;, that
helps a lot in thinking about the language, and writing programs.&lt;/p&gt;

&lt;p&gt;This approach competes with monads as the main tool to write effectful
programs in an otherwise effect-free base language. They were careful
to note that there is no consensus yet on which way is more
convenient; they developped libraries to write code in an
effect-handler style in several languages (Haskell, OCaml, SML,
Racket...) and encouraged the audience to try them.&lt;/p&gt;

&lt;p&gt;Unfortunately, I haven't found slides on the web yet (they were
quite good), but you can have a look at &lt;a href=&quot;http://homepages.inf.ed.ac.uk/slindley/papers/handlers.pdf&quot;&gt;this 12-pages
draft&lt;/a&gt;
covering this research.&lt;/p&gt;

&lt;h1&gt;Koka, by Dan Leijen&lt;/h1&gt;

&lt;p&gt;I already knew of Dan Leijen as he has worked on a lot of different
topics that are highly related to what we've been doing in Gallium:
extensible records with row types, MLF-inspired type inference, and
more recently &amp;quot;simple enough&amp;quot; effect systems. I was eager to see his
presentation (and meet him), and it did not disappoint.&lt;/p&gt;

&lt;p&gt;Dan's goal is to define an effect system that is not too complex to
use. His aim is to have effect inference, and generally be as
transparent as possible to the user that should program like in any
ML, and yet at the same time track at the type level the possibility
of raising exceptions, accessing mutable state or even being
non-terminating.&lt;/p&gt;

&lt;p&gt;The talk demonstrated that after a few tries, he settled on a system
based on row polymorphism (as the theoretical underpinnings of the
OCaml object system), and he indeed showed some examples that were
both expressive and easy to infer.&lt;/p&gt;

&lt;p&gt;Dan is extremely humble and he kept presenting his work as something
simple and almost too naive. However, he actually knows to use
advanced type systems idea quite well; he uses a very nice technique
to prevent non-termination from being unnoticed when mixing higher-order
functions and mutable-state, that relies on qualified types
(the theoretical underpinnings of type classes). His system is also
able to check that local use of mutation are observationally
effect-free, à la ST monad, but without any explicit annotation from
the programmer.&lt;/p&gt;

&lt;p&gt;He has &lt;a href=&quot;http://rise4fun.com/Koka&quot;&gt;a website&lt;/a&gt; with
a compilation-to-javascript to allow you to experiment live. Try it!&lt;/p&gt;

&lt;h1&gt;Logical Relations for Fine-Grained Concurrency, by Aaron Turon&lt;/h1&gt;

&lt;p&gt;(Joint work with Jacomb Thamsborg, Amal Ahmed, Lars Birkedal and Derek Dreyer)&lt;/p&gt;

&lt;p&gt;I've been impressed at OPLSS by the excellent lectures that Amal Ahmed
gave on logical relations, and was hoping to get more of this
goodness. This talk, however, was explicitly not focusing on this
aspect of the presented work, and rather discussing the concurrency
problems they studied and the techniques they developped.&lt;/p&gt;

&lt;p&gt;The rough idea of the work is to specify a complex concurrent
algorithm &lt;em&gt;implementation&lt;/em&gt; (using fine-grained locking,
compare-and-swap or what have you), not by giving a (possibly brittle)
logic &lt;em&gt;specification&lt;/em&gt; of its working, but by comparing it to another
simpler &lt;em&gt;implementation&lt;/em&gt;. If you prove that the complex and efficient
implementation simulates the simple one (using for example a simple
lock on the whole structure), you get a simple idea of what it does.&lt;/p&gt;

&lt;p&gt;This idea of specifying program by other, simpler programs
(reference implementations) pops up in a lot of different place and
I like it very much. One advantage of the approach is that it is
a form of specification that can easily be &amp;quot;executed&amp;quot; to check, on
a concrete example, that it really specifies what you mean it
does. With formally verified software, the bugs come not from the code
itself, but from the specification that may not say what you think it
says. Having a readable and testable specification is the therefore the
most important part for trusting the software.&lt;/p&gt;

&lt;p&gt;Aaron was an excellent speaker and he clearly explained the issues
with the fine-graind algorithm they were working with. He presented
some of the proof techniques they used to check algorithm for the
litterature, some of them quite simply, some of them requirement the
use of all their different proof techniques at the same time.&lt;/p&gt;

&lt;p&gt;Again, I didn't find the slides online, but there is &lt;a href=&quot;http://www.ccs.neu.edu/home/turon/relcon/relcon.pdf&quot;&gt;a 12-pages
paper&lt;/a&gt; for a more
formal view of the topic.&lt;/p&gt;
</description><pubDate>14 Sep 2012 08:00:00 +0000</pubDate><category>research</category><category>conference</category><category>icfp</category><category>type systems</category><category>row types</category><category>effect handlers</category><category>logical relations</category><guid isPermaLink="true">http://gallium.inria.fr/~scherer/gagallium/icfp-sunday/index.html</guid></item><item><title>ICFP 2012, Monday</title><link>http://gallium.inria.fr/~scherer/gagallium/icfp-monday/index.html</link><description>&lt;p&gt;I am in Copenhagen for
&lt;a href=&quot;http://icfpconference.org/icfp2012/index.html&quot;&gt;ICFP&lt;/a&gt;, in very good
company (including a large part of Gallium). There is a lot to do and
just listening to talks and speaking to people takes a large amount of
time and energy.&lt;/p&gt;




&lt;p&gt;On Sunday, I attended the HOPE workshop, Higher-Order Programming with
Effects; I would very much have liked to attend LFMTP and WGP as well,
but you can only be in one place... (I hope to also write a bit about
the talks at HOPE, but I need to first see if the authors have their
slides online...)&lt;/p&gt;

&lt;p&gt;Monday was the beginning of the main conference. After an explosive
keynote by Conor, we had a lot of great talks. I originally planned to
skip one of the sessions to go to the swimming-pool and preserve some
of my mental sanity for the next few days, but couldn't come to doing
it because it all looked too tempting. Here are a few highlights.&lt;/p&gt;

&lt;p&gt;(I also had great experiences with Danish food, in particular the
sweet marinated herring. At lunch I had a great discussion with
Jonathan and Scott Kilpatrick, a student of Derek Dreyer that is
working on formal approches of Haskell package management inspired
from ML module systems.)&lt;/p&gt;

&lt;h1&gt;Agda-curious? An Exploration of Programming with Dependent Types,&lt;/h1&gt;

&lt;p&gt;keynote by Conor McBride&lt;/p&gt;

&lt;p&gt;Conor made a dramatic keynote as he does well. It was fun, it was
interesting, it was surprisingly practical and it all worked well. He
wrote a compiler from a simple arithmetic expression language to
a simple stack language in Agda, and then shown how he could gradually
refine the type of the stack items to bake in correctness arguments:
that the size of the stack is respected (evaluating the result of
compiling an expression only adds one element on the stack), that the
type of the stack items are correct, and finally that compilation
preserves the expression's semantics.&lt;/p&gt;

&lt;p&gt;I think what was most impressive to the part of the audience not
familiar with Agda was the demonstration of its interactive
programming mode, all in Emacs glory, with hole-filling and
single-keystroke-dependent-case-splitting. This strongly support
Conor's vision that we should use types to help us &lt;em&gt;write&lt;/em&gt; programs,
not just debug or maintain them. I regretted the absence of his
typical colorful hand-drawn overlays, but admittedly proof assistants
don't run very well on pen&amp;amp;plastic.&lt;/p&gt;

&lt;p&gt;Conor also made a running comment on an opposition in the proof
assistants community between people that advocated writing programs
with simple types (eg. corresponding to ML or System F), and
separately proving properties about them, and people that advocate
writing richly, dependently typed programs that carry their own
correctness proofs. He believes in &amp;quot;a mixed economy&amp;quot; where everyone is
free to choose a point in the continuum between those two opposites,
and suggests that we should actively support both styles.&lt;/p&gt;

&lt;h1&gt;Verified Heap Theorem Prover with Paramodulation, by Gordon Stewart&lt;/h1&gt;

&lt;p&gt;(Join work with Lennart Beringer and Andrew Appel)&lt;/p&gt;

&lt;p&gt;The talk was very strong, with both crisp ideas of the problem domain
(which I don't know much about) and well-done, easy to assess empirical
results. In short, they wrote an automated prover for separation logic
statement similar to an existing solver, but their one is entirely
written in Coq, with of course a correctness proof.&lt;/p&gt;

&lt;p&gt;In the SAT/SMT community, the histories of &amp;quot;we took this paper and
implemented in Coq&amp;quot; are often not that good: a lot of the performances
come from low-level hack that are hard to compete with in a sane
language. Their story here is much better, as they are competitive
with existing, specialized solvers, thanks to the use of
a state-of-the-art algorithm and nice work on profiling and optimizing
the Coq program -- executed through extraction to OCaml.&lt;/p&gt;

&lt;p&gt;The small detail : they presented a range of optimizations they did on
the code (from micro-optimizations like extraction to OCaml's int31
native integers to high-level algorithmic changes) and for each gave
not only the runtime speedup, but also the blowup in code size
resulting from the change. They ended up 22 times faster than their
prototype, but also with 5 times more code. I liked having both
numbers and think we should do that more often.&lt;/p&gt;

&lt;h1&gt;Superficially Substrucural logic, by Neelakantan Krishnaswami&lt;/h1&gt;

&lt;p&gt;(Joint work with Aaron Turon, Derek Dreyer and Deepak Garg)&lt;/p&gt;

&lt;p&gt;This works build upon the ideas of separation logic, a reasoning
technique for imperative program that allows the programmer to reason
about fragments in isolation, and easily compose two pieces that
affect distinct fragments. In the last few years, people have worked
on generalizing this idea to other kind of resources, not just the
memory heap but also possibly output files, key/value pairs in
a shared hash table, segments of an array shared between workers,
etc. The idea is that any monoid, in a linear type theory, can be
reasoned over as a separated resource.&lt;/p&gt;

&lt;p&gt;&lt;a href=&quot;http://www.cs.cmu.edu/~neelk/&quot;&gt;Neelk&lt;/a&gt; gave a clear presentation of
their work, which allows users to define their own resources in their
program and reason on all kind of resources at once -- instead of
having one distinct logic for each form of resource. It uses features
from the typed programmer toolset such as existential types to make
this construction possible in programs.&lt;/p&gt;

&lt;h1&gt;Operational Semantics Using the Partiality Monad, by Nils Anders Danielsson&lt;/h1&gt;

&lt;p&gt;Nils gave a well-executed talk on using a &amp;quot;Partiality Monad&amp;quot; to
represent and reason about non-terminating programs in Agda. He
started from a &amp;quot;denotational interpreter&amp;quot; for the untyped
lambda-calculus, with a naive non-typable version, and then a simple
modification of it to capture both type failure at application and the
potential for non-termination. He then went on to show how we could
state properties of the evaluator and derived implementations
(an abstract machine counterpart of it) in a natural way that still
captured all we want to say.&lt;/p&gt;

&lt;p&gt;For example, you don't want to only say that &amp;quot;if my lambda-term
terminates with a value, the compiled abstract bytecode terminates
with the same value&amp;quot;: you probably want to capture as well that
non-termination of the lambda-term should incur non-termination of the
abstract machine counterpart. Nils manages to capture both those ideas
in single, natural statement.&lt;/p&gt;

&lt;p&gt;The talk was very well executed but appealed most, I suppose, to
people familiar with those issues of stating properties about impure
programs -- a problematic that is very important in some of the work
on Compcert, for example.&lt;/p&gt;
</description><pubDate>11 Sep 2012 08:00:00 +0000</pubDate><category>research</category><category>conference</category><category>icfp</category><category>type systems</category><category>non-termination</category><guid isPermaLink="true">http://gallium.inria.fr/~scherer/gagallium/icfp-monday/index.html</guid></item></channel></rss>
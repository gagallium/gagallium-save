<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0"><channel><title>Gagallium: strings</title><link>http://gallium.inria.fr/~scherer/gagallium/kw_strings.html</link><description>A draft blog at Gallium</description><managingEditor>gabriel.scherer@inria.fr</managingEditor><pubDate>12 Jul 2012 08:00:00 +0000</pubDate><lastBuildDate>12 Jul 2012 08:00:00 +0000</lastBuildDate><generator>Stog</generator><item><title>HeVeA : a new Rope</title><link>http://gallium.inria.fr/~scherer/gagallium/hevea-a-new-rope/index.html</link><description>&lt;p&gt;I recently started an internship in Gallium, working on the HeVeA
LaTeX to HTML compiler from and with Luc Maranget. I'll stay here for
two months, which is sufficient for having to &amp;quot;blog about it&amp;quot;. In my
civil life, I'm interested in about approximately everything
concerning language(-related) theory.&lt;/p&gt;

&lt;p&gt;My first job for HeVeA was changing the structure used by the Out
module. It was yielding the ouput by concatenating together bare
strings, thus blowing up the maximal size of strings on 32 bits
machines. A data structure fit for concatenations was therefore
needed, and a good choice for it is Ropes.&lt;/p&gt;




&lt;h1&gt;Ropes&lt;/h1&gt;

&lt;p&gt;Basically, ropes are just balanced binary-trees, the leaves being
strings. Each node is also weighted for example by the length of the
rope which it is the root of (it could also be by the length of its left
child, which makes no difference but administrative ones). The leaves
are designed to be small enough to see the difference, and big enough
to avoid too deep trees (which could have a negative effect on
performances). Typically, the maximum length of leaves is 256, and the
height is only limited by integer size in OCaml.&lt;/p&gt;

&lt;p&gt;A rope for the string &lt;code&gt;&amp;quot;Gagallium&amp;quot;&lt;/code&gt; could be :&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;      9
    __|__
   /     \
  5       4
 / \     / \
Ga gal  li um
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;On this data structure, we win on complexity against bare strings for
most costly operations, while losing with &lt;em&gt;O(log n)&lt;/em&gt; vs. &lt;em&gt;O(1)&lt;/em&gt; for
a few ones (like reading the character at a given position). Let's just
link &lt;a href=&quot;http://en.wikipedia.org/wiki/Rope_(computer_science)&quot;&gt;Wikipedia&lt;/a&gt;
for the administrative details. Bottom line is, on big strings we
positively gain something (more than simply being able to run HeVeA on
old machines).&lt;/p&gt;

&lt;h1&gt;In HeVeA&lt;/h1&gt;

&lt;p&gt;The main place in HeVeA where this structure finds its use is in a file
called &lt;code&gt;out.ml&lt;/code&gt;. It provides functions to manipulate &amp;quot;buffers&amp;quot;, which
are called by the html-generating function to produce the final output.
Before my intervention, the buffer type looked like :&lt;/p&gt;

&lt;pre class=&quot;code-ocaml&quot;&gt;&lt;span class=&quot;hl kwa&quot;&gt;type&lt;/span&gt; buff &lt;span class=&quot;hl opt&quot;&gt;= {&lt;/span&gt;
  &lt;span class=&quot;hl kwa&quot;&gt;mutable&lt;/span&gt; buff &lt;span class=&quot;hl opt&quot;&gt;:&lt;/span&gt; &lt;span class=&quot;hl kwc&quot;&gt;string&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;;&lt;/span&gt;
  &lt;span class=&quot;hl kwa&quot;&gt;mutable&lt;/span&gt; bp &lt;span class=&quot;hl opt&quot;&gt;:&lt;/span&gt; &lt;span class=&quot;hl kwc&quot;&gt;int&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;;&lt;/span&gt;
  &lt;span class=&quot;hl kwa&quot;&gt;mutable&lt;/span&gt; len &lt;span class=&quot;hl opt&quot;&gt;:&lt;/span&gt; &lt;span class=&quot;hl kwc&quot;&gt;int&lt;/span&gt;
&lt;span class=&quot;hl opt&quot;&gt;}&lt;/span&gt;
&lt;span class=&quot;hl opt&quot;&gt;;;&lt;/span&gt; &lt;span class=&quot;hl com&quot;&gt;(* In 1998 they wrote ;; *)&lt;/span&gt;

&lt;span class=&quot;hl kwa&quot;&gt;type&lt;/span&gt; t &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt; &lt;span class=&quot;hl kwd&quot;&gt;Buff&lt;/span&gt; &lt;span class=&quot;hl kwa&quot;&gt;of&lt;/span&gt; buff | &lt;span class=&quot;hl kwd&quot;&gt;Chan&lt;/span&gt; &lt;span class=&quot;hl kwa&quot;&gt;of&lt;/span&gt; out_channel | &lt;span class=&quot;hl kwd&quot;&gt;Null&lt;/span&gt;
&lt;span class=&quot;hl opt&quot;&gt;;;&lt;/span&gt;
&lt;/pre&gt;

&lt;p&gt;&lt;code&gt;buff&lt;/code&gt; is the string containing the output, &lt;code&gt;bp&lt;/code&gt; is the position of the
cursor in this string, and &lt;code&gt;len&lt;/code&gt; is &lt;code&gt;String.length buff&lt;/code&gt; (probably for
optimization reasons). When you wrote something to the &lt;code&gt;buff&lt;/code&gt;, it is
blitted at &lt;code&gt;bp&lt;/code&gt; position, and when you retrieve something (for example
to put it in the final output), you get all the part between the
beginning and &lt;code&gt;bp&lt;/code&gt;. &lt;code&gt;Chan&lt;/code&gt; and &lt;code&gt;Null&lt;/code&gt; are used for unknown (at my global
understanding level) purposes and are not changed with ropes.&lt;/p&gt;

&lt;p&gt;The new type looks like :&lt;/p&gt;

&lt;pre class=&quot;code-ocaml&quot;&gt;&lt;span class=&quot;hl kwa&quot;&gt;type&lt;/span&gt; t &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt;
  | &lt;span class=&quot;hl kwd&quot;&gt;Rope&lt;/span&gt; &lt;span class=&quot;hl kwa&quot;&gt;of&lt;/span&gt; &lt;span class=&quot;hl kwd&quot;&gt;S&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;.&lt;/span&gt;t &lt;span class=&quot;hl kwc&quot;&gt;ref&lt;/span&gt;
  | &lt;span class=&quot;hl kwd&quot;&gt;Chan&lt;/span&gt; &lt;span class=&quot;hl kwa&quot;&gt;of&lt;/span&gt; out_channel
  | &lt;span class=&quot;hl kwd&quot;&gt;Null&lt;/span&gt;

&lt;span class=&quot;hl com&quot;&gt;(* S being the rope module, providing type t for ropes *)&lt;/span&gt;
&lt;/pre&gt;

&lt;p&gt;As HeVeA globally works a lot by mutability, I use a reference to a
rope, which is an immutable structure. All I had to do then was changing
the code of the module function by function, which was quite straightforward
once I understood the exact meaning of the old code. Well, I still had
strange bugs, mainly for reading too quickly the former version, but
also for unexpected behaviours from external modules which I did not
really know about (like Format from the standard library, which does
strange things with formatters; you should read carefully about it before
thinking you know how to use them).&lt;/p&gt;

&lt;h1&gt;The result&lt;/h1&gt;

&lt;p&gt;In conclusion, HeVeA is now 20% slower, which is quite a good
result considering the optimizations which were done before (and the way
we totally did not care about it when using ropes). It will probably
stay as it is concerning this point, because it is quick enough for main
purpose (namely less than one second on reasonable documents). But if it
has to be sped up one day, it would be possible due to the stronger
architecture of the Out module, and by using ropes on the other parts of
the software, instead of converting from rope to (small) string and
reciprocally on many places.&lt;/p&gt;
</description><pubDate>12 Jul 2012 08:00:00 +0000</pubDate><category>HeVeA</category><category>ocaml</category><category>strings</category><category>ropes</category><guid isPermaLink="true">http://gallium.inria.fr/~scherer/gagallium/hevea-a-new-rope/index.html</guid></item></channel></rss>
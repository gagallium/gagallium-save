<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0"><channel><title>Gagallium: gallium</title><link>http://gallium.inria.fr/~scherer/gagallium/topic_gallium.html</link><description>A draft blog at Gallium</description><managingEditor>gabriel.scherer@inria.fr</managingEditor><pubDate>23 May 2012 08:00:00 +0000</pubDate><lastBuildDate>23 May 2012 08:00:00 +0000</lastBuildDate><generator>Stog</generator><item><title>Students at Gallium</title><link>http://gallium.inria.fr/~scherer/gagallium/students-at-gallium/index.html</link><description>&lt;p&gt;A short presentation of the various young people that
regularly gather (or used to gather) at the conversation-rich and
often surprising coffee breaks we have at Gallium: &lt;em&gt;les dix
gagas&lt;/em&gt;.&lt;/p&gt;
  
&lt;h3&gt;Onsite Gallium PhD students&lt;/h3&gt;

&lt;p&gt;In decreasing order of elderness:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;&lt;p&gt;&lt;a href=&quot;http://xulforum.org/&quot;&gt;Jonathan Protzenko&lt;/a&gt; had the idea of this
blog. He is working with François Pottier on programming language
approaches to have finer control of mutability and aliasing. He also
hacks for Mozilla on his spare time (eg. he's the author of the
world-famous Conversations extension for Thunderbird) and even wrote
a full book about XUL in his young age, before seeing the truth of
functional programming. From these fond experiences he's formed
a strong sense of contribution to the community, which makes him an
asset of the OCaml bug triaging volunteer squad. Let's just add that
he took responsability for the Windows installer, to give you an
idea of his sense of self-sacrifice. His is also in charge of the
&amp;quot;junior seminar&amp;quot; here at INRIA Rocquencourt, a great opportunity for
students to practice their communication skills, that I hope this
blog will complement.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;Fixing Windows bugs is rarely a charming activity, and Jonathan's
mood sometime pays for it. &lt;a href=&quot;http://gallium.inria.fr/~jcretin/&quot;&gt;Julien
Cretin&lt;/a&gt; is the ever-smiling
one. He works on coercions (that is, in his mind, mostly anything
typing-related) with Didier Rémy but is in fact busy preparing the
revolution: anytime soon programmers will stop using term syntax and
variables, and instead draw graphs. He's also an asset for our
cultural diversity, as his main working language is Haskell; he had
the luck to do an internship in MSR Cambridge last year, where he
helped a dream team to &lt;a href=&quot;http://research.microsoft.com/en-us/um/people/simonpj/papers/ext-f/promotion.pdf&quot;&gt;promote Haskell
(PDF)&lt;/a&gt;
through kind-level programming.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;I'm &lt;a href=&quot;http://gallium.inria.fr/~scherer/&quot;&gt;Gabriel Scherer&lt;/a&gt; and
I should be working on module systems, also with Didier Rémy. In
fact, I'm trying to recover from a serious illness: the inability
not to answer a question on the Internet -- or comment on a good
post. Please also don't approach me with interesting research topics
(anything programming language related), I'm much too likely to get
distracted.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;Jacques-Henri Jourdan started as a PhD student just a few weeks
ago. He has already worked in Gallium as an intern, on a formal
correctness proof from a grammar description (in Coq, automatically
translated from a Menhir grammar) to a LR automaton. He will now
work on mechanized formalizations of abstract interpretation
techniques.&lt;/p&gt;&lt;/li&gt;
&lt;/ul&gt;

&lt;h3&gt;Stranger beings&lt;/h3&gt;

&lt;ul&gt;
&lt;li&gt;&lt;p&gt;&lt;a href=&quot;http://gallium.inria.fr/~pilkiewi/&quot;&gt;Alexandre Pilkiewicz&lt;/a&gt; found
that writing a PhD thesis was too easy, and decided to
simultaneously start a job at Google. Here, he was working on
formalizing compiler optimizations based on polyhedral loop
analyses. He is very comfortable with Coq, to the point that he once
did a formal proof of correctness for a small patch I proposed for
the standard Map data structure, just for fun.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;&lt;a href=&quot;http://cseweb.ucsd.edu/~vrobert/&quot;&gt;Valentin Robert&lt;/a&gt; worked as an
intern with Xavier Leroy on Compcert, first on a certified pointer
analysis pass, and then on test programs to verify the work of the
external non-verified toolchains that take Compcert proved-correct
assembly output and generate a real executable. He is our meme
expert, and notoriously indecisive. After much hesitation, he will
be leaving us in a few months to do a PhD in San Diego.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;&lt;a href=&quot;http://gallium.inria.fr/~rproust/&quot;&gt;Raphael Proust&lt;/a&gt; is the most
recent intern in the team; he will be working with Xavier Leroy on
the intermediate representations (in a compiler) of functional
languages : CPS, SSA, ANF etc. He previously worked with the
&lt;a href=&quot;http://www.openmirage.org/&quot;&gt;Mirage&lt;/a&gt; team in Cambridge, doing
OS-level development in OCaml, and will probably go back there to
start a PhD in a few months. That's a very interesting project with
not-so-surprising ties to programming language research; for example
he worked on the use of &lt;a href=&quot;http://anil.recoil.org/papers/drafts/2012-places-limel-draft1.pdf&quot;&gt;Limel
(PDF)&lt;/a&gt;,
a programming language with linear type, to faithfully encode the
constraints on low-level dataflow tasks in a networked system.&lt;/p&gt;&lt;/li&gt;
&lt;/ul&gt;

&lt;h3&gt;Recent former students&lt;/h3&gt;

&lt;p&gt;I won't list all the former students of Gallium, but here are those
I had the pleasure to meet while they were still on duty.&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;&lt;p&gt;&lt;a href=&quot;http://nicolaspouillard.fr/&quot;&gt;Nicolas Pouillard&lt;/a&gt; is our former
&amp;quot;cultural diversity&amp;quot; leader, using Agda as his proving tool of
choice. Coq and OCaml are more in the culture of the place, but
anyone is free to choose -- and XMonad is certainly king among the
window managers used. Nicolas worked on the hairy subject of
representing and formally reasoning on syntactic terms with
binders. He was also our local expert on practical uses of secure
design or cryptography, such as security by capabilities (a hobby
I share), web privacy, BitCoin, Tahoe-LAFS... He is now a post-doc
at ITU Copenhaguen as part of the &lt;a href=&quot;http://www.demtech.dk/&quot;&gt;demtech&lt;/a&gt;
project on &amp;quot;trustworthy democratic technology&amp;quot;.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;&lt;a href=&quot;http://www.normalesup.org/~ramanana/&quot;&gt;Tahina Ramananandro&lt;/a&gt; worked
on the craziest topic one could find. What is the hardest way to
describe a programming language?  I would say it is writing
mechanized formal semantics for it. What is the most insanely
complex language a twisted mind could want to describe? C++. At the
end of a truly heroic effort, Tahina had a formalized semantics of
memory layout of structs/classes and construction/destruction of
objects. He's now doing a post-doc in the
&lt;a href=&quot;http://flint.cs.yale.edu/&quot;&gt;FLINT&lt;/a&gt; group at Yale.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;&lt;a href=&quot;http://www.cis.upenn.edu/~montagu/&quot;&gt;Benoît Montagu&lt;/a&gt; worked on
module systems with Didier Rémy. He studied several ways to enhance
Fω with some orthogonal features each adding some aspect of the ML
module system. He's now a post-doc in Philadelphia, as part of the
quite exciting &lt;a href=&quot;http://www.crash-safe.org/&quot;&gt;CRASH/SAFE&lt;/a&gt;
project. Take a bunch of type system and formal proof experts, and
ask them to design an end-to-end (hardware and software) system
aimed for security, safety and reliability; they produce a &lt;em&gt;dynamic&lt;/em&gt;
language.&lt;/p&gt;&lt;/li&gt;
&lt;/ul&gt;
</description><pubDate>23 May 2012 08:00:00 +0000</pubDate><category>gallium</category><category>students</category><guid isPermaLink="true">http://gallium.inria.fr/~scherer/gagallium/students-at-gallium/index.html</guid></item></channel></rss>
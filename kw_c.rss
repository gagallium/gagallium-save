<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0"><channel><title>Gagallium: C</title><link>http://gallium.inria.fr/~scherer/gagallium/kw_c.html</link><description>A draft blog at Gallium</description><managingEditor>gabriel.scherer@inria.fr</managingEditor><pubDate>07 Dec 2012 08:00:00 +0000</pubDate><lastBuildDate>07 Dec 2012 08:00:00 +0000</lastBuildDate><generator>Stog</generator><item><title>Verifying a parser for a C compiler (continued)</title><link>http://gallium.inria.fr/~scherer/gagallium/verifying-a-parser-for-a-c-compiler-2/index.html</link><description>&lt;p&gt;In
  my &lt;a href=&quot;http://gallium.inria.fr/~scherer/gagallium/verifying-a-parser-for-a-c-compiler/&quot;&gt;last
  post&lt;/a&gt;, I explained my work on verifying a parser for
  Compcert. I was mainly explaining the solutions I found to my
  performances problems. Here, I will discuss an other interesting
  problem I have, concerning the context sensitivity of the C99
  grammar.&lt;/p&gt;



&lt;h2&gt;An alternative to the lexer hack&lt;/h2&gt;

&lt;p&gt;The &lt;emph&gt;lexer hack&lt;/emph&gt; is the typical solution to the problem
  of context sensitivity of the C grammar outlined in
  the &lt;a href=&quot;http://gallium.inria.fr/~scherer/gagallium/verifying-a-parser-for-a-c-compiler/&quot;&gt;previous
  blog article&lt;/a&gt; and explained more in depth
  in &lt;a href=&quot;http://eli.thegreenplace.net/2011/05/02/the-context-sensitivity-of-c%E2%80%99s-grammar-revisited/&quot;&gt;this
  external blog article&lt;/a&gt;. It cannot be implemented in our parser,
  because it uses some side effect in the semantic actions of the
  grammar in order to give some information to the lexer, and, as you
  might know, side effects are forbidden in Coq.&lt;/p&gt;

&lt;p&gt;Any Haskell programmer would say that the solution is to use a
  state monad, but both the semantic actions described in the grammar
  and the lexer would need to compute in this monad, in order to
  communicate with each other. Then, it becomes very hard to specify
  what is a valid semantic value for a token sequence, because one has
  to specify how the lexer and the parser semantic actions
  interleaves. This becomes a real nightmare when you allow the parser
  to ask the lexer for a lookahead token before deciding to call some
  semantic action...&lt;/p&gt;

&lt;p&gt;So it is necessary to find another approach. After some thoughts,
  I remembered the parsing approach of &lt;emph&gt;mixfix operators&lt;/emph&gt;
  in the Agda programming language, advertized in Gallium by our own
  Agda expert &lt;a href=&quot;http://nicolaspouillard.fr/&quot;&gt;Nicolas
  Pouillard&lt;/a&gt;. Mixfix operators are syntactic constructions that can
  be created dynamically: this is comparable to
  the &lt;code&gt;Notation&lt;/code&gt; command of Coq. The way they are handled
  in Agda is described
  in &lt;a href=&quot;http://www.cse.chalmers.se/~nad/publications/danielsson-norell-mixfix.pdf&quot;&gt;this
  paper&lt;/a&gt;. Roughly, the parser only analyzes the main structure of
  the file. The expressions are not analyzed at this point: they are
  left at the state of list of tokens. At a later stage of processing,
  an other module uses the extra context information it knows (notably
  the mixfix operators available) to parse expressions. This new
  information is obtained from the first parsing phase, which brings
  enough structure to go on.&lt;/p&gt;

&lt;p&gt;Here, the situation is very similar: we need some more context
  information (whether a given identifier is a type or a variable
  name) in order to parse expressions and declarations. We can use a
  similar technique: if the parser is not able to parse those,
  then it should store them as a list of tokens in the abstract syntax
  tree. A second pass in then necessary to parse those &amp;quot;chunks&amp;quot;
  correctly. Recursively, this second pass can delay parsing for
  smaller chunks, and so on.&lt;/p&gt;

&lt;p&gt;Obviously, I have omitted some details here. First, it requires
  some serious engineering on the C99 grammar in order to separate it
  into a &amp;quot;safe&amp;quot; part, where the extra information given by the context
  is not needed, and a &amp;quot;dangerous&amp;quot; part, where the parser needs that
  the two different flavors of identifiers (type names and variable
  names) are represented by two different tokens. Second, recognizing
  the end of these chunks without actually parsing them requires
  recognizing some patterns, typically like matching parentheses.&lt;/p&gt;

&lt;h2&gt;Avoiding polluting the AST with chunks&lt;/h2&gt;

&lt;p&gt;Storing chunks in the abstract syntax tree has a major drawback: it
  requires adding some constructors in its types for those chunks,
  which do not have any real sense in the language. The solution I use
  instead is to make all my semantic actions compute in a state
  monad, which passes the contextual environment information: while
  parsing, the semantic actions build the syntax tree in the form of
  a tree of closures. Once something has been parsed, the caller gives
  the initial environment as a parameter to the head closure, and the
  environment information propagates along the tree.&lt;/p&gt;

&lt;p&gt;When there is a chunk in some node of the AST, the corresponding
  semantic action first uses the environment information to convert
  identifier tokens to their right flavor, then recursively parses the
  chunk, and finally gives the current environment to the returned
  semantic value, so that it can itself evaluate.&lt;/p&gt;

&lt;p&gt;This monad is a bit more complicated than a simple state monad,
  because it also handles error (nested parsing can fail), and open
  recursion (the parser has to call itself, when it is not already
  defined).&lt;/p&gt;

&lt;h2&gt;Conclusion&lt;/h2&gt;

&lt;p&gt;&lt;a href=&quot;Parser.vy.html&quot;&gt;Here&lt;/a&gt; is a working version of the
  parser, using those ideas. A bit surprisingly, using all this monad
  machinery and delaying parsing does not decrease the performances
  too much. I have still some troubles proving it, and I'm looking
  forward to ideas to state all the invariants in a not-too-verbose
  way.&lt;/p&gt;

&lt;p&gt;This work is still in progress, so I would be happy if you could
  give me your comments or ideas. But, in any case, I am now
  convinced that it is really possible to have a formalization of a
  parser for the C language, and particularly for the lexer hack. And
  this is a good thing, because, as its name suggest, the lexer hack
  is a hack, whose correctness is far from clear.&lt;/p&gt;

&lt;p&gt;I have to say that, even if this work of proving a parser for
  a C compiler is sometimes quite tiresome, it allowed me to learn
  things in many different domains, from designing powerful automaton
  tactics in Coq to OCaml code optimization and LR(1)
  hackery: it's been a very challenging and rewarding experience.&lt;/p&gt;

&lt;p&gt;If I wanted to work on that again in the future, there is still
  many things one would want to prove: does the parser terminates on
  all inputs? Are we sure it does not ask for a lookahead token after
  the end of the stream (that could be a problem in interactive
  languages, because it could block the input)?&lt;/p&gt;</description><pubDate>07 Dec 2012 08:00:00 +0000</pubDate><category>research</category><category>parser</category><category>compcert</category><category>C</category><category>coq</category><guid isPermaLink="true">http://gallium.inria.fr/~scherer/gagallium/verifying-a-parser-for-a-c-compiler-2/index.html</guid></item><item><title>Verifying a parser for a C compiler</title><link>http://gallium.inria.fr/~scherer/gagallium/verifying-a-parser-for-a-c-compiler/index.html</link><description>&lt;p&gt;In 2011, I designed a formally verified C parser for my master's
  internship. You can refer to
  our &lt;a href=&quot;http://gallium.inria.fr/~xleroy/publi/validated-parser.pdf&quot;&gt;ESOP
  paper&lt;/a&gt; (with François Pottier and Xavier Leroy) to see the
  details. Here, I am going to explain what problems (and some of
  their solutions) I have to overcome in order to use it
  in &lt;a href=&quot;http://compcert.inria.fr/&quot;&gt;Compcert&lt;/a&gt;, the certified
  compiler developed at Gallium in Coq.&lt;/p&gt;



&lt;h2&gt;The parsing engine, an overview&lt;/h2&gt;

&lt;p&gt;I am not going to go into the details here, but let's get an overview
  of what was my work of 2011. Here is the big picture:&lt;/p&gt;

&lt;img alt=&quot;Parsing engine architecture&quot; src=&quot;archi.png&quot; style=&quot;margin-bottom:2em;&quot;/&gt;


&lt;p&gt;Basically, I added an instrumented Coq backend to the
  &lt;a href=&quot;http://gallium.inria.fr/~fpottier/menhir/&quot;&gt;Menhir&lt;/a&gt; parser
  generator. It generates a Coq version of the Grammar, together with an
  LR(1)-like automaton and a certificate for this automaton. A
  validator, written in Coq, checks that this certificate is valid, that
  is that the automaton is related, in some sense, to the grammar.&lt;/p&gt;

&lt;p&gt;In order to execute the parser, I developed in Coq an interpreter for
  LR(1) automatons. It takes a token stream in input and returns a
  semantic value.&lt;/p&gt;

&lt;p&gt;Then, I give a formal proof of the Coq code. Informally, it says that
  if a given automaton has been validated with respect to a grammar,
  using some certificate, then I get a correct and complete parser by
  using the interpreter and the automaton.&lt;/p&gt;

&lt;p&gt;What is a correct and complete parser? Correct means that when the
  parser accepts the input, then returned semantic value is a valid
  semantic value for the input (which implies that the input word is in
  the grammar's language). Complete means that if the input word has
  some semantic value, then the parser will return this semantic value.&lt;/p&gt;

&lt;p&gt;One advantage of this approach is that I do not need to prove the
  generator itself, which is a pretty complicated task. The validation
  process is not as complicated, as some simple information over the
  invariants of the automaton is enough. Moreover, one can change the
  variant of LR(1) automaton (LR(0), LALR, SLR, pager-LR(1), canonical
  LR(1)...) one wants to use without much work. Note that it does not
  suffers the typical drawback of translation validation in verified
  compilation. Typically, the validator performs at run time, and it
  does not need to validate its input. When it does not, either
  because it is incomplete or because its input is invalid, the
  compiler has to fail unexpectedly. Here, as the validation is done
  at parser generation time, we are sure, once it is generated, that
  it will perform well.&lt;/p&gt;

&lt;p&gt;In order to get more details on this parts, please refer to our
  &lt;a href=&quot;http://gallium.inria.fr/~xleroy/publi/validated-parser.pdf&quot;&gt;ESOP
    paper&lt;/a&gt;.&lt;/p&gt;

&lt;h2&gt;When theory goes into practice&lt;/h2&gt;

&lt;p&gt;My final aim when I began this work was to integrate it into
  Compcert. Besides, before writing the paper, I implemented a
  proof-of-concept version of Compcert including a verified
  parser. However, this parser had several more or less critical
  downsides:&lt;/p&gt;

&lt;ul&gt;
  &lt;li&gt;The context sensitivity of the C grammar makes it impossible to
    parse it using pure LR(1) technology. This
    good &lt;a href=&quot;http://eli.thegreenplace.net/2011/05/02/the-context-sensitivity-of-c%E2%80%99s-grammar-revisited/&quot;&gt;blog
      article&lt;/a&gt; explains the problem if you are not aware of
    it. Without entering into the details, our solution worked, but
    was not fully satisfactory, because the &amp;quot;lexer&amp;quot; was so complicated
    that it included a full non-certified parser, which could, if
    buggy, in some corner cases, introduce bugs in the whole parser.&lt;/li&gt;
  &lt;li&gt;Parser is a part of a compiler where the performance matters,
    because it manipulates quite a lot of data. Our approach appears
    not to be disastrous in terms of performances, but still, in our
    first implementation, the parser and lexer ran 5 times slower than
    the previous non-verified Menhir-generated parser.&lt;/li&gt;
  &lt;li&gt;It is much more convenient to describe a grammar in the Menhir's
    description language than directly in Coq. However, the validation
    of the automaton with respect to a grammar must be done using a
    Coq form of the grammar. So, actually, one must be confident over
    the translation of the grammar from Menhir form to Coq form. I
    tried to make this translation very readable so that someone could
    check it by hand, but still, it is not very satisfactory&lt;/li&gt;
&lt;/ul&gt;

&lt;h2&gt;Performance of the extracted code&lt;/h2&gt;

&lt;h3&gt;Internalizing types the right way&lt;/h3&gt;

&lt;p&gt;After some benchmarking, it appeared that the main bottleneck was
  the polymorphic hash function of OCaml. Why? Simply because the Coq
  code needed to manipulate some data that had a type I did not want
  to formalize in Coq. More details below.&lt;/p&gt;

&lt;p&gt;This problem appears typically when you want to manipulate
  identifiers in Coq: as the naive encoding of strings as lists of
  characters in Coq is terribly slow, the typical way of using them as
  identifiers in a Coq code is to build a hash map between some
  positive numbers and all the identifiers appearing in the source
  code. The construction of this hash map is done in some OCaml
  trusted code. Then you can do &amp;quot;efficiently&amp;quot; multiple things on
  identifiers in the Coq code: you can compare them, you can build
  maps using them as keys...&lt;/p&gt;

&lt;p&gt;However, in my case, I used this technique on the type of location
  information (that is, the data associated to most of the nodes of
  the AST to indicate where it comes from). It was very inefficient,
  because I needed to hash one location information for every very
  token. Moreover, I do not need to do complex computations on these
  data. So I figured out that I could use an &amp;quot;abstract type&amp;quot; for these
  data: I declare it as a &lt;code&gt;Parameter&lt;/code&gt; in Coq, and I force
  extraction to replace it by the right OCaml type. This improved
  performances a lot.&lt;/p&gt;

&lt;h3&gt;Using &lt;code&gt;Int31&lt;/code&gt;&lt;/h3&gt;

&lt;p&gt;The other bottleneck was dynamic checks: in order to avoid using
  too much dependent types in the interpreter (there are already a few
  of them, and the proofs are complicated enough like that), I put
  some dynamic checks. I have proved that they will never fail, so
  there is no problem of correctness here. But still, they exist and
  they are executed in the extracted code. It was very frustrating
  because they were consuming quite a lot of time, but I knew they
  were not useful at all.&lt;/p&gt;

&lt;p&gt;These dynamic checks typically verify that the symbols present at
  the top of the stack correspond to what is expected. This is, from
  an OCaml point of view, very simple: they consists in comparison of
  two elements of inductive types of hundreds of constructors without
  parameters.&lt;/p&gt;

&lt;p&gt;However, from a Coq point of view, this is harder: in order to
  compare values of a type, one needs to provide a decision procedure
  for equality over this type. Naively, the size of such a decision
  procedure is (at least) quadratic over the size of the inductive
  type. Let's take, for example, the case of this simple inductive
  type:&lt;/p&gt;



&lt;pre class=&quot;code-ocaml&quot;&gt;&lt;span class=&quot;hl kwd&quot;&gt;Inductive T&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;:&lt;/span&gt; &lt;span class=&quot;hl kwd&quot;&gt;Type&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;:=&lt;/span&gt; &lt;span class=&quot;hl kwd&quot;&gt;A&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;:&lt;/span&gt;&lt;span class=&quot;hl kwd&quot;&gt;T&lt;/span&gt; | &lt;span class=&quot;hl kwd&quot;&gt;B&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;:&lt;/span&gt;&lt;span class=&quot;hl kwd&quot;&gt;T&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;.&lt;/span&gt;
&lt;/pre&gt;



&lt;p&gt;A decision procedure for such a type can be written:&lt;/p&gt;



&lt;pre class=&quot;code-ocaml&quot;&gt;&lt;span class=&quot;hl kwd&quot;&gt;Definition T_eq&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;x y&lt;span class=&quot;hl opt&quot;&gt;:&lt;/span&gt;&lt;span class=&quot;hl kwd&quot;&gt;T&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;) :&lt;/span&gt; &lt;span class=&quot;hl kwc&quot;&gt;bool&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;:=&lt;/span&gt;
  &lt;span class=&quot;hl kwb&quot;&gt;match&lt;/span&gt; x&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; y &lt;span class=&quot;hl kwb&quot;&gt;with&lt;/span&gt;
  | &lt;span class=&quot;hl kwd&quot;&gt;A&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; &lt;span class=&quot;hl kwd&quot;&gt;A&lt;/span&gt; | &lt;span class=&quot;hl kwd&quot;&gt;B&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; &lt;span class=&quot;hl kwd&quot;&gt;B&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;=&amp;gt;&lt;/span&gt; &lt;span class=&quot;hl kwc&quot;&gt;true&lt;/span&gt;
  | _&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; _ &lt;span class=&quot;hl opt&quot;&gt;=&amp;gt;&lt;/span&gt; &lt;span class=&quot;hl kwc&quot;&gt;false&lt;/span&gt;
  &lt;span class=&quot;hl kwb&quot;&gt;end&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;.&lt;/span&gt;
&lt;/pre&gt;



&lt;p&gt;However, the kernel of Coq only understands simple pattern
  matching. So, the frontend desugars it into:&lt;/p&gt;



&lt;pre class=&quot;code-ocaml&quot;&gt;&lt;span class=&quot;hl kwb&quot;&gt;match&lt;/span&gt; x &lt;span class=&quot;hl kwb&quot;&gt;with&lt;/span&gt;
| &lt;span class=&quot;hl kwd&quot;&gt;A&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;=&amp;gt;&lt;/span&gt; &lt;span class=&quot;hl kwb&quot;&gt;match&lt;/span&gt; y &lt;span class=&quot;hl kwb&quot;&gt;with&lt;/span&gt; &lt;span class=&quot;hl kwd&quot;&gt;A&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;=&amp;gt;&lt;/span&gt; &lt;span class=&quot;hl kwc&quot;&gt;true&lt;/span&gt;  | &lt;span class=&quot;hl kwd&quot;&gt;B&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;=&amp;gt;&lt;/span&gt; &lt;span class=&quot;hl kwc&quot;&gt;false&lt;/span&gt; &lt;span class=&quot;hl kwb&quot;&gt;end&lt;/span&gt;
| &lt;span class=&quot;hl kwd&quot;&gt;B&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;=&amp;gt;&lt;/span&gt; &lt;span class=&quot;hl kwb&quot;&gt;match&lt;/span&gt; y &lt;span class=&quot;hl kwb&quot;&gt;with&lt;/span&gt; &lt;span class=&quot;hl kwd&quot;&gt;A&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;=&amp;gt;&lt;/span&gt; &lt;span class=&quot;hl kwc&quot;&gt;false&lt;/span&gt; | &lt;span class=&quot;hl kwd&quot;&gt;B&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;=&amp;gt;&lt;/span&gt; &lt;span class=&quot;hl kwc&quot;&gt;true&lt;/span&gt;  &lt;span class=&quot;hl kwb&quot;&gt;end&lt;/span&gt;
&lt;span class=&quot;hl kwb&quot;&gt;end&lt;/span&gt;
&lt;/pre&gt;



&lt;p&gt;This code has quadratic size in the number of constructors. The
  extraction is smart enough to factorize cases and get some linear
  length code, but the Coq kernel has to deal with these big
  terms. Actually, the problem is even worse than it seems: in order
  to prove that this term is correct directly, it seems necessary to
  give a term of cubic size...&lt;/p&gt;

&lt;p&gt;This is not tractable in our case: the types of data we are
  comparing typically have a few hundreds of constructors, and the
  generated term would be enormous, and would take ages to typecheck.
  A smarter solution is to provide an injection from our type to
  another Coq type for which I can decide equality easily. Typically,
  I can provide an injection to the type &lt;code&gt;positive&lt;/code&gt;, and
  that is what I did. This is quite inefficient, because at each
  comparison, OCaml has to find the corresponding positives, and then
  compare them bits after bits.&lt;/p&gt;

&lt;p&gt;The optimization I used here was to use 31 bits integers provided
  in the Coq standard library, instead of positives, but it was not as
  easy as it seemed. Indeed, these integers are internally represented
  as 31 bits OCaml integers when using the &lt;code&gt;vm_compute&lt;/code&gt;
  tactic, achieving good performance. But they are extracted to OCaml
  to a bit by bit representation, which have not much better
  performance than &lt;code&gt;positive&lt;/code&gt;s. So I had to teach Coq to
  extract &lt;code&gt;int31&lt;/code&gt; to OCaml's &lt;code&gt;int&lt;/code&gt; (which is
  either 31 bits long or 63 bits long, depending on the
  architecture). A final care was necessary to make sure that the
  int31 literals in the Coq source code were really converted to
  a constant after the whole compilation process (that was not
  obvious!).&lt;/p&gt;

&lt;h3&gt;Storing the automaton tables&lt;/h3&gt;

&lt;p&gt;After these optimization, the performance are about only 2.5 to 3
  times slower than the non-certified parser. Based on my
  benchmarks, one of the bottleneck is the representation of the
  tables of the automaton: I currently use big &lt;code&gt;match&lt;/code&gt;
  constructs, which does not seem to be as efficient as static
  arrays, which I do not have in Coq.&lt;/p&gt;

&lt;p&gt;However, I don' know a satisfactory solution here (maybe you do?):
  I think I will let it as is, as the performances are good
  enough.&lt;/p&gt;</description><pubDate>24 Oct 2012 08:00:00 +0000</pubDate><category>research</category><category>parser</category><category>compcert</category><category>C</category><category>coq</category><guid isPermaLink="true">http://gallium.inria.fr/~scherer/gagallium/verifying-a-parser-for-a-c-compiler/index.html</guid></item></channel></rss>
<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0"><channel><title>Gagallium: merge</title><link>http://gallium.inria.fr/~scherer/gagallium/kw_merge.html</link><description>A draft blog at Gallium</description><managingEditor>gabriel.scherer@inria.fr</managingEditor><pubDate>18 Aug 2012 08:00:00 +0000</pubDate><lastBuildDate>18 Aug 2012 08:00:00 +0000</lastBuildDate><generator>Stog</generator><item><title>Comments on Matías Giovannini's &quot;Merge Right&quot;</title><link>http://gallium.inria.fr/~scherer/gagallium/comments-on-merge-right/index.html</link><description>&lt;p&gt;A relatively common story: a comment I started writing in reaction to
Matías Giovannini's &lt;a href=&quot;http://alaska-kamtchatka.blogspot.ca/2012/08/merge-right.html&quot;&gt;Merge
right&lt;/a&gt;
blog post turned out lengthier than I initially expected. It feels
a bit off-topic on Gagallium, but I know there are people interested
in reading about Matías' perfectionist code snippets (I am), so there
might also be an audience for perfectionist comments about
perfectionist blog posts...&lt;/p&gt;




&lt;p&gt;If you haven't read &lt;a href=&quot;http://alaska-kamtchatka.blogspot.ca/2012/08/merge-right.html&quot;&gt;Matías' blog
post&lt;/a&gt;
yet, you should have a look at it now – or skip this post
altogether. He defines a rather generic merging operation on sorted
collections, parametrized by functions deciding what to do when the
next item on the left collection is smaller, or on the right
collection, when both items are equal, or when one of the two
collections has no more elements.&lt;/p&gt;

&lt;p&gt;He then enumerates all reasonable choices for those function
parameters, to get a list of 32 different &amp;quot;merge operators&amp;quot; that are
instance of this general function. It's amusing to see this
classification carried out in full.&lt;/p&gt;

&lt;p&gt;The code for the generic merge function is essentially this one:&lt;/p&gt;

&lt;pre class=&quot;code-ocaml&quot;&gt;&lt;span class=&quot;hl kwa&quot;&gt;let&lt;/span&gt; merge ln rn lt eq gt &lt;span class=&quot;hl opt&quot;&gt;:&lt;/span&gt; 'a list &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; 'a list &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; 'a list &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt;
  &lt;span class=&quot;hl kwa&quot;&gt;let rec&lt;/span&gt; go l r &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt; &lt;span class=&quot;hl kwb&quot;&gt;match&lt;/span&gt; l&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; r &lt;span class=&quot;hl kwb&quot;&gt;with&lt;/span&gt;
  | &lt;span class=&quot;hl opt&quot;&gt;[],&lt;/span&gt; ys &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; ln ys
  | xs&lt;span class=&quot;hl opt&quot;&gt;, [] -&amp;gt;&lt;/span&gt; rn xs
  | x &lt;span class=&quot;hl opt&quot;&gt;::&lt;/span&gt; xs&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; y &lt;span class=&quot;hl opt&quot;&gt;::&lt;/span&gt; ys &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt;
    &lt;span class=&quot;hl kwb&quot;&gt;match&lt;/span&gt; compare x y &lt;span class=&quot;hl kwb&quot;&gt;with&lt;/span&gt;
    | &lt;span class=&quot;hl opt&quot;&gt;-&lt;/span&gt;&lt;span class=&quot;hl num&quot;&gt;1&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; lt x &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;go xs r&lt;span class=&quot;hl opt&quot;&gt;)&lt;/span&gt;
    |  &lt;span class=&quot;hl num&quot;&gt;0&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; eq x &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;go xs ys&lt;span class=&quot;hl opt&quot;&gt;)&lt;/span&gt;
    |  &lt;span class=&quot;hl num&quot;&gt;1&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; gt y &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;go l  ys&lt;span class=&quot;hl opt&quot;&gt;)&lt;/span&gt;
    |  _ &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; &lt;span class=&quot;hl kwb&quot;&gt;assert&lt;/span&gt; &lt;span class=&quot;hl kwc&quot;&gt;false&lt;/span&gt;
  &lt;span class=&quot;hl kwa&quot;&gt;in&lt;/span&gt; go
&lt;/pre&gt;

&lt;p&gt;With the following operators used as parameters, in two groups, one
for &lt;code&gt;ln/rn&lt;/code&gt; and one for &lt;code&gt;lt/eq/gt&lt;/code&gt;:&lt;/p&gt;

&lt;pre class=&quot;code-ocaml&quot;&gt;&lt;span class=&quot;hl kwa&quot;&gt;let&lt;/span&gt; self xs &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt;      xs
&lt;span class=&quot;hl kwa&quot;&gt;and&lt;/span&gt; null _  &lt;span class=&quot;hl opt&quot;&gt;=      []&lt;/span&gt;

&lt;span class=&quot;hl kwa&quot;&gt;let&lt;/span&gt; cons x xs &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt; x &lt;span class=&quot;hl opt&quot;&gt;::&lt;/span&gt; xs
&lt;span class=&quot;hl kwa&quot;&gt;and&lt;/span&gt; tail _ xs &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt;      xs
&lt;/pre&gt;

&lt;p&gt;Matías shows, for example, that intersection of two collection may be
defined as &lt;code&gt;merge null null tail cons tail&lt;/code&gt;, while symmetric
difference is &lt;code&gt;merge self self cons tail cons&lt;/code&gt;.&lt;/p&gt;

&lt;h3&gt;Parametricity : a bit too good to be true&lt;/h3&gt;

&lt;p&gt;I think it is not exactly true that &amp;quot;provided the output sequence must
be increasing, these four functions [self, null, cons and tail]
exhaust the possibilities by parametricity&amp;quot;: it seems that removing
some elements from the sequence &lt;code&gt;xs&lt;/code&gt; (but not all as &lt;code&gt;null&lt;/code&gt; and
&lt;code&gt;tail&lt;/code&gt; do) would preserve this sorting criterion.&lt;/p&gt;

&lt;p&gt;I'm not sure which is the best way to reduce the number of dimensions
of freedom of this problem to capture the behavior you want. Here are
some ideas, step by step.&lt;/p&gt;

&lt;p&gt;One may notice that all &lt;code&gt;lt/eq/gt&lt;/code&gt; operators return the &lt;code&gt;xs&lt;/code&gt; list
unchanged. The only question is whether or not they add their input
parameter. This suggests the following change in code, using new
combinators:&lt;/p&gt;

&lt;pre class=&quot;code-ocaml&quot;&gt;&lt;span class=&quot;hl kwa&quot;&gt;let&lt;/span&gt; merge ln rn lt eq gt &lt;span class=&quot;hl opt&quot;&gt;:&lt;/span&gt; 'a list &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; 'a list &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; 'a list &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt;
  &lt;span class=&quot;hl kwa&quot;&gt;let rec&lt;/span&gt; go l r &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt; &lt;span class=&quot;hl kwb&quot;&gt;match&lt;/span&gt; l&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; r &lt;span class=&quot;hl kwb&quot;&gt;with&lt;/span&gt;
  | &lt;span class=&quot;hl opt&quot;&gt;[],&lt;/span&gt; ys &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; ln ys
  | xs&lt;span class=&quot;hl opt&quot;&gt;, [] -&amp;gt;&lt;/span&gt; rn xs
  | x &lt;span class=&quot;hl opt&quot;&gt;::&lt;/span&gt; xs&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; y &lt;span class=&quot;hl opt&quot;&gt;::&lt;/span&gt; ys &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt;
    &lt;span class=&quot;hl kwb&quot;&gt;match&lt;/span&gt; compare x y &lt;span class=&quot;hl kwb&quot;&gt;with&lt;/span&gt;
    | &lt;span class=&quot;hl opt&quot;&gt;-&lt;/span&gt;&lt;span class=&quot;hl num&quot;&gt;1&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; lt x @ go xs r
    |  &lt;span class=&quot;hl num&quot;&gt;0&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; eq x @ go xs ys
    |  &lt;span class=&quot;hl num&quot;&gt;1&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; gt y @ go l  ys
    |  _ &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; &lt;span class=&quot;hl kwb&quot;&gt;assert&lt;/span&gt; &lt;span class=&quot;hl kwc&quot;&gt;false&lt;/span&gt;
  &lt;span class=&quot;hl kwa&quot;&gt;in&lt;/span&gt; go

&lt;span class=&quot;hl kwa&quot;&gt;let&lt;/span&gt; keep x &lt;span class=&quot;hl opt&quot;&gt;= [&lt;/span&gt;x&lt;span class=&quot;hl opt&quot;&gt;]&lt;/span&gt;
&lt;span class=&quot;hl kwa&quot;&gt;let&lt;/span&gt; drop x &lt;span class=&quot;hl opt&quot;&gt;= []&lt;/span&gt;
&lt;/pre&gt;

&lt;p&gt;Those new combinators are still not uniquely defined by parametricity:
&lt;code&gt;let duplicate x = [x; x]&lt;/code&gt; would preserve well-ordering of the
result. One solution could be to request those combinators to have an
&amp;quot;affine&amp;quot; type, where &amp;quot;affine&amp;quot; is a weak notion of linearity meaning
that they can either ignore their output, or use it only once. One may
also take as input a boolean &lt;code&gt;keep&lt;/code&gt;, and use &lt;code&gt;filter (const keep) [x]&lt;/code&gt;
– a sophisticated way to say that we use a boolean to choose between
the only two combinators of interest, &lt;code&gt;keep&lt;/code&gt; or &lt;code&gt;drop&lt;/code&gt;, which feels
a bit like cheating.&lt;/p&gt;

&lt;p&gt;Another remark on this change: it makes it possible to define
a tail-recursive variant of &lt;code&gt;merge&lt;/code&gt; without changing the interface
exposed to the user: instead of writing &lt;code&gt;lt x @ go xs r&lt;/code&gt;, one would
write &lt;code&gt;List.rev_append (lt x) acc&lt;/code&gt;, for &lt;code&gt;acc&lt;/code&gt; an accumulator variable
that gets reversed at the end. This is not possible with the previous
definition of &lt;code&gt;lt/eq/gt&lt;/code&gt; that allows them to inspect the whole list of
results of future calls.&lt;/p&gt;

&lt;p&gt;&lt;p/&gt;&lt;/p&gt;

&lt;p&gt;Regarding &lt;code&gt;ln/rn&lt;/code&gt;, one may avoid the pitfall of removing elements from
the lists by the same trick: not giving the combinators access to the
whole list at once.&lt;/p&gt;

&lt;pre class=&quot;code-ocaml&quot;&gt;&lt;span class=&quot;hl kwa&quot;&gt;let&lt;/span&gt; merge ln rn lt eq gt &lt;span class=&quot;hl opt&quot;&gt;:&lt;/span&gt; 'a list &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; 'a list &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; 'a list &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt;
  &lt;span class=&quot;hl kwa&quot;&gt;let rec&lt;/span&gt; go l r &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt; &lt;span class=&quot;hl kwb&quot;&gt;match&lt;/span&gt; l&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; r &lt;span class=&quot;hl kwb&quot;&gt;with&lt;/span&gt;
  | &lt;span class=&quot;hl opt&quot;&gt;[],&lt;/span&gt; ys &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; flatten &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;map ln ys&lt;span class=&quot;hl opt&quot;&gt;)&lt;/span&gt;
  | xs&lt;span class=&quot;hl opt&quot;&gt;, [] -&amp;gt;&lt;/span&gt; flatten &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;map rn xs&lt;span class=&quot;hl opt&quot;&gt;)&lt;/span&gt;
  | x &lt;span class=&quot;hl opt&quot;&gt;::&lt;/span&gt; xs&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; y &lt;span class=&quot;hl opt&quot;&gt;::&lt;/span&gt; ys &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt;
    &lt;span class=&quot;hl kwb&quot;&gt;match&lt;/span&gt; compare x y &lt;span class=&quot;hl kwb&quot;&gt;with&lt;/span&gt;
    | &lt;span class=&quot;hl opt&quot;&gt;-&lt;/span&gt;&lt;span class=&quot;hl num&quot;&gt;1&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; lt x @ go xs r
    |  &lt;span class=&quot;hl num&quot;&gt;0&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; eq x @ go xs ys
    |  &lt;span class=&quot;hl num&quot;&gt;1&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; gt y @ go l  ys
    |  _ &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; &lt;span class=&quot;hl kwb&quot;&gt;assert&lt;/span&gt; &lt;span class=&quot;hl kwc&quot;&gt;false&lt;/span&gt;
  &lt;span class=&quot;hl kwa&quot;&gt;in&lt;/span&gt; go
&lt;/pre&gt;

&lt;p&gt;With this definition (&lt;code&gt;flatten : 'a list list -&amp;gt; 'a list&lt;/code&gt;
may as well be named &lt;code&gt;join&lt;/code&gt;), &lt;code&gt;ln/rn&lt;/code&gt; use the same combinators as
&lt;code&gt;lt/eq/gt&lt;/code&gt;, at type &lt;code&gt;'a -&amp;gt; 'a list&lt;/code&gt;, which helps
simplifying matters. I think that using the affine-type restriction
mentioned above, one could then say that parametricity guarantees that
all the possibilities are the ones Matías describe.&lt;/p&gt;

&lt;p&gt;&lt;p/&gt;&lt;/p&gt;

&lt;p&gt;I have one last idea that is even stricter: I think the &lt;code&gt;ln&lt;/code&gt; and &lt;code&gt;rn&lt;/code&gt;
combinators may be an unnecessary level of freedom here. From the
point of view of a merging operator, there should be not much of
a difference between an empty list &lt;code&gt;[]&lt;/code&gt; and a list beginning with an
&lt;em&gt;infinite&lt;/em&gt; value, &lt;code&gt;∞::_&lt;/code&gt;. Indeed, if one imagines that &lt;code&gt;merge&lt;/code&gt; picks
the smallest element of both lists, such an infinity value will force
all the (non-infinite) elements of the other lists to be considered in
turn, a bit like the current code path for empty lists. We could
therefore handle the pattern&lt;/p&gt;

&lt;pre class=&quot;code-ocaml&quot;&gt;  | &lt;span class=&quot;hl opt&quot;&gt;[],&lt;/span&gt; ys &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt; ....&lt;/span&gt;
&lt;/pre&gt;

&lt;p&gt;as if it actually was something like&lt;/p&gt;

&lt;pre class=&quot;code-ocaml&quot;&gt;  | &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;∞&lt;span class=&quot;hl opt&quot;&gt;::&lt;/span&gt;_&lt;span class=&quot;hl opt&quot;&gt;),&lt;/span&gt; y&lt;span class=&quot;hl opt&quot;&gt;::&lt;/span&gt;ys &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt; ....&lt;/span&gt;
&lt;/pre&gt;

&lt;p&gt;which suggests the following right-hand-side
(knowing that &lt;code&gt;compare ∞ y &amp;gt; 0&lt;/code&gt;)&lt;/p&gt;

&lt;pre class=&quot;code-ocaml&quot;&gt;  | &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;∞&lt;span class=&quot;hl opt&quot;&gt;::&lt;/span&gt;_&lt;span class=&quot;hl opt&quot;&gt;),&lt;/span&gt; y&lt;span class=&quot;hl opt&quot;&gt;::&lt;/span&gt;ys &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; gt y @ go l ys
&lt;/pre&gt;

&lt;p&gt;This results in the following function:&lt;/p&gt;

&lt;pre class=&quot;code-ocaml&quot;&gt;&lt;span class=&quot;hl kwa&quot;&gt;let&lt;/span&gt; merge lt eq gt &lt;span class=&quot;hl opt&quot;&gt;:&lt;/span&gt; 'a list &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; 'a list &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; 'a list &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt;
  &lt;span class=&quot;hl kwa&quot;&gt;let rec&lt;/span&gt; go l r &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt; &lt;span class=&quot;hl kwb&quot;&gt;match&lt;/span&gt; l&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; r &lt;span class=&quot;hl kwb&quot;&gt;with&lt;/span&gt;
  | &lt;span class=&quot;hl opt&quot;&gt;[],      []      -&amp;gt; []&lt;/span&gt;
  | x &lt;span class=&quot;hl opt&quot;&gt;::&lt;/span&gt; xs&lt;span class=&quot;hl opt&quot;&gt;, []      -&amp;gt;&lt;/span&gt; lt x @ go xs r
  | &lt;span class=&quot;hl opt&quot;&gt;[],&lt;/span&gt;      y &lt;span class=&quot;hl opt&quot;&gt;::&lt;/span&gt; ys &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; gt y @ go l  ys
  | x &lt;span class=&quot;hl opt&quot;&gt;::&lt;/span&gt; xs&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; y &lt;span class=&quot;hl opt&quot;&gt;::&lt;/span&gt; ys &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt;
    &lt;span class=&quot;hl kwb&quot;&gt;match&lt;/span&gt; compare x y &lt;span class=&quot;hl kwb&quot;&gt;with&lt;/span&gt;
    | &lt;span class=&quot;hl opt&quot;&gt;-&lt;/span&gt;&lt;span class=&quot;hl num&quot;&gt;1&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; lt x @ go xs r
    |  &lt;span class=&quot;hl num&quot;&gt;0&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; eq x @ go xs ys
    |  &lt;span class=&quot;hl num&quot;&gt;1&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; gt y @ go l  ys
    |  _ &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; &lt;span class=&quot;hl kwb&quot;&gt;assert&lt;/span&gt; &lt;span class=&quot;hl kwc&quot;&gt;false&lt;/span&gt;
  &lt;span class=&quot;hl kwa&quot;&gt;in&lt;/span&gt; go
&lt;/pre&gt;

&lt;p&gt;This new definition is strictly less expressive as it only has 2^3
instead of 2^5 parametrizations. It is also less efficient as we
handle elements of the remaining list one by one (which can be
disastrous in particular in applications where &lt;code&gt;ys&lt;/code&gt;, a diff list, can
be expected to be much smaller than &lt;code&gt;xs&lt;/code&gt;, a data list), but we already
gave up on performances when moving to the &lt;code&gt;flatten (map ...)&lt;/code&gt; style –
a price we pay to look nice.&lt;/p&gt;

&lt;p&gt;The eight remaining behaviors are the following:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;keep keep keep (self self, cons cons cons) : A∪B
keep keep drop (null self, cons cons tail) : A
keep drop keep (self self, cons tail cons) : AΔB
keep drop drop (null self, cons tail tail) : A - B
drop keep keep (self null, tail cons cons) : B
drop keep drop (null null, tail cons tail) : A∩B
drop drop keep (self null, tail tail cons) : B - A
drop drop drop (null null, tail tail tail) : ∅
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;This captures all the operations Matías found most useful, but drops
(bad pun intended) the &amp;quot;remainders&amp;quot; &lt;code&gt;B/A&lt;/code&gt; and &lt;code&gt;A/B&lt;/code&gt;. My intuition is
that this restriction on the behavior of &lt;code&gt;ln/rn&lt;/code&gt; by &amp;quot;prolongation to
∞&amp;quot; enforces a kind of monotonicity property: all operators &lt;code&gt;O(A,B)&lt;/code&gt; so
defined satisfy the property that &lt;code&gt;O(A₁,B₁) @ O(A₂,B₂)&lt;/code&gt; is the same as
&lt;code&gt;O(A₁@A₂, B₁@B₂)&lt;/code&gt; if the biggest elements of &lt;code&gt;A₁&lt;/code&gt; and &lt;code&gt;B₁&lt;/code&gt; are smaller
than the smallest elements of &lt;code&gt;B₁&lt;/code&gt; and &lt;code&gt;B₂&lt;/code&gt; (&lt;code&gt;A₁ &amp;lt; A₂&lt;/code&gt; for example is
necessary for &lt;code&gt;A₁@A₂&lt;/code&gt; to be a valid sorted sequence). This property
does not hold for the &lt;code&gt;A/B&lt;/code&gt; operation.&lt;/p&gt;

&lt;h3&gt;Reasoning on the parametrization&lt;/h3&gt;

&lt;p&gt;I had one other remark on Matías' post. In fact, it is the only remark
I had after reading it, that got me motivated to write a reddit
comment; I caught the parametricity &amp;quot;bug&amp;quot; while re-reading the post
before commenting. It is nice to have a parametrization of set merges
fully worked on, but it would be even better if this parametrization
was actually helpful to reason about programs: can we express
transformation on the result of those operators in term of
transformations on the parameters?&lt;/p&gt;

&lt;p&gt;This is indeed the case. One can check that, if I call &lt;code&gt;O{lt,eq,gt}&lt;/code&gt;
the operator parametrized by the three &lt;code&gt;lt/eq/gt&lt;/code&gt; combinators, then
&lt;code&gt;O{l₁,e₁,g₁}(A,B) ∪ O{l₂,e₂,g₂}(A,B)&lt;/code&gt; is equal to
&lt;code&gt;O{l₁∨l₂,e₁∨e₂,g₁∨g₂}(A,B)&lt;/code&gt;, where &lt;code&gt;∨&lt;/code&gt; is the symmetrical operator
defined by:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;keep ∨ _    := keep
_    ∨ keep := keep
drop ∨ drop := drop
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;If one defines a &lt;code&gt;∧&lt;/code&gt; operator by swapping the role of &lt;code&gt;keep&lt;/code&gt; and
&lt;code&gt;drop&lt;/code&gt; in this definition, one get &lt;code&gt;O{l₁∧l₂,e₁∧e₂,g₁∧g₂}(A,B)&lt;/code&gt; equal
to &lt;code&gt;O{l₁,e₁,g₁}(A,B) ∩ O{l₂,e₂,g₂}(A,B)&lt;/code&gt;. There might be a general
definition of what &lt;code&gt;O{l,e,g}(O{l₁,e₁,g₁}(A,B), O{l₂,e₂,g₂}(A,B))&lt;/code&gt; is,
but this looks exhausting to sort out. Of course, the five-parameter
version of Matías certainly leads to related functional reasoning,
more expressive but also more tedious.&lt;/p&gt;

&lt;p&gt;&lt;p/&gt;&lt;/p&gt;

&lt;p&gt;That's it! Thanks for your patience, and stay tuned for more content
on Gagallium, after a guilty summer break.&lt;/p&gt;
</description><pubDate>18 Aug 2012 08:00:00 +0000</pubDate><category>code</category><category>merge</category><category>collection</category><category>sorted list</category><guid isPermaLink="true">http://gallium.inria.fr/~scherer/gagallium/comments-on-merge-right/index.html</guid></item></channel></rss>
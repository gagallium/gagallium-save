(* to be called just after with, to record backtraces *)
val register : exn -> unit

(* print them on the given channel *)
val print_backtraces_of : out_channel -> exn -> unit

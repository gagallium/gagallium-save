library(ggplot2)
# header = TRUE ignores the first line, check.names = FALSE allows '+' in 'C++'

benchmark <- read.table("bench1.dat", 
	header = TRUE, row.names = "Title")

# 't()' is matrix tranposition, 'beside = TRUE' separates the benchmarks,
# 'heat' provides nice colors

#pdf("graph.pdf", height=6, width=6)
png("bench1.png")
par(las=2)
par(oma=c(2,0,0,0))
bp <- barplot(as.matrix(benchmark), beside = FALSE, col = heat.colors(3), xaxt='n')
axis(1, at = bp, labels = colnames(benchmark), tick = FALSE, las = 1)

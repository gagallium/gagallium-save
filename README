# Description

This is the static content of Gagallium, a stog-generated blog.

The documentation for stog (from Maxence Guesdon) can be found there:
  http://zoggy.github.com/stog/doc.html

Basically, you should create a directory per blog post, and imitate
the structure of the already existing one; stog takes care of
gathering the articles etc.

The tmpl/ directory contains the template files that are used by stog/
for the HTML generation. The less/ directory contains the CSS files
(in the format of the ruby Less preprocessor) used for styling. Feel
free to tweak those files to get a fine-tuned global result.

Stog use the rather hackish but flexible Xtmpl template engine from
Maxence Guesdon. We currently use a "markdown" plugin to be able to
write parts of posts in the markdown syntax rather than HTML. Other
plugins could be implemented on demand if deemed necessary.


# Generating the blog

Use:
  make clean test

to generate a "test" version meant to be used locally; files are
generated in stog-output/, but use /tmp/stog-output as their base url,
so you should add a symlink from /tmp/stog-output to wherever you are.

Use:
  make clean site

to generate a "production" version meant to be pushed on the server;
the server URL is set in the Makefile (currently it's
http://gallium.inria.fr/~scherer/gagallium), but the files themselves
will still be put in stog-output/, it's your job to deploy them on the
remote server.

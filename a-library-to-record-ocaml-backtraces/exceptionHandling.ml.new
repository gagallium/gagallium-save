let minSize = 100
let smallFactor = (* 1/ *) 8
let decreaseRatio = 2
let increaseRatio = 4

exception Ex
let ex = Ex

let dummy_bt =
  try raise Ex with Ex -> Printexc.get_raw_backtrace ()

let backtraces = ref (Array.create minSize dummy_bt)
let exns = ref (Weak.create minSize)
let nextPtr = ref 0
let lock = ref false

let increase () =
  let old_exns = !exns in
  let old_backtraces = !backtraces in
  let sz = !nextPtr * increaseRatio in
  exns := Weak.create sz;
  Weak.blit old_exns 0 !exns 0 !nextPtr;
  backtraces := Array.create sz dummy_bt;
  Array.blit old_backtraces 0 !backtraces 0 !nextPtr

let gc () =
  if not !lock then (* otherwise, we'll try next time *)
    begin
      lock := true;
      let srcBacktraces, srcExns = !backtraces, !exns in
      let decrease = !nextPtr * smallFactor < Array.length !backtraces && minSize < Array.length !backtraces in
      let destBacktraces, destExns =
        if decrease then
          let sz = max minSize (decreaseRatio * !nextPtr) in 
          Array.create sz dummy_bt, Weak.create sz
        else
          !backtraces, !exns
      in
      let n = !nextPtr in
      nextPtr := 0;
      for i = 0 to n - 1 do
        match Weak.get srcExns i with
	| (Some _) as exn ->
	    Weak.set destExns !nextPtr exn;
	    destBacktraces.(!nextPtr) <- srcBacktraces.(i);
	    incr nextPtr
        | None -> ()
      done;
      if decrease then
        begin
          backtraces := destBacktraces;
          exns := destExns
        end;
      lock := false
    end

let _ = Gc.create_alarm gc

let register e =
  if Printexc.backtrace_status () then
    begin
      let bt = Printexc.get_raw_backtrace () in
      (* killing "Re-raised" in backtraces *)
      begin try raise ex with Ex -> () end;
      lock := true;
      if !nextPtr = Array.length !backtraces then increase ();
      Weak.set !exns !nextPtr (Some e);
      !backtraces.(!nextPtr) <- bt;
      incr nextPtr;
      lock := false
    end

let get_backtraces_of exn =
  lock := true;
  let res = ref [] in
  for i = 0 to !nextPtr - 1 do
    match Weak.get !exns i with
    | Some exn' when exn' == exn ->
	res := !backtraces.(i) :: !res
    | _ -> ()
  done;
  lock := false;
  List.rev !res

let print_backtraces_of chan exn =
  Printf.fprintf chan "%s" (String.concat "----------\n" (List.map Printexc.raw_backtrace_to_string (get_backtraces_of exn)))

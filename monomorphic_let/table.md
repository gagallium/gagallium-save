| Package        | Number of files | Failed files | Ratio | Errors/Line |
|----------------+-----------------+--------------+-------+-------------|
| OCaml          | 488             | 20           | 4%    | 1/7000      |
| LablGTK        | 101             | 7            | 7%    | 1/3000      |
| Oasis setup.ml | 1               | 1            | 100%  | 1/6000      |
| Sexplib        | 12              | 1            | 8%    | 1/3500      |
| Variantslib    | 2               | 1            | 50%   | 1/500       |
| Core_kernel    | 170             | 16           | 9%    | 1/1800      |
| Core           | 178             | 4            | 2%    | 1/4000      |
| Async_core     | 36              | 4            | 11%   | 1/1600      |
| Async_unix     | 31              | 7            | 23%   | 1/1000      |
| Async_extra    | 22              | 2            | 9%    | 1/3000      |
|                |                 |              |       |             |
| Total          | 1041            | 63           | 6%    | 1/1700      |

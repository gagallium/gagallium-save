<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0"><channel><title>Gagallium: continuations</title><link>http://gallium.inria.fr/~scherer/gagallium/kw_continuations.html</link><description>A draft blog at Gallium</description><managingEditor>gabriel.scherer@inria.fr</managingEditor><pubDate>01 Dec 2012 08:00:00 +0000</pubDate><lastBuildDate>01 Dec 2012 08:00:00 +0000</lastBuildDate><generator>Stog</generator><item><title>Fixing stack overflows using continuations</title><link>http://gallium.inria.fr/~scherer/gagallium/stack-overflow-continuations/index.html</link><description>&lt;p&gt;Just a few weeks ago, Andrej Bauer wrote two articles (
&lt;a href=&quot;http://math.andrej.com/2012/11/08/how-to-implement-dependent-type-theory-i/&quot;&gt;1&lt;/a&gt;
&lt;a href=&quot;http://math.andrej.com/2012/11/11/how-to-implement-dependent-type-theory-ii/&quot;&gt;2&lt;/a&gt;
) about implementing a dependent type theory in Ocaml. In particular, he
demonstrates a cool idea to normalize expressions, called
« normalization by evaluation ».&lt;/p&gt;

&lt;p&gt;However, when trying to evaluate 2&lt;sup&gt;20&lt;/sup&gt;, the program overflows the
stack.  It turns out that there is an easy and methodic way to locate
and fix such problems, which we will discuss in that post.&lt;/p&gt;




&lt;p&gt;Edit: there is now a &lt;a href=&quot;http://math.andrej.com/2012/11/29/how-to-implement-dependent-type-theory-iii/&quot;&gt;third
article&lt;/a&gt;
published in the series, but the code presented here is still based on
the &lt;a href=&quot;https://github.com/andrejbauer/tt/tree/blog-part-II&quot;&gt;Part II
branch&lt;/a&gt; of Andrej's
&lt;a href=&quot;https://github.com/andrejbauer/tt&quot;&gt;git repository&lt;/a&gt;.&lt;/p&gt;

&lt;p&gt;First, let's examine a similar, working example. From Andrej's test, we can
compute 2&lt;sup&gt;16&lt;/sup&gt;:&lt;/p&gt;

&lt;div&gt;


&lt;pre class=&quot;code-txt&quot;&gt;$ make native &amp;amp;&amp;amp; echo &amp;quot;Eval power two (power two four).&amp;quot; | ./tt.native -l church.tt
ocamlbuild -lib unix -use-menhir tt.native
Finished, 38 targets (38 cached) in 00:00:00.
tt master
[Type Ctrl-D to exit or &amp;quot;Help.&amp;quot; for help.]
#     = fun A : Type 0 =&amp;gt;
      fun x : A -&amp;gt; A =&amp;gt;
      fun x0 : A =&amp;gt;
      x
      (x
       (x
        (x
         (x
          (x
           (x
            (x
             (x
              (x
               (x
                (x
                 (x
                  (x
                   (x
                    (x
                     (x
                      (x
                       (x
                        (x
                         (x
                          (x
                           (x
                            (x
                             (x
                              (x
                               (x (x (x (x (x (x (x (x (x (... (...))))))))))))))))))))))))))))))))))))
    : forall A : Type 0, (A -&amp;gt; A) -&amp;gt; A -&amp;gt; A
&lt;/pre&gt;


&lt;/div&gt;

&lt;p&gt;This is just the regular Church-encoding of a huge number: you can think of A
as the natural numbers type, of &lt;code&gt;x&lt;/code&gt; as the successor constructor, and &lt;code&gt;x0&lt;/code&gt; as zero.
Notice how the result is a very deeply-nested sequence of applications. The
shape being pretty-printed here relates closely to the manipulated datatype,
and as we will see, it is the source of the overflow.&lt;/p&gt;

&lt;p&gt;Let us now modify the Makefile to add debug information to get a proper
backtrace:&lt;/p&gt;

&lt;div&gt;


&lt;pre class=&quot;code-txt&quot;&gt; native:
-  ocamlbuild -lib unix -use-menhir tt.native
+  ocamlbuild -tag annot -tag debug -lib unix -use-menhir tt.native
&lt;/pre&gt;


&lt;/div&gt;

&lt;p&gt;Rebuilding and running it on 2&lt;sup&gt;20&lt;/sup&gt;
(setting &lt;code&gt;OCAMLRUNPARAM=&amp;quot;b&amp;quot;&lt;/code&gt; to print the backtrace when the
stack overflows):&lt;/p&gt;

&lt;div&gt;


&lt;pre class=&quot;code-txt&quot;&gt;$ make native &amp;amp;&amp;amp; echo &amp;quot;Eval power two (times four five).&amp;quot; | OCAMLRUNPARAM=&amp;quot;b&amp;quot; ./tt.native -l church.tt
ocamlbuild -tag annot -tag debug -lib unix -use-menhir tt.native
Finished, 38 targets (15 cached) in 00:00:00.
tt master
[Type Ctrl-D to exit or &amp;quot;Help.&amp;quot; for help.]
# Fatal error: exception Stack_overflow
Raised by primitive operation at file &amp;quot;value.ml&amp;quot;, line 74, characters 33-43
Called from file &amp;quot;value.ml&amp;quot;, line 74, characters 33-43
Called from file &amp;quot;value.ml&amp;quot;, line 90, characters 47-54
Called from file &amp;quot;value.ml&amp;quot;, line 74, characters 33-43
Called from file &amp;quot;value.ml&amp;quot;, line 90, characters 47-54
Called from file &amp;quot;value.ml&amp;quot;, line 74, characters 33-43
Called from file &amp;quot;value.ml&amp;quot;, line 90, characters 47-54
Called from file &amp;quot;value.ml&amp;quot;, line 74, characters 33-43
[...]
&lt;/pre&gt;


&lt;/div&gt;

&lt;p&gt;The problem here is that the code tries to reify the result whose body contains the deep nesting of application nodes. When recursively processing a term, the call stack is proportional to the &amp;quot;height&amp;quot; of the term as a tree; our result here is a tree that is deeply skewed on the right, with height around 2&lt;sup&gt;20&lt;/sup&gt;. Location information tells us which recursive processing step fails (two mutually recursive calls), and the guilty function here is the following:&lt;/p&gt;

&lt;div&gt;


&lt;pre class=&quot;code-ocaml&quot;&gt;&lt;span class=&quot;hl kwa&quot;&gt;and&lt;/span&gt; reify_abstraction &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;x&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; t&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; f&lt;span class=&quot;hl opt&quot;&gt;) =&lt;/span&gt;
  &lt;span class=&quot;hl kwa&quot;&gt;let&lt;/span&gt; x &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt; &lt;span class=&quot;hl kwd&quot;&gt;Syntax&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;.&lt;/span&gt;refresh x &lt;span class=&quot;hl kwa&quot;&gt;in&lt;/span&gt;
  &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;x&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; reify t&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; reify &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;f &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;&lt;span class=&quot;hl kwd&quot;&gt;Neutral&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;&lt;span class=&quot;hl kwd&quot;&gt;Var&lt;/span&gt; x&lt;span class=&quot;hl opt&quot;&gt;))))&lt;/span&gt;
&lt;/pre&gt;


&lt;/div&gt;

&lt;p&gt;It appears that the call to &lt;code&gt;f (Neutral (Var x))&lt;/code&gt; blows the stack. To
solve the problem (or at least this part of the problem), we must
somehow put this call in tail-call position; it is currently
surrounded by some post-treatment that builds the &lt;code&gt;(x, reify t, ...)&lt;/code&gt;
triple from the result of the call.&lt;/p&gt;

&lt;p&gt;The idea of using continuation-passing-style (CPS) is to bluntly put
the call in tail-call position, and pass it the &amp;quot;post-treatment&amp;quot; to be
done as a function (continuation) called by &lt;code&gt;f&lt;/code&gt; itself when it
produces its result. &amp;quot;Don't call us, we'll call you&amp;quot;: this is an
inversion of control.&lt;/p&gt;

&lt;div&gt;


&lt;pre class=&quot;code-ocaml&quot;&gt;&lt;span class=&quot;hl kwa&quot;&gt;and&lt;/span&gt; reify_abstraction &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;x&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; t&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; f&lt;span class=&quot;hl opt&quot;&gt;)&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt;
  &lt;span class=&quot;hl kwa&quot;&gt;let&lt;/span&gt; x &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt; &lt;span class=&quot;hl kwd&quot;&gt;Syntax&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;.&lt;/span&gt;refresh x &lt;span class=&quot;hl kwa&quot;&gt;in&lt;/span&gt;
  f &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;&lt;span class=&quot;hl kwd&quot;&gt;Neutral&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;&lt;span class=&quot;hl kwd&quot;&gt;Var&lt;/span&gt; x&lt;span class=&quot;hl opt&quot;&gt;)) (&lt;/span&gt;&lt;span class=&quot;hl kwa&quot;&gt;fun&lt;/span&gt; body &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;x&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; reify t&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; reify body&lt;span class=&quot;hl opt&quot;&gt;))&lt;/span&gt;
&lt;/pre&gt;


&lt;/div&gt;

&lt;p&gt;For typing reason (the reification is polymorphic), this simple style
doesn't quite work here, we must explicitly represent the continuation
value with a first-class polymorphic type (as a record field);&lt;/p&gt;

&lt;div&gt;


&lt;pre class=&quot;code-ocaml&quot;&gt;&lt;span class=&quot;hl kwa&quot;&gt;and&lt;/span&gt; reify_abstraction &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;x&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; t&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; f&lt;span class=&quot;hl opt&quot;&gt;)&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt;
  &lt;span class=&quot;hl kwa&quot;&gt;let&lt;/span&gt; x &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt; &lt;span class=&quot;hl kwd&quot;&gt;Syntax&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;.&lt;/span&gt;refresh x &lt;span class=&quot;hl kwa&quot;&gt;in&lt;/span&gt;
  &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;f &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;&lt;span class=&quot;hl kwd&quot;&gt;Neutral&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;&lt;span class=&quot;hl kwd&quot;&gt;Var&lt;/span&gt; x&lt;span class=&quot;hl opt&quot;&gt;))).&lt;/span&gt;call
    &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;&lt;span class=&quot;hl kwa&quot;&gt;fun&lt;/span&gt; body &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;x&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; reify t&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; reify body&lt;span class=&quot;hl opt&quot;&gt;))&lt;/span&gt;
&lt;/pre&gt;


&lt;/div&gt;

&lt;p&gt;The type &lt;code&gt;abstraction&lt;/code&gt; accepted by the function needs to be changed from:&lt;/p&gt;

&lt;div&gt;


&lt;pre class=&quot;code-ocaml&quot;&gt;&lt;span class=&quot;hl kwa&quot;&gt;and&lt;/span&gt; abstraction &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt; variable &lt;span class=&quot;hl opt&quot;&gt;*&lt;/span&gt; &lt;span class=&quot;hl kwa&quot;&gt;value&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;* (&lt;/span&gt;&lt;span class=&quot;hl kwa&quot;&gt;value&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; &lt;span class=&quot;hl kwa&quot;&gt;value&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;)&lt;/span&gt;
&lt;/pre&gt;


&lt;/div&gt;

&lt;p&gt;into:&lt;/p&gt;

&lt;div&gt;


&lt;pre class=&quot;code-ocaml&quot;&gt;&lt;span class=&quot;hl kwa&quot;&gt;and&lt;/span&gt; abstraction &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt;
  variable &lt;span class=&quot;hl opt&quot;&gt;*&lt;/span&gt; &lt;span class=&quot;hl kwa&quot;&gt;value&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;* (&lt;/span&gt;&lt;span class=&quot;hl kwa&quot;&gt;value&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; &lt;span class=&quot;hl kwa&quot;&gt;value&lt;/span&gt; continuation&lt;span class=&quot;hl opt&quot;&gt;)&lt;/span&gt;

&lt;span class=&quot;hl kwa&quot;&gt;and&lt;/span&gt; 'a continuation &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;{&lt;/span&gt; call &lt;span class=&quot;hl opt&quot;&gt;:&lt;/span&gt; 'b &lt;span class=&quot;hl opt&quot;&gt;.&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;'a &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; 'b&lt;span class=&quot;hl opt&quot;&gt;)&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; 'b &lt;span class=&quot;hl opt&quot;&gt;}&lt;/span&gt;
&lt;/pre&gt;


&lt;/div&gt;

&lt;p&gt;We have to fix some parts of the program to adapt to that change. When
we before used an abstraction body directly as a function, &lt;code&gt;f x&lt;/code&gt; we
now need to pass it a continuation. In &lt;code&gt;reify_abstraction&lt;/code&gt; we were
carefull to pass just the right continuation to have a tail-call, but
the other uses are not critical for our input, so we can just pass
them the identity continuation, that immediately returns the result.&lt;/p&gt;

&lt;p&gt;For example in &lt;code&gt;equal_abstraction&lt;/code&gt;,&lt;/p&gt;

&lt;div&gt;


&lt;pre class=&quot;code-ocaml&quot;&gt; &lt;span class=&quot;hl kwa&quot;&gt;and&lt;/span&gt; equal_abstraction &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;x&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; v1&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; f1&lt;span class=&quot;hl opt&quot;&gt;) (&lt;/span&gt;_&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; v2&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; f2&lt;span class=&quot;hl opt&quot;&gt;) =&lt;/span&gt;
   equal v1 v2 &lt;span class=&quot;hl opt&quot;&gt;&amp;amp;&amp;amp;&lt;/span&gt;
    &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;&lt;span class=&quot;hl kwa&quot;&gt;let&lt;/span&gt; x &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt; &lt;span class=&quot;hl kwd&quot;&gt;Neutral&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;&lt;span class=&quot;hl kwd&quot;&gt;Var&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;&lt;span class=&quot;hl kwd&quot;&gt;Syntax&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;.&lt;/span&gt;refresh x&lt;span class=&quot;hl opt&quot;&gt;))&lt;/span&gt; &lt;span class=&quot;hl kwa&quot;&gt;in&lt;/span&gt;
      equal &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;f1 x&lt;span class=&quot;hl opt&quot;&gt;) (&lt;/span&gt;f2 x&lt;span class=&quot;hl opt&quot;&gt;))&lt;/span&gt;
&lt;/pre&gt;


&lt;/div&gt;

&lt;p&gt;The last line becomes:&lt;/p&gt;

&lt;div&gt;


&lt;pre class=&quot;code-ocaml&quot;&gt;      equal &lt;span class=&quot;hl opt&quot;&gt;((&lt;/span&gt;f1 x&lt;span class=&quot;hl opt&quot;&gt;).&lt;/span&gt;call id&lt;span class=&quot;hl opt&quot;&gt;) ((&lt;/span&gt;f2 x&lt;span class=&quot;hl opt&quot;&gt;).&lt;/span&gt;call id&lt;span class=&quot;hl opt&quot;&gt;))&lt;/span&gt;
&lt;/pre&gt;


&lt;/div&gt;

&lt;p&gt;The three other changes can be seen in &lt;a href=&quot;https://github.com/gasche/tt/commit/3bd35cdb0d98b0794485e432f3b0b132a029df9f&quot;&gt;the corresponding commit&lt;/a&gt;.&lt;/p&gt;

&lt;p&gt;Let's put this to test:&lt;/p&gt;

&lt;div&gt;


&lt;pre class=&quot;code-txt&quot;&gt;$ make native &amp;amp;&amp;amp; echo &amp;quot;Eval power two (times four five).&amp;quot; | OCAMLRUNPARAM=&amp;quot;b&amp;quot; ./tt.native -l church.tt
ocamlbuild -tag annot -tag debug -lib unix -use-menhir tt.native
Finished, 38 targets (30 cached) in 00:00:00.
tt master
[Type Ctrl-D to exit or &amp;quot;Help.&amp;quot; for help.]
# Fatal error: exception Stack_overflow
Raised at file &amp;quot;list.ml&amp;quot;, line 157, characters 19-30
Called from file &amp;quot;value.ml&amp;quot;, line 78, characters 33-43
Called from file &amp;quot;value.ml&amp;quot;, line 95, characters 47-54
Called from file &amp;quot;value.ml&amp;quot;, line 78, characters 33-43
Called from file &amp;quot;value.ml&amp;quot;, line 95, characters 47-54
Called from file &amp;quot;value.ml&amp;quot;, line 78, characters 33-43
Called from file &amp;quot;value.ml&amp;quot;, line 95, characters 47-54
Called from file &amp;quot;value.ml&amp;quot;, line 78, characters 33-43
[...]
&lt;/pre&gt;


&lt;/div&gt;

&lt;p&gt;Not there yet: we have made one particular part of the program tail-recursive, but we still need to fix the rest. The next problem arises in the treatment of the right-hand side (RHS) of application:&lt;/p&gt;

&lt;div&gt;


&lt;pre class=&quot;code-ocaml&quot;&gt;&lt;span class=&quot;hl kwa&quot;&gt;and&lt;/span&gt; reify_neutral' &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt; &lt;span class=&quot;hl kwa&quot;&gt;function&lt;/span&gt;
  | &lt;span class=&quot;hl kwd&quot;&gt;Var&lt;/span&gt; x &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; &lt;span class=&quot;hl kwd&quot;&gt;Syntax&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;.&lt;/span&gt;&lt;span class=&quot;hl kwd&quot;&gt;Var&lt;/span&gt; x
  | &lt;span class=&quot;hl kwd&quot;&gt;App&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;n&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; v&lt;span class=&quot;hl opt&quot;&gt;) -&amp;gt;&lt;/span&gt; &lt;span class=&quot;hl kwd&quot;&gt;Syntax&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;.&lt;/span&gt;&lt;span class=&quot;hl kwd&quot;&gt;App&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;reify_neutral n&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; reify v&lt;span class=&quot;hl opt&quot;&gt;)&lt;/span&gt;
&lt;/pre&gt;


&lt;/div&gt;

&lt;p&gt;As the problem on our particular input comes from the right arguments
of application nodes, the call we would like to make tail-recursive
here is &lt;code&gt;reify v&lt;/code&gt;.&lt;/p&gt;

&lt;p&gt;This part of the program is less complicated from a typing point of
view, so we don't need the explicit record wrapping for
polymorphism. However, the fix is larger as we we are in the middle of
several mutually recursive functions (&lt;code&gt;reify&lt;/code&gt;, &lt;code&gt;reify'&lt;/code&gt;,
&lt;code&gt;reify_neutral'&lt;/code&gt;, &lt;code&gt;reify_neutral&lt;/code&gt;) that all need to be made CPS
somehow.&lt;/p&gt;

&lt;p&gt;Fortunately, this can be done step-by-step. First turn this &lt;code&gt;reify v&lt;/code&gt;
call in a tail-call to a yet-to-be-defined &lt;code&gt;reify_cont&lt;/code&gt; function:&lt;/p&gt;

&lt;div&gt;


&lt;pre class=&quot;code-ocaml&quot;&gt;| &lt;span class=&quot;hl kwd&quot;&gt;App&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;n&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; v&lt;span class=&quot;hl opt&quot;&gt;) -&amp;gt;&lt;/span&gt;
    reify_cont v &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;&lt;span class=&quot;hl kwa&quot;&gt;fun&lt;/span&gt; v &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; &lt;span class=&quot;hl kwd&quot;&gt;Syntax&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;.&lt;/span&gt;&lt;span class=&quot;hl kwd&quot;&gt;App&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;reify_neutral n&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; v&lt;span class=&quot;hl opt&quot;&gt;))&lt;/span&gt;
&lt;/pre&gt;


&lt;/div&gt;

&lt;p&gt;The simplest way to define &lt;code&gt;reify_cont v ret&lt;/code&gt; (where &lt;code&gt;ret&lt;/code&gt; is the name
of the continuation, of which we can think of as a &amp;quot;return&amp;quot;) is the
following:&lt;/p&gt;

&lt;div&gt;


&lt;pre class=&quot;code-ocaml&quot;&gt;&lt;span class=&quot;hl kwa&quot;&gt;and&lt;/span&gt; reify_cont v ret &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt; ret &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;reify v&lt;span class=&quot;hl opt&quot;&gt;)&lt;/span&gt;
&lt;/pre&gt;


&lt;/div&gt;

&lt;p&gt;Of course, this definition doesn't solve anything, as it's no more tail-recursive than the initial version. But we can iteratively refine this definition to expand the continuation-passing-style.&lt;/p&gt;

&lt;p&gt;First, inline the definition of &lt;code&gt;reify&lt;/code&gt;:&lt;/p&gt;

&lt;div&gt;


&lt;pre class=&quot;code-ocaml&quot;&gt;&lt;span class=&quot;hl kwa&quot;&gt;and&lt;/span&gt; reify_cont v ret &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt;
  ret &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;&lt;span class=&quot;hl kwd&quot;&gt;Syntax&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;.&lt;/span&gt;nowhere &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;reify' v&lt;span class=&quot;hl opt&quot;&gt;))&lt;/span&gt;
&lt;/pre&gt;


&lt;/div&gt;

&lt;p&gt;Then turn this into a tail-call to a new &lt;code&gt;reify'_cont&lt;/code&gt; function,
naively defined. Note how the continuation passed to &lt;code&gt;reify'_cont&lt;/code&gt;
first applies the post-treatment of the &lt;code&gt;reify&lt;/code&gt; function, then call
the passed-in continuation &lt;code&gt;ret&lt;/code&gt;.&lt;/p&gt;

&lt;div&gt;


&lt;pre class=&quot;code-ocaml&quot;&gt;&lt;span class=&quot;hl kwa&quot;&gt;and&lt;/span&gt; reify_cont v ret &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt;
  reify'_cont v &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;&lt;span class=&quot;hl kwa&quot;&gt;fun&lt;/span&gt; v &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; ret &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;&lt;span class=&quot;hl kwd&quot;&gt;Syntax&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;.&lt;/span&gt;nowhere v&lt;span class=&quot;hl opt&quot;&gt;))&lt;/span&gt;

&lt;span class=&quot;hl kwa&quot;&gt;and&lt;/span&gt; reify'_cont v ret &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt; ret &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;reify' v&lt;span class=&quot;hl opt&quot;&gt;)&lt;/span&gt;
&lt;/pre&gt;


&lt;/div&gt;

&lt;p&gt;It should now be routine: inline the call, then specialize the
definition to expose a tail-call.&lt;/p&gt;

&lt;div&gt;


&lt;pre class=&quot;code-ocaml&quot;&gt;&lt;span class=&quot;hl kwa&quot;&gt;and&lt;/span&gt; reify'_cont v ret &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt; &lt;span class=&quot;hl kwb&quot;&gt;match&lt;/span&gt; v &lt;span class=&quot;hl kwb&quot;&gt;with&lt;/span&gt;
  | &lt;span class=&quot;hl kwd&quot;&gt;Universe&lt;/span&gt; _ | &lt;span class=&quot;hl kwd&quot;&gt;Pi&lt;/span&gt; _ | &lt;span class=&quot;hl kwd&quot;&gt;Lambda&lt;/span&gt; _ &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; ret &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;reify' v&lt;span class=&quot;hl opt&quot;&gt;)&lt;/span&gt;
  | &lt;span class=&quot;hl kwd&quot;&gt;Neutral&lt;/span&gt; n &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; reify_neutral'_cont n ret
&lt;/pre&gt;


&lt;/div&gt;

&lt;p&gt;Note that we have selectively handled the &lt;code&gt;Neutral&lt;/code&gt; case, which is
where the stack overflow happens and needs to be made tail-rec, while
all the other cases reuse the non-CPS &lt;code&gt;reify'&lt;/code&gt; function: adding CPS
versions of our code duplicate some logic, but only in the code paths
that need to be stack-preserving.&lt;/p&gt;

&lt;p&gt;The definition of &lt;code&gt;reify_neutral'_cont&lt;/code&gt; again follows the same
principle, but chains together the CPS versions of &lt;code&gt;reify&lt;/code&gt; and
&lt;code&gt;reify_neutral&lt;/code&gt;:&lt;/p&gt;

&lt;div&gt;


&lt;pre class=&quot;code-ocaml&quot;&gt;&lt;span class=&quot;hl kwa&quot;&gt;and&lt;/span&gt; reify_neutral'_cont n ret &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt; &lt;span class=&quot;hl kwb&quot;&gt;match&lt;/span&gt; n &lt;span class=&quot;hl kwb&quot;&gt;with&lt;/span&gt;
  | &lt;span class=&quot;hl kwd&quot;&gt;Var&lt;/span&gt; x &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; ret &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;&lt;span class=&quot;hl kwd&quot;&gt;Syntax&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;.&lt;/span&gt;&lt;span class=&quot;hl kwd&quot;&gt;Var&lt;/span&gt; x&lt;span class=&quot;hl opt&quot;&gt;)&lt;/span&gt;
  | &lt;span class=&quot;hl kwd&quot;&gt;App&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;n&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; v&lt;span class=&quot;hl opt&quot;&gt;) -&amp;gt;&lt;/span&gt;
    reify_cont v &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;&lt;span class=&quot;hl kwa&quot;&gt;fun&lt;/span&gt; v &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt;
      reify_neutral_cont n &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;&lt;span class=&quot;hl kwa&quot;&gt;fun&lt;/span&gt; n &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt;
        ret &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;&lt;span class=&quot;hl kwd&quot;&gt;Syntax&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;.&lt;/span&gt;&lt;span class=&quot;hl kwd&quot;&gt;App&lt;/span&gt; &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;n&lt;span class=&quot;hl opt&quot;&gt;,&lt;/span&gt; v&lt;span class=&quot;hl opt&quot;&gt;))))&lt;/span&gt;

&lt;span class=&quot;hl kwa&quot;&gt;and&lt;/span&gt; reify_neutral_cont n ret &lt;span class=&quot;hl opt&quot;&gt;=&lt;/span&gt;
  reify_neutral'_cont n &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;&lt;span class=&quot;hl kwa&quot;&gt;fun&lt;/span&gt; n &lt;span class=&quot;hl opt&quot;&gt;-&amp;gt;&lt;/span&gt; ret &lt;span class=&quot;hl opt&quot;&gt;(&lt;/span&gt;&lt;span class=&quot;hl kwd&quot;&gt;Syntax&lt;/span&gt;&lt;span class=&quot;hl opt&quot;&gt;.&lt;/span&gt;nowhere n&lt;span class=&quot;hl opt&quot;&gt;))&lt;/span&gt;
&lt;/pre&gt;


&lt;/div&gt;

&lt;p&gt;This closes the loop: we have made a &lt;code&gt;_cont&lt;/code&gt; version of all the
functions involved in the mutual recursion that overflows the
stack. And they were derived from their non-CPS definition in an
simple and methodic way.&lt;/p&gt;

&lt;p&gt;Of course, this is still not enough:&lt;/p&gt;

&lt;div&gt;


&lt;pre class=&quot;code-txt&quot;&gt;$ make native &amp;amp;&amp;amp; echo &amp;quot;Eval power two (times four five).&amp;quot; | OCAMLRUNPARAM=&amp;quot;b&amp;quot; ./tt.native -l church.tt
ocamlbuild -tag annot -tag debug -lib unix -use-menhir tt.native
Finished, 38 targets (30 cached) in 00:00:00.
tt master
[Type Ctrl-D to exit or &amp;quot;Help.&amp;quot; for help.]
#     = Fatal error: exception Stack_overflow
Raised at file &amp;quot;pervasives.ml&amp;quot;, line 149, characters 10-33
Called from file &amp;quot;beautify.ml&amp;quot;, line 64, characters 14-41
Called from file &amp;quot;beautify.ml&amp;quot;, line 53, characters 27-56
Called from file &amp;quot;beautify.ml&amp;quot;, line 64, characters 14-41
Called from file &amp;quot;beautify.ml&amp;quot;, line 53, characters 27-56
[...]
&lt;/pre&gt;


&lt;/div&gt;

&lt;p&gt;This time, we manage to reify the result, but encounter a new overflow in the
part of the code that prepares the result for pretty-printing, more
specifically and as expected, in the handling of the RHS of applications.&lt;/p&gt;

&lt;p&gt;The same transformation method into CPS applies here again, as well as
one step further in the pretty-printer code. Fixing each part of the
code in this methodic way, if a bit repetitive, is rather quick.&lt;/p&gt;

&lt;p&gt;The commits associated to the fixes described in this post can be found on
Gabriel Scherer's &lt;a href=&quot;https://github.com/gasche/tt/commits/master&quot;&gt;github
repository&lt;/a&gt;.&lt;/p&gt;

&lt;p&gt;Once this is done, we finally get a result (whose pretty-printing is
not that interesting):&lt;/p&gt;

&lt;div&gt;


&lt;pre class=&quot;code-txt&quot;&gt;$ make native &amp;amp;&amp;amp; echo &amp;quot;Eval power two (times four five).&amp;quot; | OCAMLRUNPARAM=&amp;quot;b&amp;quot; ./tt.native -l church.tt                                                                                                       ocamlbuild -tag annot -tag debug -lib unix -use-menhir tt.native
Finished, 38 targets (28 cached) in 00:00:00.
tt master
[Type Ctrl-D to exit or &amp;quot;Help.&amp;quot; for help.]
#     = fun A : Type 0 =&amp;gt;
      fun x : A -&amp;gt; A =&amp;gt;
      fun x0 : A =&amp;gt;
      x
      (x
       (x
        (x
         (x
          (x
           (x
            (x
             (x
              (x
               (x
                (x
                 (x
                  (x
                   (x
                    (x
                     (x
                      (x
                       (x
                        (x
                         (x
                          (x
                           (x
                            (x
                             (x
                              (x
                               (x (x (x (x (x (x (x (x (x (... (...))))))))))))))))))))))))))))))))))))
    : forall A : Type 0, (A -&amp;gt; A) -&amp;gt; A -&amp;gt; A
# %
&lt;/pre&gt;


&lt;/div&gt;
</description><pubDate>01 Dec 2012 08:00:00 +0000</pubDate><category>ocaml</category><category>stack overflow</category><category>debugging</category><category>ocaml</category><category>backtrace</category><category>continuations</category><guid isPermaLink="true">http://gallium.inria.fr/~scherer/gagallium/stack-overflow-continuations/index.html</guid></item></channel></rss>
<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0"><channel><title>Gagallium: abstract types</title><link>http://gallium.inria.fr/~scherer/gagallium/kw_abstract+types.html</link><description>A draft blog at Gallium</description><managingEditor>gabriel.scherer@inria.fr</managingEditor><pubDate>16 Oct 2012 08:00:00 +0000</pubDate><lastBuildDate>16 Oct 2012 08:00:00 +0000</lastBuildDate><generator>Stog</generator><item><title>Happy Ada Lovelace Day</title><link>http://gallium.inria.fr/~scherer/gagallium/ada-lovelace-day-2012/index.html</link><description>&lt;p&gt;Tuesday October 16 is an excellent day for a lot of truly excellent
reasons. The one of concern here is that it was chosen to be the Ada
Lovelace Day, whose purpose is to celebrate important women in one's
scientific field.&lt;/p&gt;




&lt;p&gt;(A recent ESOP submission has left me enthusiastic about exotic time
zones. I'm aiming to publish this article on Octobre 16 before 23:59
GMT-11, which might look like the following day to some readers,
including those living in &lt;a href=&quot;http://en.wikipedia.org/wiki/Samoa&quot;&gt;Samoa&lt;/a&gt;
as it changed timezone in December 2011. Oh well.)&lt;/p&gt;

&lt;p&gt;As far as I have witnessed, the Gallium-related fields in computer
science research (programming languages design, compilation, type
systems...) share the bane of most other computer science domains (or
software development communities) in having a very low ratio of women
at all levels.&lt;/p&gt;

&lt;p&gt;Despite this imbalance, we actually have quite a few women that made
very important advances. In the process of writing this billet,
I gathered a rather long list of names of persons I would like to
write about. Those are mostly researchers from the second half of the
20th century, but not only: &lt;a href=&quot;http://en.wikipedia.org/wiki/Emmy_Noether&quot;&gt;Emmy
Noether&lt;/a&gt; was suggested to
me by Xavier, as the well-founded induction techniques she developed
are quite useful when writing formal devlopments in a proof assistant
such as Coq -- and of course her leading role in the movement of
formalization of mathematics through abstract algebra had wide-ranging
consequences that can definitely be felt, indirectly, in the
development of programming languages and systems.&lt;/p&gt;

&lt;p&gt;I have finally decided to avoid enumerations and rather attempt to
write something interesting on just a few women. This also has the
advantage of leaving ample material for the next years, even if they
will be new names to add to the list by then.&lt;/p&gt;

&lt;h2&gt;Barbara Liskov&lt;/h2&gt;

&lt;p&gt;The works of &lt;a href=&quot;http://en.wikipedia.org/wiki/Barbara_Jane_Liskov&quot;&gt;Barbara
Liskov&lt;/a&gt; that had the
most influence on me, personally, where &lt;a href=&quot;http://en.wikipedia.org/wiki/Abstract_Data_Type&quot;&gt;abstract data
types&lt;/a&gt; (not to be
confused with algebraic data types) and the &lt;a href=&quot;http://en.wikipedia.org/wiki/Liskov_substitution_principle&quot;&gt;Liskov Substituability
Principle
(LSP)&lt;/a&gt;.&lt;/p&gt;

&lt;h3&gt;Abstract data types&lt;/h3&gt;

&lt;p&gt;The idea of &lt;a href=&quot;http://en.wikipedia.org/wiki/Abstract_Data_Type&quot;&gt;abstract data
types&lt;/a&gt; and
encapsulation seems so natural now that it is difficult to imagine
that is wasn't around before the beginning of the seventies. A lot of
people were doing similar things at that time, but Barbara Liskov
proposed a lucid notion of abstract datatypes, whose internal
implementation choices were hidden from outside user modules. She then
integrated it in 1974 into a new language design,
&lt;a href=&quot;http://en.wikipedia.org/wiki/CLU_%28programming_language%29&quot;&gt;CLU&lt;/a&gt;,
which proved very influential for many other aspects as well: garbage
collected, iterators, multiple value return, exception handling... If
you want to learn more about the genesis of abstract types and CLU at
large, you should read &lt;a href=&quot;http://www.lcs.mit.edu/publications/pubs/pdf/MIT-LCS-TR-561.pdf&quot;&gt;A History of
CLU&lt;/a&gt;
(PDF,31 pages) that she wrote later in 1992.&lt;/p&gt;

&lt;p&gt;Abstract types were named &amp;quot;clusters&amp;quot;, and this is where the CLU name
comes from. Let's show the Wikipedia example:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;complex_number = cluster is add, subtract, multiply
    rep = record [ real_part: real, imag_part: real ]
    add = proc ... end add;
    subtract = proc ... end subtract;
    multiply = proc ... end multiply;
end complex_number;
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;This cluster exports the type &lt;code&gt;complex_number&lt;/code&gt; along with operations
&lt;code&gt;add&lt;/code&gt;, &lt;code&gt;substract&lt;/code&gt; and &lt;code&gt;multiply&lt;/code&gt;. This is all the user of the cluster
ever sees. Internally, the type is given a representation (a type
definition &lt;code&gt;rep = ...&lt;/code&gt;) and the operations are implemented in terms of
this representation, but this is encapsulated and doesn't leak to the
outside.&lt;/p&gt;

&lt;p&gt;Barbara Liskov has also played an influential in the development of
object-oriented techniques (and languages for distributed computing),
but CLU was not object-oriented. Some aspects of the design were
influenced by Simula (1967), but the &amp;quot;History of CLU&amp;quot; paper gives
a careful and detailed explanation of the difference between the
&amp;quot;abstract data-type&amp;quot; and the &amp;quot;object-oriented&amp;quot; (in the spirit of
Simula and Smalltalk) design style, that would be later expanded upon
and constrasted -- see William Cook's &lt;a href=&quot;http://www.cs.utexas.edu/~wcook/Drafts/2009/essay.pdf&quot;&gt;On Understanding Data
Abstraction,
Revisited&lt;/a&gt;.&lt;/p&gt;

&lt;p&gt;Abstract types are a very important concept in ML programming
languages. In 1988, Plotkin and Mitchell proposed to understand such
type abstraction as use of existential types (&lt;a href=&quot;http://homepages.inf.ed.ac.uk/gdp/publications/Abstract_existential.pdf&quot;&gt;Abstract Typess have
Existential
Type&lt;/a&gt;),
but the ML module systems actually takes a relatively different
approach to type abstraction. Several authors have tried to bridge the
gap, including in particular our very own Benoît Montagu in his 2009
article with Didier Rémy, &lt;a href=&quot;http://gallium.inria.fr/~remy/modules/Montagu-Remy@popl09:fzip.pdf&quot;&gt;Modeling Abstract Types in Modules with
Open Existential
Types&lt;/a&gt;.&lt;/p&gt;

&lt;h3&gt;Liskov Substituability Principle&lt;/h3&gt;

&lt;p&gt;The &lt;a href=&quot;http://en.wikipedia.org/wiki/Liskov_substitution_principle&quot;&gt;Liskov Substituability Principle
(LSP)&lt;/a&gt;
stems from a 1994 paper that Barbara Liskov wrote with &lt;a href=&quot;http://en.wikipedia.org/wiki/Jeannette_Wing&quot;&gt;Jeannette
Wing&lt;/a&gt;, &lt;a href=&quot;http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.39.1223&quot;&gt;A Behavioral
Notion of
Subtyping&lt;/a&gt;. It
gives the &lt;em&gt;right&lt;/em&gt; definition of what subtyping means: a type &lt;code&gt;S&lt;/code&gt; is
a subtype of a type &lt;code&gt;T&lt;/code&gt; if all values of type &lt;code&gt;S&lt;/code&gt; behave like values
of type &lt;code&gt;T&lt;/code&gt;. From this vision you can derive most properties of
subtyping relations (in particular contravariance in
input parameters), but those were already well-understood in 1994;
Liskov and Wing made the notion of &amp;quot;behave like&amp;quot; &lt;em&gt;formal&lt;/em&gt;, by saying
that anything that could be &lt;em&gt;proved&lt;/em&gt; about values of type &lt;code&gt;T&lt;/code&gt; should
also be provable on values of type &lt;code&gt;S&lt;/code&gt;.&lt;/p&gt;

&lt;p&gt;The rest of the paper is interesting (they provide limited extension
techniques that guarantee that this strong semantics requirement
is met), but what made history was how this principle got a name, LSP,
and was widely diffused to practitioners of object-oriented
programming. It is now part of all methodology advice on programming.
Mainstream object-oriented languages went for richer notions of
extension that those that are clearly correct, and systems mixing
programs and proofs to statically ensure substituability are still at
the research stage, but everyone shoud know by now that the derived
types the programmers implement &lt;em&gt;must respect&lt;/em&gt; the LSP -- it could be
argued that the preferred extension method of object-oriented
languages, implementation inheritance, makes it particularly easy to
mistakenly violate this invariant, so talking about it loud is
extremely useful.&lt;/p&gt;

&lt;h2&gt;Christine Paulin-Mohring&lt;/h2&gt;

&lt;p&gt;I had the luck to have Christine Paulin-Mohring as teacher at the
&lt;a href=&quot;https://wikimpri.dptinfo.ens-cachan.fr/doku.php&quot;&gt;MPRI&lt;/a&gt; (if you are
looking for a research-oriented graduate curriculum in computer
science, consider coming to Paris for the MPRI). Christine played an
instrumental role in the development of the
&lt;a href=&quot;http://en.wikipedia.org/wiki/Coq&quot;&gt;Coq&lt;/a&gt; proof assistant, including
leading the development of it. Among the most apparent works of
Christine are her work on program extraction out of Coq, and on
inductive definitions.&lt;/p&gt;

&lt;p&gt;If you already know a bit of Coq, you should consider reading the
&lt;a href=&quot;http://coq.inria.fr/distrib/V8.4/refman/Reference-Manual002.html&quot;&gt;Credits
page&lt;/a&gt;
of the Coq documentation, that is essentially a changelog of important
Coq version since Coq 5.10 in 1995 -- it really is surprisingly
interesting. Christine Paulin-Mohring is directly cited in most of
those release notes, and wrote several of them.&lt;/p&gt;

&lt;p&gt;Program extraction allows to take a Coq file and output a program in
a functional programming language such as OCaml, Haskell or
Scheme. A Coq file actually is a mix of programs (or proofs) written
in a style that resembles strongly typed functional programming
language, and tactics that are evil little metaprogramming scripts
that &lt;em&gt;build&lt;/em&gt; proofs (or programs) and are composed interactively by
the user, with direct feedback on the desired type of the terms to
build and the available environment. The job of extraction is to take
this happy mix and produce a Haskell, OCaml or Scheme program that can
be compiled down to something efficient -- in particular, it strives
to remove unnecessary computations that are not really needed during
the program execution but corresponds to static correctness proof
about the program.&lt;/p&gt;

&lt;p&gt;In Coq, this separation between what's needed at runtime and what can
be &amp;quot;erased&amp;quot; is done by a strict typing discipline: some types live in
&lt;code&gt;Prop&lt;/code&gt;, the universe of logical propositions (this mean that they
themselves have type &lt;code&gt;Prop&lt;/code&gt;), and their values can always be erased,
and the rest, outside &lt;code&gt;Prop&lt;/code&gt;, is kept. Note that this does not depend
on how the terms are written (programs or tactics); if you're
adventurous, you may use tactics as a meta-programming tool to write
honest programs executed at runtime, or write proofs by directly
writing the corresponding proof terms as ML-like programs.&lt;/p&gt;

&lt;p&gt;&lt;code&gt;Prop&lt;/code&gt; has specific typing rules (it is called impredicative)
designed to make it convenient to use for mathematical proofs, and the
type-checking rules have been designed to allow extraction. In
particular, if an inductive type in &lt;code&gt;Prop&lt;/code&gt; has non-erasable parameters
or at least two constructors, you may not pattern-match on it in an
expression returning a non-erasable value. The following examples
fails to type-check with a rather self-descriptive error:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;Inductive Choice : Prop := A | B.

Definition zero_or_one choice : nat :=
  match choice with
    | A =&amp;gt; 0
    | B =&amp;gt; 1
  end.
&lt;/code&gt;&lt;/pre&gt;

&lt;blockquote&gt;
  &lt;p&gt;Error: &lt;br/&gt;
Incorrect elimination of &amp;quot;choice&amp;quot; in the inductive type &amp;quot;Choice&amp;quot;: &lt;br/&gt;
the return type has sort &amp;quot;Set&amp;quot; while it should be &amp;quot;Prop&amp;quot;. &lt;br/&gt;
Elimination of an inductive object of sort Prop &lt;br/&gt;
is not allowed on a predicate in sort Set &lt;br/&gt;
because proofs can be eliminated only to build proofs.&lt;/p&gt;
&lt;/blockquote&gt;

&lt;p&gt;Indeed, the parameter &lt;code&gt;choice&lt;/code&gt;, having type &lt;code&gt;Choice&lt;/code&gt;, will be erased
during extraction. Yet the return type of &lt;code&gt;zero_or_one&lt;/code&gt; is a concrete
natural number; it is impossible to compute this result if the
parameter &lt;code&gt;choice&lt;/code&gt; is not passed at runtime. One can fix this by
moving the type of &lt;code&gt;Choice&lt;/code&gt; out of &lt;code&gt;Prop&lt;/code&gt;, or the result type inside
&lt;code&gt;Prop&lt;/code&gt;. When writing certified programs these choices are usually
rather simple to make, as it is clear from the whole program
architecture which parts are &amp;quot;program&amp;quot; and which are &amp;quot;proofs about the
program&amp;quot; : the program definition should not depend on its own
correctness proof. Of course in some cases you would wish for a more
flexible distinction, but that's for another discussion.&lt;/p&gt;

&lt;p&gt;While this whole description is a bit fuzzy on the details and make
some simplifications, I took care to point out that the limitation on
pattern-matching of propositions to return runtime values held for
inductive types of at least two constructors (or using
non-erasable parameters). Indeed, if a type has only one constructor,
of which all parameters are themselves erasable, matching on it does
not actually influence the runtime execution of the program (it is
called &amp;quot;non-informative&amp;quot;) and may be used for its type-checking
virtues. The reason I develop this example here is that it interleaves
both topics of inductive definitions and program extraction.&lt;/p&gt;

&lt;p&gt;The one-constructor case is extremely useful. That is how, for
example, equalities between terms can be used to type-check
a non-erasable program : they have only one constructor,
reflexivity. This is also the key to the use of accessibility
predicates (a constructive remnant of noetherian, or
well-founded, induction) to write programs with subtle termination
proof. Given a relation &lt;code&gt;R&lt;/code&gt; on the type &lt;code&gt;A&lt;/code&gt;, here is the type that
claims that &lt;code&gt;R&lt;/code&gt; is well-founded:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;Inductive Acc (x: A) : Prop :=
  Acc_intro : (forall y:A, R y x -&amp;gt; Acc y) -&amp;gt; Acc x.
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;When making a recursive call that has not obviously decreasing
parameters, one may instead use such an accessibility predicate to
show only that the recursive arguments are related (~ smaller than)
the arguments of the current call. This allows the user to choose the
termination argument (encoded in the relation &lt;code&gt;R&lt;/code&gt;) in a very general
way.&lt;/p&gt;

&lt;p&gt;The other major use of this rule is the case of the inductive type
having no constructor at all. This is the proposition &lt;code&gt;False&lt;/code&gt;, whose
proposition can be asked to the Coq system (here using the &lt;code&gt;coqtop&lt;/code&gt;
interactive toplevel):&lt;/p&gt;

&lt;blockquote&gt;
  &lt;p&gt;Coq &amp;lt; Print False. &lt;br/&gt;
Inductive False : Prop :=    &lt;/p&gt;
&lt;/blockquote&gt;

&lt;p&gt;That's it: &lt;code&gt;False&lt;/code&gt; have no constructor. This means that when you have
a term at type &lt;code&gt;False&lt;/code&gt; (a proof of contradiction), you may say &amp;quot;I'm
done, this case never happens&amp;quot; through an empty pattern matching:
&lt;code&gt;match contradiction with end.&lt;/code&gt; This pattern-matching works at any
target type &lt;code&gt;A&lt;/code&gt; (the outer function you're defining tries to produce
an &lt;code&gt;A&lt;/code&gt;) as all cases of the match are well-typed at type &lt;code&gt;A&lt;/code&gt;. And it
works inside proofs, but also to build terms despite &lt;code&gt;False&lt;/code&gt; being
a proposition, as it has less than two constructors.&lt;/p&gt;

&lt;p&gt;Christine Paulin-Mohring worked on all of this, and later taught it to
me and numerous other students.&lt;/p&gt;
</description><pubDate>16 Oct 2012 08:00:00 +0000</pubDate><category>research</category><category>computer science</category><category>women</category><category>abstract types</category><category>subtyping</category><category>inductive types</category><category>extraction</category><guid isPermaLink="true">http://gallium.inria.fr/~scherer/gagallium/ada-lovelace-day-2012/index.html</guid></item></channel></rss>
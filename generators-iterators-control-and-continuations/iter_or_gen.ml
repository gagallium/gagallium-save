(* from the caml-shift directory:
ocamlfind ocamlopt -rectypes -cclib -L. delimcc.cmxa -package unix,bench -linkpkg iter_or_gen.ml -o test.native
*)

(* comment to enable benchmarking *)
(* module Bench = struct *)
(*   let bench = ignore *)
(* end *)

type 'a iter = ('a -> unit) -> unit
type 'a gen = unit -> 'a

module type Data = sig
  type 'a t
  val of_iter : 'a iter -> 'a t
  val to_iter : 'a t -> 'a iter
  val of_gen : 'a gen -> 'a t
  val to_gen : 'a t -> 'a gen
end

module List : Data with type 'a t = 'a list
= struct
  type 'a t = 'a list

  let of_iter iter =
    let st = ref [] in
    iter (fun x -> st := x :: !st);
    List.rev !st

  let to_iter li = fun f ->
    let rec loop = function
      | [] -> ()
      | x :: xs -> f x; loop xs
    in loop li

  let () = assert (of_iter (to_iter [1;2;3]) = [1;2;3])

  let of_gen gen =
    let st = ref [] in
    try while true do st := gen () :: !st done; raise Exit
    with Exit -> List.rev !st

  let to_gen li =
    let st = ref li in
    fun () -> match !st with
      | [] -> raise Exit
      | x::xs -> st := xs; x

  let () = assert (of_gen (to_gen [1;2;3]) = [1;2;3])
end

let int_gen limit =
  let n = ref 0 in
  fun () ->
    incr n;
    if !n > limit then raise Exit;
    !n

let int_iter limit = fun f ->
  for i = 1 to limit do f i done

let long_limit = 50_000
let long_list = Array.to_list (Array.init long_limit (fun i -> i))
let long_gen = int_gen long_limit
let long_iter = int_iter long_limit

let very_long_limit = 200_000
let very_long_list = Array.to_list (Array.init very_long_limit (fun i -> i))
let very_long_gen = int_gen long_limit
let very_long_iter = int_iter long_limit

let consume_gen gen =
  try while true do ignore (gen ()) done
  with Exit -> ()

let consume_iter iter = iter ignore

let () = Bench.bench [
  "List: to_gen; consume",
    (fun () -> consume_gen (List.to_gen very_long_list));
  "List: to_iter; consume",
    (fun () -> consume_iter (List.to_iter very_long_list));
]

let () = Bench.bench [
  "List: of_gen", (fun () -> List.of_gen very_long_gen);
  "List: of_iter", (fun () -> List.of_iter very_long_iter);
]

let () = Bench.bench [
  "List: to_iter; of_iter",
    (fun () -> List.of_iter (List.to_iter very_long_list));
  "List: to_gen; of_gen",
    (fun () -> List.of_gen (List.to_gen very_long_list));
]

(* tree *)
type 'a tree = Leaf | Node of 'a tree * 'a * 'a tree

let rec deep = function
  | 0 -> Leaf
  | n -> let t = deep (n - 1) in Node (t, n, t)

let long_tree = deep 18

module Tree : Data with type 'a t = 'a tree
= struct
  type 'a t = 'a tree

  let rec iter f = function
    | Leaf -> ()
    | Node (left, x, right) -> iter f left; f x; iter f right

  let to_iter t f = iter f t

  (* this must be compiled with -rectypes *)
  let to_gen_delim t =
    let p = Delimcc.new_prompt () in
    let next = ref begin
      Delimcc.push_prompt p (fun () ->
        iter (fun x ->
          Delimcc.take_subcont p (fun k () -> Some (x, k))) t;
        None)
    end in
    fun () ->
      match !next with
        | None -> raise Exit
        | Some (x, k) ->
          next := Delimcc.push_delim_subcont k (fun () -> ());
          x

  let rec iter_cps f t k = match t with
    | Leaf -> k ()
    | Node (left, x, right) ->
      iter_cps f left (fun () ->
        f x; iter_cps f right k)

  type 'a kiter =
    | Stop
    | Left of 'a kiter * 'a * 'a tree

  let rec iter_cps_defun f t k = match t with
    | Leaf -> run f k
    | Node (left, x, right) ->
      iter_cps_defun f left (Left (k, x, right))
  and run f = function
    | Stop  -> ()
    | Left (k, x, right) -> f x; iter_cps_defun f right k

  let rec iter_cps_defun f t k = match t with
    | Leaf -> run f k
    | Node (left, x, right) -> iter_cps_defun f left (Node (k, x, right))
  and run f = function
    | Leaf -> ()
    | Node (k, x, right) -> f x; iter_cps_defun f right k

  let bench_iter = Bench.bench [
    ("Tree: iter", fun () -> iter ignore long_tree);
    ("Tree: iter_cps", fun () -> iter_cps ignore long_tree (fun x -> x));
    ("Tree: iter_cps_defun", fun () -> iter_cps_defun ignore long_tree Leaf);
  ]

  let to_gen t =
    let next = ref t in
    let cont = ref Leaf in
    let rec iter t k = match t with
      | Leaf -> run k
      | Node (left, x, right) -> iter left (Node (k, x, right))
    and run = function
      | Leaf -> raise Exit
      | Node (k, x, right) ->
        next := right;
        cont := k;
        x
    in fun () -> iter !next !cont

  let bench_iter = Bench.bench [
    ("Tree: to_gen", fun () -> consume_gen (to_gen long_tree));
    ("Tree: to_iter", fun () -> consume_iter (to_iter long_tree));
  ]

  (** returns a tree of height [n], along with a boolean [finished]
      indicating whether the generator was exhausted during the
      construction. If [finished] is false, the tree is a complete tree
      of height [n], otherwise it may be partial. *)
  let rec fill f n =
    match n with
    | 0 -> false, Leaf
    | _ ->
      let finished, left = fill f (n - 1) in
      if finished then finished, left
      else fill_right f n left
  
  (** taking a complete left subtree as input, it builds a node by
      creating a possibly-partial right subtree *)
  and fill_right f n left =
    match (try Some (f ()) with Exit -> None) with
      | None -> true, left
      | Some x ->
        let finished, right = fill f (n - 1) in
        finished, Node (left, x, right)
  
  (** iterate the filling functions above with ever-increasing values of
      `n`, until the generator is exhausted. *)
  let rec loop f n left =
    match fill_right f n left with
      | true, t -> t
      | false, left -> loop f (n + 1) left

  let of_gen gen = loop gen 1 Leaf

  let () =
    let li = [1;2;3;4;5;6] in
    let t = of_gen (List.to_gen li) in
    assert (List.of_iter (to_iter t) = li);
    assert (List.of_gen (to_gen t) = li);
    ()

  let () =
    let li = [1;2;3;4;5;6] in
    assert (List.of_gen (to_gen_delim (of_gen (List.to_gen li))) = li)

  let test_of_gen, test_of_iter =
    let sing n = Node (Leaf, n, Leaf) in
    let t = Node (Node (sing 1, 2, sing 3), 4, Node (sing 5, 6, Leaf)) in
    let test_of_gen of_gen =
      (t = of_gen (List.to_gen [1;2;3;4;5;6])) in
    let test_of_iter of_iter =
      (t = of_iter (List.to_iter [1;2;3;4;5;6])) in
    test_of_gen, test_of_iter

  let () = assert (test_of_gen of_gen)

  let bench_of_gen = ("Tree: of_gen", fun () ->
    consume_iter long_iter; of_gen long_gen)

  let rec fill_cps f n k = match n with
    | 0 -> k false Leaf
    | _ ->
      fill_cps f (n - 1) (fun finished left ->
        if finished then k finished left
        else fill_right_cps f n left k)
  and fill_right_cps f n left k =
    match (try Some (f ()) with Exit -> None) with
      | None -> k true left
      | Some x ->
        fill_cps f (n - 1) (fun finished right ->
          k finished (Node (left, x, right)))

  let rec loop_cps f n left k =
    fill_right_cps f n left (fun finished t ->
      if finished then k t
      else loop_cps f (n + 1) t k)

  let of_gen gen = loop_cps gen 1 Leaf (fun x -> x)

  let () = assert (test_of_gen of_gen)

  type 'a kfill =
    | Stop
    | Left of 'a kfill * int
    | Right of 'a tree * 'a * 'a kfill
    | Loop of int * 'a kfill

  let rec fill_cps_defun f n k = match n with
    | 0 -> run f false Leaf k
    | _ -> fill_cps_defun f (n - 1) (Left (k, n))
  and fill_right_cps_defun f n left k =
    match (try Some (f ()) with Exit -> None) with
      | None -> run f true left k
      | Some x ->
        fill_cps_defun f (n - 1) (Right (left, x, k))
  and loop_cps_defun f n left k = fill_right_cps_defun f n left (Loop (n, k))
  and run f finished t = function
    | Stop -> t
    | Left (k, n) ->
      if finished then run f finished t k
      else fill_right_cps_defun f n t k
    | Right (left, x, k) ->
      run f finished (Node (left, x, t)) k
    | Loop (n, k) ->
      if finished then run f finished t k
      else loop_cps_defun f (n + 1) t k

  let of_gen gen = loop_cps_defun gen 1 Leaf Stop

  let () = assert (test_of_gen of_gen)

  type 'a call1 =
    | Call_loop of int * 'a tree * 'a kfill
    | Call_fill of int * 'a kfill

  let of_iter iter =
    let st = ref (Call_loop (1, Leaf, Stop)) in
    let result = ref None in
    let rec fill f n k = match n with
      | 0 -> run f false Leaf k
      | _ -> fill f (n - 1) (Left (k, n))
    and fill_right f n left k =
      match f with
        | None -> run f true left k
        | Some x ->
            st := Call_fill (n - 1, Right (left, x, k))
    and loop f n left k = fill_right f n left (Loop (n, k))
    and run f finished t = function
      | Stop -> result := Some t
      | Left (k, n) ->
        if finished then run f finished t k
        else fill_right f n t k
      | Right (left, x, k) ->
        run f finished (Node (left, x, t)) k
      | Loop (n, k) ->
        if finished then run f finished t k
        else loop f (n + 1) t k
    in
    let call x = function
      | Call_loop (n, t, k) -> loop x n t k
      | Call_fill (n, k) -> fill x n k
    in
    let add x = call (Some x) !st in
    iter add;
    call None !st;
    match !result with
      | None -> assert false
      | Some res -> res

  let () = assert (test_of_iter of_iter)

  let bench_of_iter1 = ("Tree: of_iter 1", fun () ->
    consume_gen long_gen; of_iter long_iter)

  let rec fill_cps2 f n ~kfin ~knot = match n with
    | 0 -> knot Leaf
    | _ ->
      fill_cps2 f (n - 1) ~kfin
        ~knot:(fun left -> fill_right_cps2 f n left ~kfin ~knot)
  and fill_right_cps2 f n left ~kfin ~knot =
    match (try Some (f ()) with Exit -> None) with
      | None -> kfin left
      | Some x ->
        fill_cps2 f (n - 1)
          ~kfin:(fun right -> kfin (Node (left, x, right)))
          ~knot:(fun right -> knot (Node (left, x, right)))
  let rec loop_cps2 f n left ~kfin ~knot =
    fill_right_cps2 f n left ~kfin
      ~knot:(fun t -> loop_cps2 f (n + 1) t ~kfin ~knot)

  let of_gen gen = loop_cps2 gen 1 Leaf ~kfin:(fun x -> x) ~knot:(fun x -> x)

  let () = assert (test_of_gen of_gen)

  let of_gen (type a) (f : unit -> a) =
    let module E = struct exception Finished of a tree end in
    let open E in
    let rec fill n =
      match n with
        | 0 -> Leaf
        | _ ->
          let left = fill (n - 1) in
          fill_right n left
    and fill_right n left =
      match (try Some (f ()) with Exit -> None) with
        | None -> raise (Finished left)
        | Some x ->
          try
            let right = fill (n - 1) in
            Node (left, x, right)
          with Finished right ->
            raise (Finished (Node (left, x, right)))
    in
    let rec loop n left =
      let left = fill_right n left in
      loop (n + 1) left
    in
    try loop 1 Leaf with Finished t -> t

  let () = assert (test_of_gen of_gen)

  let bench_of_gen_exn = ("Tree: of_gen (exn)", fun () -> of_gen long_gen)
  
  type 'a kfin =
    | FinStop
    | FinNode of 'a tree * 'a * 'a kfin

  type 'a knot =
    | NotStop
    | NotLeft of int * 'a kfin * 'a knot
    | NotNode of 'a tree * 'a * 'a knot
    | NotLoop of int * 'a kfin * 'a knot

  let rec fill_cps2_defun f n ~kfin ~knot = match n with
    | 0 -> run_not f knot Leaf
    | _ -> fill_cps2_defun f (n - 1) ~kfin ~knot:(NotLeft (n, kfin, knot))
  and fill_right_cps2_defun f n left ~kfin ~knot =
    match (try Some (f ()) with Exit -> None) with
      | None -> run_fin kfin left
      | Some x ->
        fill_cps2_defun f (n - 1)
          ~kfin:(FinNode (left, x, kfin))
          ~knot:(NotNode (left, x, knot))
  and loop_cps2_defun f n left ~kfin ~knot =
    fill_right_cps2_defun f n left ~kfin ~knot:(NotLoop (n, kfin, knot))
  and run_not f = function
    | NotStop -> (fun x -> x)
    | NotLeft (n, kfin, knot) ->
        (fun left -> fill_right_cps2_defun f n left ~kfin ~knot)
    | NotNode (left, x, knot) ->
        (fun right -> run_not f knot (Node (left, x, right)))
    | NotLoop (n, kfin, knot) -> 
        (fun t -> loop_cps2_defun f (n + 1) t ~kfin ~knot)
  and run_fin = function
    | FinStop -> (fun x -> x)
    | FinNode (left, x, kfin) -> 
        (fun right -> run_fin kfin (Node (left, x, right)))

  let of_gen f =
    loop_cps2_defun f 1 Leaf ~kfin:FinStop ~knot:NotStop

  let () = assert (test_of_gen of_gen)

  type 'a call2 =
    | Call_loop of int * 'a tree * 'a kfin * 'a knot
    | Call_fill of int * 'a kfin * 'a knot
  
  let of_iter iter =
    let st = ref (Call_loop (1, Leaf, FinStop, NotStop)) in
    let result = ref None in
    let rec fill x n ~kfin ~knot = match n with
      | 0 -> run_not x knot Leaf
      | _ -> fill x (n - 1) ~kfin ~knot:(NotLeft (n, kfin, knot))
    and fill_right x n left ~kfin ~knot =
      match x with
        | None -> result := Some (run_fin kfin left)
        | Some x ->
          st := Call_fill (n - 1,
                           FinNode (left, x, kfin),
                           NotNode (left, x, knot))
    and loop x n left ~kfin ~knot =
      fill_right x n left ~kfin ~knot:(NotLoop (n, kfin, knot))
    and run_not x = function
      | NotStop -> assert false
      | NotLeft (n, kfin, knot) ->
        (fun left -> fill_right x n left ~kfin ~knot)
      | NotNode (left, v, knot) ->
        (fun right -> run_not x knot (Node (left, v, right)))
      | NotLoop (n, kfin, knot) ->
        (fun t -> loop x (n + 1) t ~kfin ~knot)
    and run_fin = function
      | FinStop -> (fun x -> x)
      | FinNode (left, x, kfin) ->
        (fun right -> run_fin kfin (Node (left, x, right)))
    in
    let call x = function
      | Call_loop (n, t, kfin, knot) -> loop x n t ~kfin ~knot
      | Call_fill (n, kfin, knot) -> fill x n ~kfin ~knot
    in
    let add x = call (Some x) !st in
    iter add;
    call None !st;
    match !result with
      | None -> assert false
      | Some res -> res

  let () = assert (test_of_iter of_iter)

  let bench_of_iter2 = ("Tree: of_iter 2", fun () ->
    consume_gen long_gen; of_iter long_iter)

  type (_, _) input =
    | Next : 'a -> ('a, unit) input
    | End_of_input : ('a, 'a tree) input
  
  let of_iter (type a) (iter : a iter) =
    let st = ref (Call_loop (1, Leaf, FinStop, NotStop)) in
    let rec run_fin = function
      | FinStop -> (fun x -> x)
      | FinNode (left, x, kfin) ->
        (fun right -> run_fin kfin (Node (left, x, right)))
    in
    let rec fill x n ~kfin ~knot = match n with
      | 0 -> run_not x knot Leaf
      | _ -> fill x (n - 1) ~kfin ~knot:(NotLeft (n, kfin, knot))
    and fill_right : type b . (a, b) input -> int -> a tree
                              -> kfin:a kfin -> knot:a knot -> b =
        fun x n left ~kfin ~knot -> 
      match x with
        | End_of_input -> run_fin kfin left
        | Next x ->
          st := Call_fill (n - 1,
                           FinNode (left, x, kfin),
                           NotNode (left, x, knot))
    and loop x n left ~kfin ~knot =
      fill_right x n left ~kfin ~knot:(NotLoop (n, kfin, knot))
    and run_not x = function
      | NotStop -> assert false
      | NotLeft (n, kfin, knot) ->
        (fun left -> fill_right x n left ~kfin ~knot)
      | NotNode (left, v, knot) ->
        (fun right -> run_not x knot (Node (left, v, right)))
      | NotLoop (n, kfin, knot) ->
        (fun t -> loop x (n + 1) t ~kfin ~knot)
    in
    let call x = function
      | Call_loop (n, t, kfin, knot) -> loop x n t ~kfin ~knot
      | Call_fill (n, kfin, knot) -> fill x n ~kfin ~knot
    in
    let add x = call (Next x) !st in
    iter add;
    call End_of_input !st    

  let () = assert (test_of_iter of_iter)

  let () = Bench.bench [
    bench_of_gen;
    bench_of_gen_exn;
    bench_of_iter1;
    bench_of_iter2;
  ]

  (* this must be compiled with -rectypes *)
  let of_iter_delim iter =
    let p = Delimcc.new_prompt () in
    let next = ref begin
      Delimcc.push_prompt p (fun () ->
        iter (fun x ->
          Delimcc.take_subcont p (fun k () -> Some (x, k)));
        None)
    end in
    of_gen (fun () ->
      match !next with
        | None -> raise Exit
        | Some (x, k) ->
          next := Delimcc.push_delim_subcont k (fun () -> ());
          x)

  let () = assert (test_of_iter of_iter_delim)
end

module type SizedData = sig
  type 'a t
  val of_iter : int * 'a iter -> 'a t
  val to_iter : 'a t -> int * 'a iter
  val of_gen : int * 'a gen -> 'a t
  val to_gen : 'a t -> int * 'a gen
end

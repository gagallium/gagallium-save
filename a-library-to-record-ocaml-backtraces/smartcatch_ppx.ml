(*
  ocamlc -I +compiler-libs ocamlcommon.cma smartcatch_ppx.ml -o smartcatch_ppx.byte
  
  echo "let () = try raise Exit with Exit -> print_newline ()" > test.ml
  ocamlc -ppx ./smartcatch_ppx.byte -dsource test.ml
*)

open Parsetree
open Asttypes

let add_register patvar body =
  let open Ast_mapper in
  let register_fun = Location.mknoloc (Longident.parse "ExceptionHandling.register") in
  (* let _ = <register_fun> <patvar> in <body> *)
  E.(let_ Nonrecursive
       [P.any (), apply (ident register_fun)  ["", ident patvar]]
       body)

let instrumenter = object (self)
  inherit Ast_mapper.mapper as super

  method expr e =
    let e = super#expr e in
    { e with pexp_desc =
        match e.pexp_desc with
          | Pexp_try (body, handler) ->
            let instrument_case (pat, body) = 
              let patvar_str =  "__exn" in
              let patvar = Location.mknoloc (Longident.parse patvar_str) in
              let pat = { pat with ppat_desc =
                Ppat_alias (pat, Location.mknoloc patvar_str) } in
              (pat, add_register patvar body) in
            Pexp_try (body, List.map instrument_case handler)
          | other -> other
    }
end

let () =
  Ast_mapper.main (object
    method implementation input_name structure =
      (input_name, instrumenter#structure structure)
    method interface input_name signature =
      (input_name, signature)
  end)

<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0"><channel><title>Gagallium: algorithms</title><link>http://gallium.inria.fr/~scherer/gagallium/kw_algorithms.html</link><description>A draft blog at Gallium</description><managingEditor>gabriel.scherer@inria.fr</managingEditor><pubDate>03 Dec 2012 08:00:00 +0000</pubDate><lastBuildDate>03 Dec 2012 08:00:00 +0000</lastBuildDate><generator>Stog</generator><item><title>Performance improvements in the universe checker of Coq</title><link>http://gallium.inria.fr/~scherer/gagallium/union-find-and-coq-universes/index.html</link><description>&lt;p&gt;
One of my &lt;a href=&quot;http://gallium.inria.fr/~scherer/gagallium/verifying-a-parser-for-a-c-compiler/&quot;&gt;parsing-related&lt;/a&gt;
Coq files takes more than two hours to compile, which is pretty annoying
when the file is less than one thousand lines long and does no complex
computations. After some profiling, it appeared that Coq was spending
most of its time in the &lt;code&gt;Univ&lt;/code&gt; module. This module is part of the
Coq kernel, its role is to check that the user is using universes in a
consistent way.&lt;/p&gt;



&lt;p&gt;You can get an introduction of what universes are in
&lt;a href=&quot;http://adam.chlipala.net/cpdt/html/Universes.html&quot;&gt;this book
  chapter&lt;/a&gt;. In Coq, universes are implicit: it is the role of the
Coq kernel to determine whether it is possible to give a number to
each occurrence of &lt;code&gt;Type&lt;/code&gt;, in a manner that is proved not to
introduce inconsistencies.&lt;/p&gt;

&lt;p&gt;This computation is done in the &lt;code&gt;Univ&lt;/code&gt; module: the rest of the Coq
kernel generates some constraints, and this module is supposed to warn
if they are not satisfiable. Roughly, theses constraints are of the
form: &amp;quot;universe A has an index strictly smaller than universe B&amp;quot;, or
&amp;quot;universe A has an index smaller or equal to universe B&amp;quot;. In order to
check that they are satisfiable, it is enough to see all these
constraints as a graph where edges are constraints and vertexes are
universes, and check that there is no cycle containing a strict
constraint.&lt;/p&gt;

&lt;p&gt;This graph can be simplified: if there is a cycle of large
constraints, that does mean that all universes in this cycle are in
fact necessarily equivalent, and we can replace this cycle with only
one universe. We have to keep track of equivalences, using a
specialized data structure. We recognize here the classical
&lt;a href=&quot;http://en.wikipedia.org/wiki/Disjoint-set_data_structure&quot;&gt;union-find 
problem&lt;/a&gt;, which have very efficient solutions.&lt;/p&gt;

&lt;p&gt;However, in order to do this task, in Coq, a naive approach is used:
it builds equivalence trees, but nothing is done to prevent these
trees to be very unbalanced. And, in fact, that was the problem in my
file: for some reason, my tactics generated hundred of universes constraints.
All these universes were inferred to be equivalent. In order to keep track
of this information, a long chain of universes were created: for each
of them, Coq knows that it is equivalent to the next one, but it does
not know how to go directly to the head of the chain, that is effectively
present in the graph. So, it was actually spending most of its time
following this long chain...&lt;/p&gt;

&lt;p&gt;In the literature, there are two main heuristics to solve this
(classical) problem: &amp;quot;union by rank&amp;quot; and &amp;quot;path compression&amp;quot;. Using
either one gives logarithmic amortized complexity, and they give
a practically constant amortized complexity if implemented
together. By experience, I know that each of these heuristics gives
very good performances when used alone, because the worst case is very
rare. Moreover, it is not easy to implement path compression in a
&lt;a href=&quot;http://en.wikipedia.org/wiki/Persistent_data_structure&quot;&gt;persistent
  way&lt;/a&gt;, because it involves side effects. So, I decided to
  implement &amp;quot;union by rank&amp;quot; inside the Univ module of Coq.&lt;/p&gt;

&lt;p&gt;It was not really difficult: I just added a &lt;code&gt;rank&lt;/code&gt; field
in the record representing the nodes of the graph, that contains the
length of the longest path of equivalences leading to this node. So,
when we are merging nodes, we can chose to keep the node having the
longest chain as the head node. This keeps the graph balanced: if the
other nodes were part of smaller chains, the longest chain's length
doesn't increase.&lt;/p&gt;

&lt;p&gt;I got a spectacular improvement of performances when Coq is executed
on my code: from more than two hours, I went to about 6.5
minutes. I get a reasonable 1% improvement when compiling the Coq
standard library: it is actually expected, because the standard
library does not contain a lot of complex polymorphic code.&lt;/p&gt;

&lt;p&gt;You can see the current patch (which may still evolve a bit)
in &lt;a href=&quot;https://github.com/coq/coq/pull/2&quot;&gt;this pull
request&lt;/a&gt;. My first implementation was against 8.3, this patch is on
the trunk version of Coq, and we're planning to port it to 8.4
soon. Do not hesitate to test it if you have universe-heavy
developments; any feedback is welcome.&lt;/p&gt;</description><pubDate>03 Dec 2012 08:00:00 +0000</pubDate><category>coq</category><category>performance</category><category>algorithms</category><category>union-find</category><guid isPermaLink="true">http://gallium.inria.fr/~scherer/gagallium/union-find-and-coq-universes/index.html</guid></item></channel></rss>
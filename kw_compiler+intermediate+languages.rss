<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0"><channel><title>Gagallium: compiler intermediate languages</title><link>http://gallium.inria.fr/~scherer/gagallium/kw_compiler+intermediate+languages.html</link><description>A draft blog at Gallium</description><managingEditor>gabriel.scherer@inria.fr</managingEditor><pubDate>18 Jun 2012 08:00:00 +0000</pubDate><lastBuildDate>18 Jun 2012 08:00:00 +0000</lastBuildDate><generator>Stog</generator><item><title>Reading papers on CPS, SSA, ANF and the likes</title><link>http://gallium.inria.fr/~scherer/gagallium/ssa-cps-reading-notes/index.html</link><description>&lt;p&gt;I have recently started an internship in the Gallium team working on intermediate representation (IR) for compilers. The aim is to get a better understanding of the relation that links &amp;quot;traditional&amp;quot; forms and their functional counterparts.&lt;/p&gt;


&lt;p&gt;Many different representations exist, the ones I am focusing on are:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;static single assignment (SSA): each variable is assigned to only once (appears once in the left-hand side of an assignment), this makes data-flow tracking easy,&lt;/li&gt;
&lt;li&gt;continuation passing style (CPS): a variant of &lt;span class=&quot;math&quot;&gt;λ&lt;/span&gt;-calculus where functions do not return but apply a continuation to their result, and&lt;/li&gt;
&lt;li&gt;administrative normal form (ANF): a variant of &lt;span class=&quot;math&quot;&gt;λ&lt;/span&gt;-calculus with flat (not-nested) &lt;code&gt;let&lt;/code&gt; bindings.&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;There are other IRs (such as the &lt;em&gt;monadic intermediate language&lt;/em&gt;) which I will not explore.&lt;/p&gt;
&lt;p&gt;I started my internship by reading papers on the topic, and coding a tool to help in the exploration of the different IRs. The code is on &lt;a href=&quot;https://github.com/raphael-proust/cps-ssa&quot;&gt;my github&lt;/a&gt; (which is not bad for reasons of OSS code hosted on non OSS-powered servers but because it uses flash).&lt;/p&gt;
&lt;h3 id=&quot;an-example-with-ssa-and-cps&quot;&gt;An example with SSA and CPS&lt;/h3&gt;
&lt;p&gt;Here is the dummy pseudo-code example we will be using:&lt;/p&gt;
&lt;pre&gt;&lt;code&gt;i = 0
j = 10
while j&amp;gt;0 do
  i = i+1
  if i%2 = 0
    j = j-i
  else
    j = j-1
done
return j
&lt;/code&gt;&lt;/pre&gt;
&lt;p&gt;Translation into SSA requires the conversion to a control flow graph (CFG; here flattened using &lt;code&gt;label&lt;/code&gt;s and &lt;code&gt;jump&lt;/code&gt;s/&lt;code&gt;branch&lt;/code&gt;s for edges) and placement of &lt;span class=&quot;math&quot;&gt;φ&lt;/span&gt;-functions. These &lt;span class=&quot;math&quot;&gt;φ&lt;/span&gt;-functions hack around the main SSA restriction: variables can not be assigned to twice. Thus &lt;span class=&quot;math&quot;&gt;φ&lt;/span&gt;-functions merge variables coming from different places.&lt;/p&gt;
&lt;pre&gt;&lt;code&gt;i0 = 0
j0 = 10

label l_while_test
j1 = phi j0 j2 j3
i1 = phi i0 i2
t0 = gt j1 0
branch t0 l_while_body l_while_exit

label l_while_body
i2 = add i1 1
t1 = mod i2 2
t2 = eq t1 0
branch t2 l_if_true l_if_false

label l_if_true
j2 = sub j1 i2
jump l_while_test

label l_if_false
j3 = sub j1 1
jump l_while_test

label l_while_exit
return j1
&lt;/code&gt;&lt;/pre&gt;
&lt;p&gt;The notations used in this examples use the following conventions:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;assignment to a variable &lt;code&gt;v&lt;/code&gt; is noted &lt;code&gt;v = &amp;lt;operator&amp;gt; &amp;lt;operand1&amp;gt; &amp;lt;operand2&amp;gt;&lt;/code&gt;,&lt;/li&gt;
&lt;li&gt;branching is done via &lt;code&gt;branch &amp;lt;variable&amp;gt; &amp;lt;label1&amp;gt; &amp;lt;label2&amp;gt;&lt;/code&gt; and is equivalent to &lt;code&gt;jump &amp;lt;label1&amp;gt;&lt;/code&gt; if the variable is &lt;code&gt;true&lt;/code&gt;, and &lt;code&gt;jump &amp;lt;label2&amp;gt;&lt;/code&gt; otherwise, and&lt;/li&gt;
&lt;li&gt;variables that were introduced as a result of the single-assignment constraint preserve their original variable names as prefixes, others are prefixed with &lt;code&gt;t&lt;/code&gt;.&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;The CPS version resemble SSA's. The main difference is with the &lt;span class=&quot;math&quot;&gt;φ&lt;/span&gt; &lt;span style=&quot;text-decoration: line-through;&quot;&gt;hack&lt;/span&gt; ingenious idea. The following table sums up the correspondence:&lt;/p&gt;
&lt;table&gt;
&lt;col width=&quot;25%&quot;/&gt;
&lt;col width=&quot;26%&quot;/&gt;
&lt;thead&gt;
&lt;tr class=&quot;header&quot;&gt;
&lt;th align=&quot;left&quot;&gt;SSA&lt;/th&gt;
&lt;th align=&quot;left&quot;&gt;CPS&lt;/th&gt;
&lt;/tr&gt;
&lt;/thead&gt;
&lt;tbody&gt;
&lt;tr class=&quot;odd&quot;&gt;
&lt;td align=&quot;left&quot;&gt;Procedure&lt;/td&gt;
&lt;td align=&quot;left&quot;&gt;&lt;span class=&quot;math&quot;&gt;λ&lt;/span&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr class=&quot;even&quot;&gt;
&lt;td align=&quot;left&quot;&gt;Block&lt;/td&gt;
&lt;td align=&quot;left&quot;&gt;&lt;span class=&quot;math&quot;&gt;λ&lt;/span&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr class=&quot;odd&quot;&gt;
&lt;td align=&quot;left&quot;&gt;&lt;span class=&quot;math&quot;&gt;φ&lt;/span&gt;-node&lt;/td&gt;
&lt;td align=&quot;left&quot;&gt;Parameters for a block-&lt;span class=&quot;math&quot;&gt;λ&lt;/span&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr class=&quot;even&quot;&gt;
&lt;td align=&quot;left&quot;&gt;Jump/Branch/…&lt;/td&gt;
&lt;td align=&quot;left&quot;&gt;Application&lt;/td&gt;
&lt;/tr&gt;
&lt;/tbody&gt;
&lt;/table&gt;
&lt;p&gt;Here is the translated examples. The &lt;span class=&quot;math&quot;&gt;λ&lt;/span&gt;s are noted with a &lt;code&gt;\&lt;/code&gt;. The names of the labels have been reused to make the correspondence easier to see. (Note that we do not use a complete CPS and allow the use of operators in direct style. On the other hand we kept CPS for &lt;code&gt;if-then-else&lt;/code&gt;.)&lt;/p&gt;
&lt;pre&gt;&lt;code&gt;\ i j k . (
  while_test i j k
  where while_test i j k = (
    if (j&amp;gt;0) (k i j) (while_body i j while_test)
    where while_body i j k = (
      \ i . (
        if (i%2 = 0) (if_true i j k) (if_false i j k)
        where if_true i j k = (k i (j - i))
        and   if_false i j k = (k i (j - 1))
      ) (i+1)
    )
    in
  )
  in
  )
) 0 10 (\ i j k . (k j))
&lt;/code&gt;&lt;/pre&gt;
&lt;p&gt;This example might be more difficult to read for those not familiar with &lt;span class=&quot;math&quot;&gt;λ&lt;/span&gt;-calculus or CPS code. Points of interest are:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;the &lt;span class=&quot;math&quot;&gt;φ&lt;/span&gt;s at the &lt;code&gt;while_test&lt;/code&gt;-labeled block are translated as parameters to the &lt;code&gt;while_test&lt;/code&gt;-named function,&lt;/li&gt;
&lt;li&gt;both &lt;code&gt;jump&lt;/code&gt;s, &lt;code&gt;return&lt;/code&gt;s, and &lt;code&gt;branch&lt;/code&gt;s are translated as continuation application&lt;/li&gt;
&lt;li&gt;each block is translated as a &lt;span class=&quot;math&quot;&gt;λ&lt;/span&gt;, and&lt;/li&gt;
&lt;li&gt;the nesting of &lt;span class=&quot;math&quot;&gt;λ&lt;/span&gt;s matches the &lt;em&gt;dominator&lt;/em&gt; tree of the control-flow graph of the blocks.&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;The notion of dominator will not be formally defined here. Let's just say that &amp;quot;a block &lt;span class=&quot;math&quot;&gt;&lt;em&gt;b&lt;/em&gt;&lt;sub&gt;1&lt;/sub&gt;&lt;/span&gt; dominates a block &lt;span class=&quot;math&quot;&gt;&lt;em&gt;b&lt;/em&gt;&lt;sub&gt;2&lt;/sub&gt;&lt;/span&gt; if every path leading to &lt;span class=&quot;math&quot;&gt;&lt;em&gt;b&lt;/em&gt;&lt;sub&gt;2&lt;/sub&gt;&lt;/span&gt; goes through &lt;span class=&quot;math&quot;&gt;&lt;em&gt;b&lt;/em&gt;&lt;sub&gt;1&lt;/sub&gt;&lt;/span&gt;&amp;quot;. Details can be found in the literature.&lt;/p&gt;
&lt;h3 id=&quot;papers&quot;&gt;Papers&lt;/h3&gt;
&lt;p&gt;Here are comments on a few interesting papers related to the aforementioned topic. It is intended for those in need of more details, others might skip to the Code section.&lt;/p&gt;
&lt;h4 id=&quot;ssa-is-functional-programming&quot;&gt;SSA is Functional Programming&lt;/h4&gt;
&lt;p&gt;by Appel, 1998, &lt;a href=&quot;http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.34.3282&quot;&gt;available here&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;The article informally explains the links between SSA and functional programming in general. SSA has syntactic constraints that makes it nested-scope-like and thus easy to translate into a functional language. Jumps are mapped to function applications and &lt;span class=&quot;math&quot;&gt;φ&lt;/span&gt;s to function parameters.&lt;/p&gt;
&lt;h4 id=&quot;compiling-standard-ml-to-java-bytecodes&quot;&gt;Compiling Standard ML to Java Bytecodes&lt;/h4&gt;
&lt;p&gt;by Benton, Kennedy and Russell, 1998, &lt;a href=&quot;http://research.microsoft.com/apps/pubs/default.aspx?id=64029&quot;&gt;available here&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;The article explains the internals of MLj, a compiler from SML97 to Java bytecode. It uses a typed, functional intermediate representation called monadic intermediate language (MIL). Features include: SML-java interoperability (possible foreign function calls) and various optimisations (such as monomorphisation).&lt;/p&gt;
&lt;p&gt;MIL is a typed functional language. The type system differentiate values and computations—the latter including effect information. Optimisations based on &lt;span class=&quot;math&quot;&gt;β&lt;/span&gt; and &lt;span class=&quot;math&quot;&gt;η&lt;/span&gt; reduction need a transformation called &lt;em&gt;commuting conversions&lt;/em&gt; to avoid &lt;code&gt;let&lt;/code&gt;-&lt;code&gt;in&lt;/code&gt; nesting.&lt;/p&gt;
&lt;p&gt;Towards the end, the compiler transforms (commuting-conversion normalised) MIL into a variant of SSA. This final representation is then transformed into real bytecode.&lt;/p&gt;
&lt;p&gt;Note: sources for the compiler are available on the &lt;a href=&quot;http://www.dcs.ed.ac.uk/home/mlj/index.html&quot;&gt;MLj page&lt;/a&gt;.&lt;/p&gt;
&lt;h4 id=&quot;compiling-with-continuations-continued&quot;&gt;Compiling with Continuations, Continued&lt;/h4&gt;
&lt;p&gt;by Kennedy, 2007, &lt;a href=&quot;http://research.microsoft.com/en-us/um/people/akenn/sml/&quot;&gt;available here&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;The paper advocates the use of CPS and argues that it is better than either ANF or MIL for use as an intermediate language in compilers. Optimisations apply better on CPS than on alternative in that they are well known &lt;span class=&quot;math&quot;&gt;λ&lt;/span&gt;-calculus transformation and they preserve the language (thus avoiding the need for a normalisation/simplification pass).&lt;/p&gt;
&lt;p&gt;The CPS language is formally defined (well-formed-ness and semantics have derivation rules). The translation from a reasonable fragment of ML into CPS is detailed. The CPS language is then extended to include exceptions and recursive functions.&lt;/p&gt;
&lt;p&gt;It is made clear that optimisations are all particular cases of &lt;span class=&quot;math&quot;&gt;β&lt;/span&gt; and &lt;span class=&quot;math&quot;&gt;η&lt;/span&gt; rewritings. This contrasts with MIL and ANF in that both require commuting conversion normalisation to be performed between optimisation passes—leading to &lt;span class=&quot;math&quot;&gt;&lt;em&gt;O&lt;/em&gt;(&lt;em&gt;n&lt;/em&gt;&lt;sup&gt;2&lt;/sup&gt;)&lt;/span&gt; complexity on some optimisation/normalisation interleavings.&lt;/p&gt;
&lt;h4 id=&quot;design-and-implementation-of-code-optimizations-for-a-type-directed-compiler-for-standard-ml&quot;&gt;Design and Implementation of Code Optimizations for a Type-Directed Compiler for Standard ML&lt;/h4&gt;
&lt;p&gt;by Tarditi, 1996, &lt;a href=&quot;http://research.microsoft.com/apps/pubs/default.aspx?id=68739&quot;&gt;available here&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;This PhD thesis explores different aspects of type-directed compilation (using the Typed Intermediate Language (TIL) framework. It uses an IR called LMLI (for &lt;span class=&quot;math&quot;&gt;λ&lt;sub&gt;&lt;em&gt;i&lt;/em&gt;&lt;/sub&gt;&lt;sup&gt;&lt;em&gt;M&lt;/em&gt;&lt;em&gt;L&lt;/em&gt;&lt;/sup&gt;&lt;/span&gt; or lambda intermediate ML). The optimisation phase is performed on a subset of LMLI similar to ANF with type annotations.&lt;/p&gt;
&lt;p&gt;The following arguments are presented concerning this choice of IR rather than CPS:&lt;/p&gt;
&lt;ol style=&quot;list-style-type: decimal&quot;&gt;
&lt;li&gt;the two are theoretically equivalent (there exist an efficient translation from one form to the other),&lt;/li&gt;
&lt;li&gt;direct-style (DS) (that is non-CPS)
&lt;ol style=&quot;list-style-type: lower-alpha&quot;&gt;
&lt;li&gt;makes it easier to re-use compilation techniques (e.g. from GHC), and&lt;/li&gt;
&lt;li&gt;is more suited to the set of implemented optimisations, even though&lt;/li&gt;
&lt;/ol&gt;&lt;/li&gt;
&lt;li&gt;CPS has a more powerful &lt;span class=&quot;math&quot;&gt;β&lt;/span&gt; inlining&lt;/li&gt;
&lt;/ol&gt;
&lt;p&gt;Concerning point (2a), it is interesting to note that some compilers (e.g. SML/NJ and some Scheme compilers) use CPS.&lt;/p&gt;
&lt;p&gt;Point (2b) is based on the idea that function calls and returns are difficult to distinguish in CPS. This is not necessarily true as some CPS languages have distinct lexical categories for these different types of continuations (e.g. in &lt;a href=&quot;http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.3.6773&quot;&gt;A Correspondence between Continuation Passing Style and Static Single Assignment Form&lt;/a&gt; by Kelsey, 1995)&lt;/p&gt;
&lt;h4 id=&quot;optimizing-nested-loops-using-local-cps-conversion&quot;&gt;Optimizing Nested Loops Using Local CPS Conversion&lt;/h4&gt;
&lt;p&gt;by Reppy, 2000, &lt;a href=&quot;http://moby.cs.uchicago.edu/papers/&quot;&gt;available here&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;Using a DS IR makes certain loop optimisations difficult (more specifically, nested loops can makes some calls appear as non-tail while they essentially are). Because this is specific to DS, it is proposed to transform appropriate parts of the program to CPS, hence the &lt;em&gt;local&lt;/em&gt; nature of CPS conversion.&lt;/p&gt;
&lt;p&gt;Detecting the parts that need conversion requires a traversal of the DS IR (and a fixed-point search to support mutually recursive functions) to build the environment associating program variables to the abstract continuations lattice: &lt;span class=&quot;math&quot;&gt;Γ : &lt;em&gt;v&lt;/em&gt;&lt;em&gt;a&lt;/em&gt;&lt;em&gt;r&lt;/em&gt; → (&lt;em&gt;l&lt;/em&gt;&lt;em&gt;a&lt;/em&gt;&lt;em&gt;b&lt;/em&gt;&lt;em&gt;e&lt;/em&gt;&lt;em&gt;l&lt;/em&gt; ∪ {⊤, ⊥})&lt;/span&gt;&lt;/p&gt;
&lt;p&gt;The LCPS conversion offers then the possibility to group functions into &lt;em&gt;clusters&lt;/em&gt;. Clusters can then be compiled as would loops: with only one stack frame (however many functions there are) and jump operations. Thus nested loops as well as &amp;quot;mutually tail recursive&amp;quot; (used for state-machine encoding) can be optimised.&lt;/p&gt;
&lt;p&gt;Just as pointed by Kennedy in &amp;quot;Compiling with Continuations, Continued&amp;quot;, defects of the DS IR are avoided introducing continuations.&lt;/p&gt;
&lt;h4 id=&quot;a-functional-perspective-on-ssa-optimisation-algorithms&quot;&gt;A Functional Perspective on SSA Optimisation Algorithms&lt;/h4&gt;
&lt;p&gt;by Chakravarty, Keller and Zadarnowski, 2003, &lt;a href=&quot;http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.14.6418&quot;&gt;available here&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;Mapping the ideas behind the Sparse Conditional Constant Propagation (SCCP) algorithm (see &lt;a href=&quot;http://www.cs.wustl.edu/~cytron/cs531/Resources/Papers/cprop.pdf&quot;&gt;Constant Propagation with Conditional Branches&lt;/a&gt; by Wegman and Zadeck, 1991) from SSA to ANF form. The ANF form, the algorithm, and associated proofs are exposed formally.&lt;/p&gt;
&lt;p&gt;The new algorithm (SCC&lt;sub&gt;ANF&lt;/sub&gt;) uses:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;an environment &lt;span class=&quot;math&quot;&gt;Γ &lt;/span&gt; mapping the set &lt;span class=&quot;math&quot;&gt;&lt;em&gt;V&lt;/em&gt;&lt;em&gt;a&lt;/em&gt;&lt;em&gt;r&lt;/em&gt;&lt;/span&gt; of variables onto the &lt;span class=&quot;math&quot;&gt;&lt;em&gt;A&lt;/em&gt;&lt;em&gt;b&lt;/em&gt;&lt;em&gt;s&lt;/em&gt; = {⊤, ⊥} ∪ &lt;em&gt;C&lt;/em&gt;&lt;em&gt;o&lt;/em&gt;&lt;em&gt;n&lt;/em&gt;&lt;em&gt;s&lt;/em&gt;&lt;em&gt;t&lt;/em&gt;&lt;/span&gt; lattice—where &lt;span class=&quot;math&quot;&gt;⊤&lt;/span&gt; is for variables for which there is no information, and &lt;span class=&quot;math&quot;&gt;⊥&lt;/span&gt; is for variables that do not receive concrete values&lt;sup&gt;&lt;a href=&quot;#fn1&quot; class=&quot;footnoteRef&quot; id=&quot;fnref1&quot;&gt;1&lt;/a&gt;&lt;/sup&gt;,&lt;/li&gt;
&lt;li&gt;a working set for remembering the things that remains to be done&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;and is two fold:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Analysis (looking for the biggest fix-point):
&lt;ul&gt;
&lt;li&gt;assign the &lt;span class=&quot;math&quot;&gt;⊤&lt;/span&gt; value to every &lt;span class=&quot;math&quot;&gt;&lt;em&gt;v&lt;/em&gt;&lt;em&gt;a&lt;/em&gt;&lt;em&gt;r&lt;/em&gt;&lt;/span&gt;&lt;/li&gt;
&lt;li&gt;fold over the program modifying the environment and the working set&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;li&gt;Specialisation (Using the Analysis's results):
&lt;ul&gt;
&lt;li&gt;fold over the program
&lt;ul&gt;
&lt;li&gt;removing dead-code (functions, not in &lt;span class=&quot;math&quot;&gt;Γ &lt;/span&gt;)&lt;/li&gt;
&lt;li&gt;replacing the constants by their values&lt;/li&gt;
&lt;li&gt;replacing non-terminating computations by an infinite loop—with some tricks for keeping side-effects.&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;h3 id=&quot;code&quot;&gt;Code&lt;/h3&gt;
&lt;p&gt;In order to understand the relationship between the different IRs, I'll try to port algorithms from SSA to CPS (and, if time constraints allow it, ANF). This translation is more interesting than its inverse because:&lt;/p&gt;
&lt;ol style=&quot;list-style-type: lower-alpha&quot;&gt;
&lt;li&gt;SSA algorithms are plenty and easy to find, and&lt;/li&gt;
&lt;li&gt;&lt;span class=&quot;math&quot;&gt;λ&lt;/span&gt;-calculus has strong theoretical foundations.&lt;/li&gt;
&lt;/ol&gt;
&lt;p&gt;Item (a) widens the scope of the study and item (b) makes results easier to interpret (or so I hope).&lt;/p&gt;
&lt;p&gt;Because it is easy—once one has the dominator relation computed—to translate SSA into CPS the problem of porting an optimisation &lt;span class=&quot;math&quot;&gt;&lt;em&gt;O&lt;/em&gt;&lt;em&gt;p&lt;/em&gt;&lt;em&gt;t&lt;/em&gt;&lt;em&gt;i&lt;/em&gt;&lt;em&gt;m&lt;/em&gt;&lt;sub&gt;&lt;em&gt;s&lt;/em&gt;&lt;em&gt;s&lt;/em&gt;&lt;em&gt;a&lt;/em&gt;&lt;/sub&gt;&lt;/span&gt; is to close following diagram; that is, finding the &lt;span class=&quot;math&quot;&gt;&lt;em&gt;O&lt;/em&gt;&lt;em&gt;p&lt;/em&gt;&lt;em&gt;t&lt;/em&gt;&lt;em&gt;i&lt;/em&gt;&lt;em&gt;m&lt;/em&gt;&lt;sub&gt;&lt;em&gt;c&lt;/em&gt;&lt;em&gt;p&lt;/em&gt;&lt;em&gt;s&lt;/em&gt;&lt;/sub&gt;&lt;/span&gt;.&lt;/p&gt;
&lt;pre&gt;&lt;code&gt;      ssa ----------Translate--------------&amp;gt; Translate(ssa) =      cps
       |                                                            ¦
       |                                                            ¦
   Optim_ssa                                                   Optim_cps?
       |                                                            ¦
       |                                                            ¦
       V                                                            V
  Optim_ssa(ssa) ---Translate---&amp;gt; Translate(Optim_ssa(ssa)) = Optim_cps(cps)?
&lt;/code&gt;&lt;/pre&gt;
&lt;p&gt;Of course the trivial solution &lt;span class=&quot;math&quot;&gt;&lt;em&gt;T&lt;/em&gt;&lt;em&gt;r&lt;/em&gt;&lt;em&gt;a&lt;/em&gt;&lt;em&gt;n&lt;/em&gt;&lt;em&gt;s&lt;/em&gt;&lt;em&gt;l&lt;/em&gt;&lt;em&gt;a&lt;/em&gt;&lt;em&gt;t&lt;/em&gt;&lt;em&gt;e&lt;/em&gt;&lt;sup&gt; - 1&lt;/sup&gt;(&lt;em&gt;O&lt;/em&gt;&lt;em&gt;p&lt;/em&gt;&lt;em&gt;t&lt;/em&gt;&lt;em&gt;i&lt;/em&gt;&lt;em&gt;m&lt;/em&gt;&lt;sub&gt;&lt;em&gt;s&lt;/em&gt;&lt;em&gt;s&lt;/em&gt;&lt;em&gt;a&lt;/em&gt;&lt;/sub&gt;(&lt;em&gt;T&lt;/em&gt;&lt;em&gt;r&lt;/em&gt;&lt;em&gt;a&lt;/em&gt;&lt;em&gt;n&lt;/em&gt;&lt;em&gt;s&lt;/em&gt;&lt;em&gt;l&lt;/em&gt;&lt;em&gt;a&lt;/em&gt;&lt;em&gt;t&lt;/em&gt;&lt;em&gt;e&lt;/em&gt;(. )))&lt;/span&gt; is correct but useless as our goal is to understand the relation between SSA and CPS—finding &lt;span class=&quot;math&quot;&gt;&lt;em&gt;O&lt;/em&gt;&lt;em&gt;p&lt;/em&gt;&lt;em&gt;t&lt;/em&gt;&lt;em&gt;i&lt;/em&gt;&lt;em&gt;m&lt;/em&gt;&lt;sub&gt;&lt;em&gt;c&lt;/em&gt;&lt;em&gt;p&lt;/em&gt;&lt;em&gt;s&lt;/em&gt;&lt;/sub&gt;&lt;/span&gt; is a mean of exploration. I do not expect every algorithm to be translatable in a pseudo-commuting manner. Pseudo-commutation might only be verified on some particular subset of SSA/CPS.&lt;/p&gt;
&lt;p&gt;Additionally, I am writing a parser from LLVM human-readable assembly to my own SSA format so as to be able to use the optimization algorithms available in the LLVM project. My custom SSA form is close enough to LLVM's. target architecture information, calling convention annotations, and other details are ignored, and some invariants are enforced in the type system.&lt;/p&gt;
&lt;div class=&quot;footnotes&quot;&gt;
&lt;hr/&gt;
&lt;ol&gt;
&lt;li id=&quot;fn1&quot;&gt;&lt;p&gt;Absence of concrete value can be either due to code being dead or as a result of non-terminating computations. &lt;a href=&quot;#fnref1&quot; class=&quot;footnoteBackLink&quot;&gt;↩&lt;/a&gt;&lt;/p&gt;&lt;/li&gt;
&lt;/ol&gt;
&lt;/div&gt;
</description><pubDate>18 Jun 2012 08:00:00 +0000</pubDate><category>compilation</category><category>cps</category><category>ssa</category><category>optimisations</category><category>compiler intermediate languages</category><guid isPermaLink="true">http://gallium.inria.fr/~scherer/gagallium/ssa-cps-reading-notes/index.html</guid></item></channel></rss>
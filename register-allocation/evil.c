int evil(int x1, int x2, int x3, int x4, int x5, int x6, int x7, int x8) {
  int i;
  int y = 0;
  int times = x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8;
  for(i = 0; i < times; i++) x1 += x1;
  for(i = 0; i < times; i++) x2 += x2;
  for(i = 0; i < times; i++) x3 += x3;
  for(i = 0; i < times; i++) x4 += x4;
  for(i = 0; i < times; i++) x5 += x5;
  for(i = 0; i < times; i++) x6 += x6;
  for(i = 0; i < times; i++) x7 += x7;
  for(i = 0; i < times; i++) x8 += x8;
  return x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8;
}

int main() {
  int p = 10000000;
  return evil(p, p, p, p, p, p, p, p);
}

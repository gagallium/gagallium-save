module type FUNCTOR = sig
  type 'a t
  val map : ('a -> 'b) -> ('a t -> 'b t)
  (* with laws:
       map (id : 'a -> 'a) = (id : 'a t -> 'a t)
       map (f ∘ g) = map f ∘ map g
  *)
end

module Fixpoint (F : FUNCTOR) : sig
  (* with -rectypes *)
  type fix = fix F.t
  val fold : ('a F.t -> 'a) -> (fix -> 'a)
  val unfold : ('a -> 'a F.t) -> ('a -> fix)
end = struct
  type fix = fix F.t
  let rec fold f d   = f (F.map (fold f) d)
  let rec unfold f d = F.map (unfold f) (f d)
end

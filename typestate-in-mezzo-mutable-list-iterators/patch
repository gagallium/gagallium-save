diff --git a/typestate-in-mezzo-mutable-list-iterators/index.html b/typestate-in-mezzo-mutable-list-iterators/index.html
index edefc02..65eba1e 100644
--- a/typestate-in-mezzo-mutable-list-iterators/index.html
+++ b/typestate-in-mezzo-mutable-list-iterators/index.html
@@ -370,11 +370,6 @@ iterator.
 Consequently, the definition of `iterator` changes a bit: an `iterator` is now
 also parametrized by a permission `hole`, which in fact means "what does the
 iterator need to fill its hole and be able to generate `post`".  
-We also need a new function, `get`, that detaches an element from the iterator
-(if there is one). `next` purpose will be to fill the hole.
-
-![The typestate has slightly changed, as the state "living iterator" has been
-split into "iterator without hole" and "iterator with hole".](ts2.png)
 
 Here is the new definition of `iterator`. Note that it doesn't contains `hole`,
 but we need it to generate `post`:
@@ -383,7 +378,7 @@ but we need it to generate `post`:
 data mutable iterator a (hole: perm) (post: perm) = Iterator {
   content: { p: perm } (
     xs: list a,
-    rewind: (| p * hole * xs @ list a) -> (| post)
+    rewind: (| p * hole * l @ list a) -> (| post)
     | p
   )
 }
@@ -392,11 +387,14 @@ data mutable iterator a (hole: perm) (post: perm) = Iterator {
 Thus, an iterator without a hole is an `iterator a empty post`, while an
 iterator that has given away `x @ a` to the user is a `iterator a (x @ a) post`.
 
-We can now write `get`, which -- in my implementation -- does all the job:
+We can now write `next`. It takes an iterator parametrized by any permission
+`hole`, the permission `hole` itself, and implicitly fills the hole by merging
+`hole` into `p`. It finally returns the next element (if any).
 
 ```ocaml
-val get [a, post: perm] (consumes it: iterator a empty post):
+val next [a, hole: perm, post: perm] (consumes (it: iterator a hole post | hole)):
   offer post (x: a | it @ iterator a (x @ a) post) =
+
   let xs, rewind = it.content in
   match xs with
   | Nil ->
@@ -408,35 +406,31 @@ val get [a, post: perm] (consumes it: iterator a empty post):
   end
 ```
 
-On the other side, `next` only combines the permissions of an iterator with a
-hole and the element needed to fill the hole:
-
-```ocaml
-val next [a, x: term, post: perm] (consumes (s: iterator a (x @ a) post | x @ a)):
-                                  (| s @ iterator a empty post) =
-  ()
-```
-
-We can now use this iterator:
-
+And we can now use this iterator:
 ```ocaml
 (* [nth] takes an iterator [it] and an integer [n], makes him go forward of [n]
    steps, and then returns it (if it hasn't been consumed) *)
-val rec nth [a, post: perm] (consumes it: iterator a empty post, n: int):
+val rec nth [a, hole: perm, post: perm]
+  (consumes (it: iterator a hole post | hole), n: int):
   offer (x: a | it @ iterator a (x @ a) post) post =
-  match get it with
+
+  match next [hole = hole] it with
   | None ->
     None
   | Some { x } ->
     if n <= 0 then (
       Some { x = x }
     ) else (
-      next [x = x] it;
-      nth [a = a] (it, n-1)
+      nth [a = a, hole = (x @ a)] (it, n-1)
     )
   end 
 ```
 
+You can note that we have sometimes to instantiate by hand the polymorphic
+parameters when calling a function. For example, here, when calling recursively
+`nth`, we have to say that a previous call to `next` has created a hole of
+"shape" `x @ a` we want to merge back to continue the iteration.
+
 # The cherry on top
 
 So, here it is, an iterator on lists!  
diff --git a/typestate-in-mezzo-mutable-list-iterators/listiterator.mz b/typestate-in-mezzo-mutable-list-iterators/listiterator.mz
index f4a5cff..0182ca3 100644
--- a/typestate-in-mezzo-mutable-list-iterators/listiterator.mz
+++ b/typestate-in-mezzo-mutable-list-iterators/listiterator.mz
@@ -10,12 +10,12 @@ data mutable iterator a (hole: perm) (post: perm) = Iterator {
   )
 }
 
-data offer (post: perm) a =
+data offer a (post: perm) =
 | None { | post }
 | Some { x: a }
 
-val get [a, post: perm] (consumes it: iterator a empty post):
-                        offer post (x: a | it @ iterator a (x @ a) post) =
+val next [a, hole: perm, post: perm] (consumes (it: iterator a hole post | hole)):
+  offer (x: a | it @ iterator a (x @ a) post) post =
   match it.content with
   | Nil ->
     convert ();
@@ -25,10 +25,6 @@ val get [a, post: perm] (consumes it: iterator a empty post):
     Some { x = head }
   end 
 
-val next [a, x: term, post: perm] (consumes (it: iterator a (x @ a) post | x @ a)):
-                                  (| it @ iterator a empty post) =
-  ()
-
 val stop [a, hole: perm, post: perm] (consumes (it: iterator a hole post | hole)): (| post) =
   convert ()
 
@@ -37,34 +33,36 @@ val new [a] (consumes l: list a): iterator a empty (l @ list a) =
 
 (* -------------------------------------------- *)
 
-val rec loop [a, post: perm] (consumes it: iterator a empty post, f: a -> ()): (| post) =
-  match get it with
+val rec loop [a, hole: perm, post: perm]
+  (consumes (it: iterator a hole post | hole), f: a -> ()): (| post) =
+
+  match next [hole = hole] it with
   | None ->
+    assert post;
     ()
   | Some { x } ->
     f x;
-    next [x = x] it;
-    loop (it, f)
+    loop [hole = (x @ a)] (it, f)
   end
 
-val rec nth [a, post: perm] (consumes it: iterator a empty post, n: int):
-                            offer post (x: a | it @ iterator a (x @ a) post) =
-  match get it with
+val rec nth [a, hole: perm, post: perm] (consumes it: (iterator a hole post | hole), n: int):
+  offer (x: a | it @ iterator a (x @ a) post) post =
+
+  match next [hole = hole] it with
   | None ->
     None
   | Some { x } ->
     if n <= 0 then (
       Some { x = x }
     ) else (
-      next [x = x] it;
-      nth [a = a] (it, n-1)
+      nth [a = a, hole = (x @ a)] (it, n-1)
     )
   end
 
 val _ =
   let l = three (1, 2, 3) in
   let it = new l in
-  match nth (it, 1) with
+  match nth [hole = empty] (it, 1) with
   | None ->
     ()
   | Some { x } ->
@@ -75,3 +73,4 @@ val _ =
     stop it
   end
   (* l @ list int *)
+

<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0"><channel><title>Gagallium: hamlet</title><link>http://gallium.inria.fr/~scherer/gagallium/kw_hamlet.html</link><description>A draft blog at Gallium</description><managingEditor>gabriel.scherer@inria.fr</managingEditor><pubDate>07 Jun 2012 08:00:00 +0000</pubDate><lastBuildDate>07 Jun 2012 08:00:00 +0000</lastBuildDate><generator>Stog</generator><item><title>Gallium submissions to the ML Workshop 2012</title><link>http://gallium.inria.fr/~scherer/gagallium/ml-workshop-2012-submissions/index.html</link><description>&lt;p&gt;I hope you know about the &lt;a href=&quot;http://oud.ocaml.org/2012/&quot;&gt;OCaml Users and Developers
Meeting&lt;/a&gt;, a continuation of previous
years' &amp;quot;OCaml community meetings&amp;quot; that will be colocated with
&lt;a href=&quot;http://icfpconference.org/icfp2012/&quot;&gt;ICFP&lt;/a&gt; in Denmark next September;
and that you are considering attending! Some Gallium members will
certainly also attend the &lt;a href=&quot;http://www.lexifi.com/ml2012/&quot;&gt;ML Workshop
2012&lt;/a&gt;, another satellite that is less
OCaml-focused and maybe more research-oriented than
community-oriented. By mere coincidence, it happens that we submitted
four different proposals to this workshop. Plenty of
activity!&lt;/p&gt;




&lt;p&gt;The proposals are only two pages long, so you should feel
free to have a look, react, and ask questions. Please keep in mind
that they describe work in progress, and are therefore maybe not as
polished as your usual research article.&lt;/p&gt;

&lt;p&gt;Jonathan Protzenko and François Pottier submitted &lt;a href=&quot;http://xulforum.org/papers/extended-abstract.pdf&quot;&gt;a talk
proposal&lt;/a&gt; about
Hamlet, a new programming language they are working on. Hamlet
provides fine-grained control over state mutation by tracking aliasing
and ownership of state, without being so complex that only type system
researchers can use it. In case you want to know more about Hamlet,
they have already written an &lt;a href=&quot;http://gallium.inria.fr/~fpottier/publis/hamlet-tutorial.pdf&quot;&gt;introduction to
Hamlet&lt;/a&gt;
(PDF, 20 pages), also available in &lt;a href=&quot;http://gallium.inria.fr/~fpottier/publis/hamlet-tutorial-long.pdf&quot;&gt;a long
version&lt;/a&gt;
(PDF, 27 pages).&lt;/p&gt;

&lt;p&gt;Jacques Garrigue and Didier Rémy submitted &lt;a href=&quot;http://gallium.inria.fr/~remy/gadts/Garrigue-Remy:gadts@abs2012.pdf&quot;&gt;a
proposal&lt;/a&gt;
about inference for GADT (which were very recently added
to OCaml). Doing inference in presence of GADT is difficult because of
ambiguities that may arise; in particular, principality is in
danger. There has been active research on this problem, with works for
example by &lt;a href=&quot;http://gallium.inria.fr/~fpottier/biblio/pottier.html#simonet-pottier-hmg-toplas&quot;&gt;Simonet and
Pottier&lt;/a&gt;
(2004), &lt;a href=&quot;http://arxiv.org/abs/cs/0507037&quot;&gt;Struckey and Sulzmann&lt;/a&gt;
(2005), &lt;a href=&quot;http://www.pps.univ-paris-diderot.fr/~yrg/research.php#pottier-regis-gianas-06&quot;&gt;Régis-Gianas and
Pottier&lt;/a&gt;
(2006), &lt;a href=&quot;http://research.microsoft.com/en-us/um/people/simonpj/papers/gadt/index.htm&quot;&gt;Peyton-Jones, Vytiniotis, Weirich and
Washburn&lt;/a&gt;
(2004-2006) and finally &lt;a href=&quot;http://research.microsoft.com/apps/pubs/default.aspx?id=79812&quot;&gt;Schrijvers, Peyton-Jones, Sulzmann, and
Vytiniotis&lt;/a&gt;
(2009). Jacques and Didier are considering a new technique, based on
a finer-grained notion of ambiguity, that is quite lightweight and
integrates well with the current type system implementation.&lt;/p&gt;

&lt;p&gt;Xavier Clerc (whom we are lucky to have as a part-time
research engineer) sent &lt;a href=&quot;http://ocamljava.x9c.fr/ocamljava.pdf&quot;&gt;a
proposal&lt;/a&gt; about his recent
progress on the OCaml-Java project, which is quite
impressive. OCaml-Java compiles OCaml programs to run on the Java
Virtual Machine, and provides an interoperability layer to interact at
runtime with Java objects. It used to take a very large performance
hit compared to the existing native or bytecode compilers, but recent
work by Xavier has made it much more competitive, joining the select
club of third-party compilers that stand between native and bytecode
compilation, whose only members were, as far as I know,
&lt;a href=&quot;http://benediktmeurer.de/2010/11/16/ocamljit-20/&quot;&gt;OCamlJIT&lt;/a&gt; and
&lt;a href=&quot;http://ocsigen.org/js_of_ocaml/manual/&quot;&gt;js_of_ocaml&lt;/a&gt;.&lt;/p&gt;

&lt;p&gt;Finally, Didier and I submitted &lt;a href=&quot;http://gallium.inria.fr/~scherer/drafts/ml_workshop_2012.pdf&quot;&gt;another
proposal&lt;/a&gt;
about GADT, this time focused on its interaction with subtyping, and
in particular the variance check for type parameters of a GADT
declaration. This is the result of questions I have stumbled upon
mostly by chance (or misfortune) and asked on the mailing-list; it
turns out there has been little work on combining GADT and subtyping
(Vincent Simonet did a bit of that with François, and their paper is
a great help). Since this mailing-list discussion I have done a bit of
formal work, but it does not appear in the proposal. In fact, formal
criteria used to be present, until Jonathan and Jacques-Henri most
helpfully pointed out that it was impossible to understand (given the
space constraints rather than my writing style, I hope). I went for an
example-oriented approach which is, I think, much better.&lt;/p&gt;

&lt;p&gt;Summing up, and hot off the press:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;&lt;a href=&quot;http://xulforum.org/papers/extended-abstract.pdf&quot;&gt;Programming with permissions: the HaMLet language (extended abstract)&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;http://gallium.inria.fr/~remy/gadts/Garrigue-Remy:gadts@abs2012.pdf&quot;&gt;Tracing ambiguity in GADT type inference&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;http://ocamljava.x9c.fr/ocamljava.pdf&quot;&gt;OCaml-Java : Ocaml on the JVM&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;http://gallium.inria.fr/~scherer/drafts/ml_workshop_2012.pdf&quot;&gt;GADT meet subtyping&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;Of course, some of these proposals may be rejected. That's the point
of celebrating when you submit something rather than once you get the
reviews back: you don't get to rejoice in front of your colleague in
tears. In any case, you will probably hear about these works again in
the future, if you are interested.&lt;/p&gt;
</description><pubDate>07 Jun 2012 08:00:00 +0000</pubDate><category>research</category><category>conference</category><category>ml2012</category><category>hamlet</category><category>GADT</category><category>inference</category><category>subtyping</category><guid isPermaLink="true">http://gallium.inria.fr/~scherer/gagallium/ml-workshop-2012-submissions/index.html</guid></item></channel></rss>
Set Implicit Arguments.

Lemma Zdiv_le : forall a b: int, (0 <= a) -> (1 <= b) -> (Zdiv a b <= a).
Proof. admit. Qed.

Require Import CFLib CFLibCredits.
Require Import RandomAccessListSig_ml (* RandomAccessListSig_proof *).
Require Import BinaryRandomAccessList_ml.
Require Import LibListZ.
Require Import LibFunOrd.
Require Import LibFilter LibFilterTowardsInfinity LibFilterEverywhere.
Require Import LibFilterProductFixedComponent.
Require Import LibMonotonic.
Require Import LibDominated LibDominatedFacts.
Require Import LibIDominated LibIDominatedFacts.
Require Import CFSpecO.

Module BinaryRandomAccessListSpec (* <: RandomAccessListSigSpec *).

(** instantiations *)

Module Import R <: MLRandomAccessList := MLBinaryRandomAccessList.
Import MLBinaryRandomAccessList.

(** invariant *)

Section Polymorphic.
Variables (a : Type).

Inductive btree : int -> tree a -> list a -> Prop :=
  | btree_nil : forall x,
      btree 0 (Leaf x) (x::nil)
  | btree_cons : forall p p' n t1 t2 L1 L2 L',
      btree p t1 L1 ->
      btree p t2 L2 ->
      p' =' p+1 ->
      n =' 2^p' ->
      L' =' L1 ++ L2 ->
      btree p' (Node n t1 t2) L'.

Inductive inv : int -> rlist a -> list a -> Prop :=
  | inv_nil : forall p,
      p >= 0 ->
      inv p nil nil
  | inv_cons : forall p (t: tree a) ts d L L' T,
      inv (p+1) ts L ->
      L' <> nil ->
      p >= 0 ->
      (match d with
       | Zero => L = L'
       | One t => btree p t T /\ L' = T ++ L
       end) ->
      inv p (d :: ts) L'.

Definition Rlist (s: rlist a) (L: list a) :=
  inv 0 s L.

End Polymorphic.

Implicit Arguments btree [a].
Implicit Arguments inv [a].

(** automation *)

(* Let [math] know that [length] is in fact a nat,
   this way, we automatically have [0 <= length L] for all [L]. *)
Ltac math_0 ::= unfold length in *.
(* Use [eauto with maths] in ~-suffixed tactics *)
Ltac auto_tilde ::= eauto with maths.

(* Do not try to unfold Z.mul, e.g. in [2 * n] *)
Opaque Z.mul.

Hint Constructors btree inv.
Hint Extern 1 (@lt nat _ _ _) => rew_list; math.
Hint Extern 1 (@rep Z _ _ _ _) => reflexivity.
Hint Resolve ZNth_zero ZUpdate_here ZUpdate_not_nil.
Hint Resolve app_not_empty_l app_not_empty_r.

Section Polymorphic'.
Variables (a : Type).

Implicit Types L: list a.
Implicit Types ts: list (digit a).

(** useful facts *)

Fixpoint tree_size (t:tree a) : nat :=
  match t with
  | Leaf _ => 0%nat
  | Node _ t1 t2 => (1 + tree_size t1 + tree_size t2)%nat
  end.

Definition Size (t:tree a) :=
  match t with
  | Leaf _ => 1
  | Node w _ _ => w
  end.

Lemma btree_size_correct : forall p t L,
  btree p t L -> Size t = 2^p.
Proof. introv Rt. inverts~ Rt. Qed.
Hint Resolve btree_size_correct.

Lemma btree_p_pos : forall p t L,
  btree p t L -> p >= 0.
Proof. introv Rt. inductions Rt; math. Qed.
Hint Resolve btree_p_pos.

Lemma length_correct : forall t p L,
  btree p t L -> length L = 2^p :> int.
Proof.
  introv Rt. induction Rt. auto.
  unfolds eq'. subst. rew_length. rewrite~ pow2_succ.
Qed.

Lemma btree_size_pos : forall p t L,
  btree p t L -> p >= 0.
Proof. introv Rt. induction Rt; unfolds eq'; math. Qed.

Hint Resolve btree_size_pos.

Lemma to_empty : forall p L,
  inv p nil L -> L = nil.
Proof. introv RL. inverts~ RL. Qed.

Lemma from_empty : forall p ts,
  inv p ts nil -> ts = nil.
Proof. introv RL. inverts RL; auto_false. Qed.

Lemma btree_not_empty : forall p t L,
  btree p t L -> L <> nil.
Proof.
  introv Rt. lets: (length_correct Rt). intro_subst_hyp.
  rew_length in H. forwards~: (@pow2_pos p). math.
Qed.

Hint Resolve btree_not_empty.

Lemma group_ineq : forall n m, 0 <= n - m -> m <= n.
Proof. intros. math. Qed.

(* Useless *)
Lemma inv_length_correct : forall ts p L,
    @inv a p ts L ->
    If ts = nil then
      length L = 0
    else
      (2 ^ p <= length L <= 2 ^ (length ts + p) - 2 ^ p).
Proof.
  induction ts.
  - intros. case_if. rewrites~ (>> to_empty ___).
  - { rename a0 into d. case_if; intros p L Rdts.
      - { inverts Rdts as. intros L0 T _t Rts Lnnil ?. forwards IH: IHts Rts.
          forwards~: pow2_pos p.
          destruct d; intros; subst.
          - { assert (ts <> nil). { intro TS. apply Lnnil. subst. applys~ to_empty. }
              case_If. destruct IH as [IH1 IH2]. split.
              - rew_ineq <- IH1. rew_pow~ 2 p. 
              - rewrite~ length_cons. rew_ineq IH2. rew_pow~ 2 p. }
          - { case_If.
              - { unpack; subst. forwards~: (>> to_empty ___); subst. split.
                  - forwards~: (>> length_correct ___). rew_list~.
                  - rew_length~. forwards~ Tlen: (>> length_correct ___). rew_pow~ 2 p. }
              - { destruct IH as [IH1 IH2]; unpack; subst.
                  split; forwards~ Tlen: (>> length_correct ___); rew_length~.
                  rew_ineq IH2. rewrite Tlen. rew_pow~ 2 p. } } } }
Qed.

Lemma inv_ts_len : forall ts p L,
    @inv a p ts L -> ts <> nil -> 2 ^ (p + (length ts) - 1) <= length L.
Proof.
  induction ts.
  - intros. auto_false.
  - { rename a0 into d. intros p L Rdts. inverts Rdts as. intros L0 T _t Rts Lnnil ? ? ?.
      destruct ts.
      - { (* ts = nil // interesting case *)
          destruct d; intros; subst. (* [d] must be a [One t] for the invariant to hold. *)
          - forwards~: (>> to_empty ___); auto_false.
          - unpack; subst. forwards~: (>> to_empty ___); subst.
            forwards~ lenT: (>> length_correct ___).
            rew_length. rew_pow~ 2 p. }
      - { forwards IH: IHts Rts. auto_false.
          destruct d; intros; unpack; subst; rew_length in *;
          rew_pow~ 2 p in IH; rew_pow~ 2 p. } }
Qed.

(* move to tlc? *)
Lemma Zdiv_le : forall a b: int, (0 <= a) -> (1 <= b) -> (Zdiv a b <= a).
Proof. admit. Qed.

Lemma ts_bound : forall ts p L,
    inv p ts L -> 2 ^ (length ts) <= 2 * (length L) + 1.
Proof.
  destruct ts; intros.
  - { forwards~: (>> to_empty ___). subst; rew_length. reflexivity. }
  - { forwards I: inv_ts_len H. congruence.
      assert (2 ^ length (d::ts) <= 2 * (length L) / 2 ^ p). {
        forwards~ I': int_le_mul_pos_l 2 I.
        forwards~ I'': Z_div_le (2^p) I'. inverts H. forwards~: (>> pow2_pos ___).
        rewrite <-pow2_succ in I''. rewrites~ <-Z.pow_sub_r in I''.
        math_rewrite~ ((((p + length (d :: ts)) - 1) + 1) - p = length (d :: ts)) in I''.
        inverts~ H. rew_length. inverts~ H.
      }
      inverts H. forwards~: (>> pow2_pos ___).
      forwards~: Zdiv_le (2 * length L) (2^p). }
Qed.

Lemma ts_bound_log : forall ts p L,
    inv p ts L -> length ts <= Z.log2 (2 * (length L) + 1).
Proof.
  intros. forwards~: ts_bound. forwards~: Z.log2_le_mono. rewrites~ Z.log2_pow2 in *.
Qed.

(* Useless *)
Lemma p_bound_log : forall ts p L,
    inv p ts L -> If ts <> nil then p <= Z.log2 (length L) else True.
Proof.
  introv Rts. forwards~ inv_bounds: inv_length_correct. case_If.
  - case_If. destruct inv_bounds as [I1 _].
    forwards~: Z.log2_le_mono. rewrites~ Z.log2_pow2 in *.
    inverts~ Rts.
  - auto.
Qed.

(** verification *)

Lemma pow2_succ_div : forall p, p >= 0 -> 2 ^ (p+1) / 2 = 2 ^ p.
Proof.
  intros. rewrite~ pow2_succ. rewrite~ div2_odd.
Qed.

Lemma simpl_zero_credits : forall n, n = 0 -> \$ n ==> \[].
Proof. intros. subst. rewrite <-credits_int_zero_eq. hsimpl. Qed.

Lemma empty_spec :
  Rlist (@empty a) (@nil a).
Proof. rewrite (empty_cf a). constructors~. Qed.

Hint Extern 1 (RegisterSpec empty) => Provide empty_spec.

Lemma is_empty_spec :
  SpecO1 (fun F =>
    Spec is_empty (l: rlist a) |R>>
      R (\$ F tt) (fun (b: bool) => \[b = isTrue (l = nil)])).
Proof.
  xcfO (# 1).
  xcf. intros; xpay. xgo; hsimpl; fold_bool; fold_prop; eauto.
Qed.

(* private *)
Lemma size_spec :
  SpecO1 (fun F =>
    Spec size (t: tree a) |R>>
      R (\$ F tt) (fun (n: int) => \[n = Size t])).
Proof.
  xcfO (# 1). xcf. intros; xpay. xgo~.
Qed.

(* private *)
Lemma link_spec :
  SpecO1 (fun F =>
    Spec link (t1:tree a) (t2:tree a) |R>>
      forall p L1 L2, btree p t1 L1 -> btree p t2 L2 ->
      R (\$ F tt) (fun t' => \[btree (p+1) t' (L1++L2)])).
Proof.
  destruct size_spec as [size_cost ?]; unpack.
  xcfO (# 2 * (size_cost tt) + 1). 
  specialize (H tt).
  xcf. intros. xpay; csimpl. repeat xapps; csimpl. apply~ simpl_zero_credits.
  xret. hsimpl. constructors~.
  do 2 (erewrite btree_size_correct; eauto).
  rewrite~ pow2_succ.
Qed.

Lemma cons_tree_spec_aux :
  SpecO (fun n => n) (fun F =>
    Spec cons_tree (t: tree a) (ts: rlist a) |R>>
      forall p T L, btree p t T -> inv p ts L ->
      R (\$ F (length ts)) (fun ts' => \[inv p ts' (T++L)])).
Proof.
  destruct link_spec as (link_cost & link_cost_nonneg & ? & ?).
  applys @SpecO_of_SpecO_after 0.
  specialize (link_cost_nonneg tt).
  xcfO (fun n => 1 + (1 + (link_cost tt)) * n).
  intros F' FeqF'. 
  xinduction (fun (t:tree a) (ts:rlist a) => LibList.length ts).
  xcf. intros ? ts. introv IH Rt Rts. rewrites~ FeqF'.
  inverts Rts.
  - xpay. csimpl. xgo; hsimpl; constructors~.
  - { xpay. csimpl. simpl_nonneg~.
      xmatch.
      - xret; hsimpl; constructors~; subst; splits~.
      - unpack; subst. xapps~.
        { csimpl. rew_length. math_nia. }
        intros. xapps~.
        { rewrites~ FeqF'. csimpl; rew_length; math_nia. }
        intros. xret.
        { hsimpl. constructors~. rew_list~. } }
Qed.

(* private *)
Lemma cons_tree_spec :
  SpecO Z.log2 (fun F =>
    Spec cons_tree (t: tree a) (ts: rlist a) |R>>
      forall p T L, btree p t T -> inv p ts L ->
      R (\$ F (length L)) (fun ts' => \[inv p ts' (T++L)])).
Proof.
  destruct cons_tree_spec_aux
    as (cons_tree_cost & cost_pos & cost_mon & cost_dom & cons_tree_spec).
  xcfO (fun n => cons_tree_cost (Z.log2 (2 * n + 1))).
  - applys~ @monotonic_comp. monotonic_Z_auto~. 
  - applys @idominated_transitive. applys~ @idominated_comp cost_dom.
    monotonic_Z_auto. monotonic_Z_auto~. simpl. idominated_Z_auto~.
  - xweaken~. do 4 intro. intro spec. intros. xgc; [xapply~ spec |]; csimpl~.
    { apply~ cost_mon. apply~ ts_bound_log. }
Qed.

Lemma cons_spec :
  SpecO Z.log2 (fun F =>
    Spec cons (x: a) (l: rlist a) |R>>
      forall L, Rlist l L ->
      R (\$F (length L)) (fun l' => \[Rlist l' (x::L)])).
Proof.
  destruct cons_tree_spec as (cons_tree_cost & ? & ? & ? & ?).
  xcfO (fun n => 1 + cons_tree_cost n).
  xcf. introv RL. xpay. csimpl~.
  xapp~; csimpl~.
Qed.

(* private *)
Lemma uncons_tree_spec_aux :
  SpecO (fun n => n) (fun F =>
    Spec uncons_tree (ts:rlist a) |R>>
      forall p L, inv p ts L -> ts <> nil ->
      R (\$F (length ts))
        (fun r => let (t',ts') := r : tree a * rlist a in
                  \[exists T' L', btree p t' T' /\ inv p ts' L' /\ L = T' ++ L'])).
Proof.
  applys @SpecO_of_SpecO_after 0.
  xcfO (fun n => n + 1).
  intros F' eqF'.
  xinduction (fun (ts:rlist a) => LibList.length ts).
  xcf. introv IH Rts Ne. rewrites~ eqF'. xpay; csimpl. xmatch; inverts Rts as.
  - intros L0 T RT RL ? ? ?; unpack. xgo.
    inverts RL. subst. hsimpl. exists___~.
  - intros L0 T RT RL ? ? ?; unpack. xgo.
    asserts: (L0 <> nil). { intro_subst_hyp. eapply C0. fequals. apply~ from_empty. }
    subst. hsimpl. exists___~.
  - { intros _t ? ? RL.
      asserts: (ts <> nil). { intro_subst_hyp. inverts~ RL. }
      xapp~ as [t l]. { csimpl; rewrite~ eqF'. rew_length~. } intros (?&?&BT&?&?).
      xmatch; xgo; inverts BT.
      - hsimpl. maths (p0 = p). subst. exists___. rew_list. splits~.
      - math.
      - applys~ C2. }
Qed.

Lemma uncons_tree_spec :
  SpecO Z.log2 (fun F =>
    Spec uncons_tree (ts:rlist a) |R>>
      forall p L, inv p ts L -> ts <> nil ->
      R (\$F (length L))
        (fun r => let (t',ts') := r : tree a * rlist a in
                  \[exists T' L', btree p t' T' /\ inv p ts' L' /\ L = T' ++ L'])).
Proof.
  destruct uncons_tree_spec_aux
    as (uncons_tree_cost & ? & uncons_tree_mon & uncons_tree_dom & ?).
  xcfO (fun n => uncons_tree_cost (Z.log2 (2 * n + 1))).
  - applys~ @monotonic_comp. monotonic_Z_auto~.
  - applys @idominated_transitive. applys~ @idominated_comp uncons_tree_dom.
    monotonic_Z_auto. monotonic_Z_auto~. simpl. idominated_Z_auto~.
  - xweaken~. intros ? ? ? spec. intros.
    xgc; [xapply~ spec |]; csimpl~.
    { applys~ uncons_tree_mon. applys~ ts_bound_log. }
Qed.

Lemma head_spec :
  SpecO Z.log2 (fun F =>
    Spec head (l: rlist a) |R>>
      forall L, Rlist l L -> l <> nil ->
      R (\$F (length L)) (fun x => \[is_head L x])).
Proof.
  destruct uncons_tree_spec as (uncons_tree_cost & cost_nonneg & ? & ? & ?).
  xcfO (fun n => 1 + uncons_tree_cost n).
  xcf. introv Rts NE.
  xpay. csimpl~.
  xapp~ as [t l]. csimpl~. intros (?&?&B&I&?).
  xmatch.
  - xret. hsimpl. invert B; intros x' ? ?. subst. rew_list~ in *.
  - { xfail. inverts B as.
      - applys~ C.
      - introv B1 B2. forwards: (btree_size_pos B1). math. }
Qed.

Lemma tail_spec :
  SpecO Z.log2 (fun F =>
    Spec tail (l: rlist a) |R>>
      forall L, Rlist l L -> l <> nil ->
      R (\$F (length L)) (fun tl => \[exists TL, Rlist tl TL -> is_tail L TL])).
Proof.
  destruct uncons_tree_spec as (uncons_tree_cost & cost_nonneg & ? & ? & ?).
  xcfO (fun n => 1 + uncons_tree_cost n). 
  xcf. introv Rts NE.
  xpay. csimpl~.
  xgo~. csimpl~. xextract as (? & ? & BT & ? & ?).
  hsimpl. subst. exists___.
  inverts BT.
  - rew_list~ in *.
  - forwards~: btree_size_pos. math.
Qed.

(* private *)
Lemma lookup_tree_spec :
  SpecO (fun n => n) (fun F =>
    Spec lookup_tree (i: int) (t: tree a) |R>>
      forall p L, btree p t L -> ZInbound i L ->
      R (\$F p) (fun x => \[ZNth i L x])).
Proof.
  applys @SpecO_of_SpecO_after 0.
  xcfO (fun n => n+1). 
  intros F' eqF'.
  xinduction (fun (i:int) t => tree_size t).
  xcf. intros i t. introv IH Rt Bi. forwards~: btree_p_pos. rewrites~ eqF'.
  inverts Rt as.
  { xpay. csimpl.
    xmatch. xgo. hsimpl~. apply~ ZInbound_one_pos_inv. }
  { introv Bt1 Bt2. intros.
    xpay. csimpl. xmatch. xapps~. forwards~: btree_p_pos.
    xif; subst; repeat xapps~; try rewrites~ eqF'; rewrites~ pow2_succ_div in *.
    - apply~ ZInbound_app_l_inv; rewrite~ (length_correct Bt1).
    - csimpl.
    - hsimpl; apply~ ZNth_app_l.
    - apply~ ZInbound_app_r_inv; rewrite~ (length_correct Bt1).
    - csimpl.
    - hsimpl; apply~ ZNth_app_r; rewrite~ (length_correct Bt1). }
Qed.

(* private *)
Lemma update_tree_spec :
  SpecO (fun n => n) (fun F =>
    Spec update_tree (i: int) (x: a) (t: tree a) |R>>
      forall p L, btree p t L -> ZInbound i L ->
      R (\$F p) (fun t' => \[exists L', btree p t' L' /\ ZUpdate i x L L'])).
Proof.
  applys @SpecO_of_SpecO_after 0.
  xcfO (fun n => n+1). 
  intros F' eqF'.
  xinduction (fun (i:int) (x:a) t => tree_size t).
  xcf. intros i x t. introv IH Rt Bi. forwards~: btree_p_pos. rewrites~ eqF'.
  xpay. csimpl.
  inverts Rt as.
  { xmatch. xgo. hsimpl~. apply~ ZInbound_one_pos_inv. }
  { introv Bt1 Bt2. intros. xmatch. forwards~: btree_p_pos.
    xapps~. xif; subst; repeat xapps~; try rewrites~ eqF'; rewrites~ pow2_succ_div in *.
    - apply~ ZInbound_app_l_inv. rewrite~ (length_correct Bt1).
    - csimpl.
    - intros (L'&?&?). xret. hsimpl. exists___. split. constructors~. apply~ ZUpdate_app_l.
    - apply~ ZInbound_app_r_inv. rewrite~ (length_correct Bt1).
    - csimpl.
    - intros (L'&?&?). xret. hsimpl. exists___. split. constructors~. apply~ ZUpdate_app_r.
      rewrite~ (length_correct Bt1). }
Qed.

Definition lookup_update_cost
           (size_cost : unit -> Z)
           (op_cost: Z -> Z): Z * Z -> Z :=
  fun mn =>
    match mn with
      (p, len_ts) =>
      1 +
      ((len_ts * (2 * size_cost tt + 1)) +
       (op_cost (p + len_ts)))
    end.

Lemma lookup_update_cost_dom :
  forall x0: Z,
  forall size_cost : unit -> Z,
  forall op_cost : Z -> Z,
  (0 <= x0) ->
  (forall x, 0 <= size_cost x) ->
  (forall x, 0 <= op_cost x) ->
  monotonic le le op_cost ->
  idominated _ _ op_cost (fun n => n) ->
  idominated _ _
    (FO := fo_towards_infinity_xZ x0)
    (fun mn => lookup_update_cost size_cost op_cost (proj1_sig mn))
    (fun mn => let '(m, n) := proj1_sig mn in m + n).
Proof.
  introv ? size_cost_nonneg op_cost_nonneg op_cost_mon op_cost_dom.
  specialize (size_cost_nonneg tt).
  unfold lookup_update_cost.

  idominated_Z_auto.
  idominated_binary_Z_sum_split.
  eapply idominated_sum_split_Z.
  - { idominated_Z_auto~.
      eapply idominated_of_dominated.
      - monotonic_Z_auto. eapply filter_towards_infinity_xZ.
        apply monotonic_fixed_fst_proj. simpl. monotonic_Z_auto.
      - apply subrelation_norm_le_dominated. intros [[? ?] ?].
        simpl in *. math_nia. }
  - sets id : (fun n: int => n).
    sets h : (fun mn: FixedFst x0 => let '(m, n) := proj1_sig mn in m + n).
    rewrites_extens h (fun n => id (h n)).
    rewrites_extens (fun a: FixedFst x0 => let '(m,n) := proj1_sig a in op_cost (m + n))
                    (fun a: FixedFst x0 => op_cost (h a)).
    (* what's going on here? I'm unable to apply idominated_comp
    without doing all this stuff, why? *)
    eapply (idominated_comp (A := FixedFst x0)); eauto.
    subst id; monotonic_Z_auto.
    subst h. monotonic_Z_auto. apply filter_towards_infinity_xZ.
    apply monotonic_fixed_fst_proj. simpl. monotonic_Z_auto.
Qed.

Lemma lookup_spec_ind :
  exists (F: Z * Z -> Z),
  (forall m n, 0 <= m -> 0 <= n -> 0 <= F (m, n)) /\
  (forall (p: Z),
    0 <= p ->
    monotonic (fixed_fst_le le p) le (fun p => F (proj1_sig p)) /\
    idominated (FO := fo_towards_infinity_xZ p) _ _
               (fun p => F (proj1_sig p))
               (fun p => let '(m, n) := proj1_sig p in m + n)) /\
  Spec lookup (i:int) (ts: rlist a) |R>>
    forall p L, inv p ts L -> ZInbound i L ->
    R (\$F (p, length ts)) (fun x => \[ZNth i L x]).
Proof.
  destruct size_spec as (size_cost & size_cost_nonneg & ? & ?).
  destruct lookup_tree_spec
    as (lookup_tree_cost &
        lookup_tree_cost_nonneg &
        lookup_tree_cost_mon &
        lookup_tree_cost_dom & ?).
  exists (@lookup_update_cost size_cost lookup_tree_cost). splits.
  - intros. unfold lookup_update_cost. simpl_nonneg~.
  - { intros p p_pos. splits.
      - unfold lookup_update_cost.
        apply monotonic_fixed_fst_proj. simpl. monotonic_Z_auto.
        simpl_nonneg~. applys @monotonic_comp lookup_tree_cost_mon. monotonic_Z_auto.
      - applys~ lookup_update_cost_dom. }
  - { specialize (size_cost_nonneg tt). unfold lookup_update_cost.
      xinduction (fun (i:int) (ts:rlist a) => LibList.length ts).
      xcf. intros i ts. introv IH Rts Bi.
      xpay. { csimpl. simpl_nonneg~. }
      xmatch; inverts Rts.
      - xgo. apply~ ZInbound_nil_inv.
      - xapps~. csimpl. rew_length. math_rewrite ((p+1) + length ts0 = p + (1 + length ts0)).
        math_nia. simpl_nonneg~.
      - unpack. forwards~: (@length_correct t). forwards~: (@btree_size_correct p t).
        xapps~.
        { csimpl. rew_length. apply group_ineq. ring_simplify. simpl_nonneg~. }
        xif.
        xapps~.
          { subst. apply~ ZInbound_app_l_inv. }
          { csimpl. rew_length.
            forwards~: lookup_tree_cost_mon p (p + (1 + length ts0)). math_nia. eauto. }
          { hsimpl. subst. apply~ ZNth_app_l. }
        xapps~.
          { csimpl. rew_length.
            forwards~: lookup_tree_cost_nonneg (p + (1 + length ts0)). math_nia. }
        subst. xapps~.
          { apply~ ZInbound_app_r_inv. }
          { csimpl. rew_length.
            math_rewrite ((p + 1) + length ts0 = p + (1 + length ts0)).
            math_nia. simpl_nonneg~. }
          { hsimpl. apply~ ZNth_app_r. } }
Qed.

Lemma lookup_spec :
  SpecO Z.log2 (fun F =>
    Spec lookup (i: int) (ts: rlist a) |R>>
      forall L, Rlist ts L -> ZInbound i L ->
      R (\$F (length L)) (fun x => \[ZNth i L x])).
Proof.
  destruct lookup_spec_ind as (lookup_spec_cost &
                               lookup_spec_cost_nonneg &
                               lookup_spec_mon_dom_p &
                               spec).
  forwards~ [lookup_spec_mon lookup_spec_dom]: lookup_spec_mon_dom_p 0.
  forwards lookup_spec_mon': proj1 @monotonic_fixed_fst_proj lookup_spec_mon.
  forwards lookup_spec_dom': @idominated_xZ_proj lookup_spec_dom.
  simpl in lookup_spec_mon', lookup_spec_dom'.
  xcfO (fun n => lookup_spec_cost (0, Z.log2 (2 * n + 1))). 
  - intros. applys~ lookup_spec_cost_nonneg. applys Z.log2_nonneg.
  - applys~ @monotonic_comp (fun n => lookup_spec_cost (0, n)). monotonic_Z_auto~.
  - eapply idominated_transitive.
    applys~ @idominated_comp lookup_spec_dom'; monotonic_Z_auto~.
    idominated_Z_auto~.
  - xweaken spec. intros ? ts. introv W H Rts Bi. unfold Rlist in Rts.
    xgc. xapply~ H.
    { csimpl. applys lookup_spec_mon'. applys~ ts_bound_log. simpl_nonneg~. }
    { csimpl. eauto. }
Qed.

Lemma update_spec_ind :
  exists (F: Z * Z -> Z),
  (forall m n, 0 <= m -> 0 <= n -> 0 <= F (m, n)) /\
  (forall (p: Z),
    0 <= p ->
    monotonic (fixed_fst_le le p) le (fun p => F (proj1_sig p)) /\
    idominated (FO := fo_towards_infinity_xZ p) _ _
               (fun p => F (proj1_sig p))
               (fun p => let '(m, n) := proj1_sig p in m + n)) /\
    Spec update (i: int) (x: a) (ts: rlist a) |R>>
      forall p L, inv p ts L -> ZInbound i L ->
      R (\$F (p, length ts)) (fun ts' => \[exists L', inv p ts' L' /\ ZUpdate i x L L']).
Proof.
  destruct size_spec as (size_cost & size_cost_nonneg & ? & ?).
  destruct update_tree_spec
    as (update_tree_cost &
        update_tree_cost_nonneg &
        update_tree_cost_mon &
        update_tree_cost_dom & ?).
  exists (@lookup_update_cost size_cost update_tree_cost). splits.
  - intros. unfold lookup_update_cost. simpl_nonneg~.
  - { intros p p_pos. splits.
      - unfold lookup_update_cost.
        apply monotonic_fixed_fst_proj. simpl. monotonic_Z_auto.
        simpl_nonneg~. applys @monotonic_comp update_tree_cost_mon. monotonic_Z_auto.
      - applys~ lookup_update_cost_dom. }
  - { specialize (size_cost_nonneg tt). unfold lookup_update_cost.
      xinduction (fun (i:int) (x:a) (ts:rlist a) => LibList.length ts).
      xcf. intros i x ts. introv IH Rts Bi.
      xpay. { unfold lookup_update_cost. csimpl. simpl_nonneg~. }
      xmatch; inverts Rts.
      - xgo. apply~ ZInbound_nil_inv.
      - xgo~.
        { csimpl. rew_length.
          math_rewrite ((p + 1) + length ts0 = p + (1 + length ts0)). math_nia.
          simpl_nonneg~. }
        xextract as HE. unpack HE. hsimpl. exists___. split~.
      - unpack. forwards~: (@length_correct t).
        forwards~: (@btree_size_correct p t). xapps~.
        { csimpl. rew_length.
          forwards~: update_tree_cost_nonneg (p + (1 + length ts0)). math_nia. }
        xif. xapps~. subst. apply~ ZInbound_app_l_inv.
        { csimpl. rew_length.
          forwards~: update_tree_cost_mon p (p + (1 + length ts0)). math_nia. simpl_nonneg~. }
        intros (?&?&?). xret. hsimpl. subst. exists___. split; eauto. apply~ ZUpdate_app_l.
        xapps~.
        { csimpl. rew_length.
          forwards~: update_tree_cost_nonneg (p + (1 + length ts0)). math_nia. }
        xapps~. subst. apply~ ZInbound_app_r_inv.
        { csimpl. rew_length.
          math_rewrite ((p + 1) + length ts0 = p + (1 + length ts0)). math_nia. simpl_nonneg~. }
        intros (?&?&?). xret. hsimpl. subst. exists___. split; eauto. apply~ ZUpdate_app_r. }
Qed.

Lemma update_spec :
  SpecO Z.log2 (fun F =>
    Spec update (i: int) (x: a) (ts: rlist a) |R>>
      forall L, Rlist ts L -> ZInbound i L ->
      R (\$ F (length L)) (fun l' => \[exists L', Rlist l' L' -> ZUpdate i x L L'])).
Proof.
  destruct update_spec_ind as (update_spec_cost &
                               update_spec_cost_nonneg &
                               update_spec_mon_dom &
                               spec).
  forwards~ [update_spec_mon update_spec_dom]: update_spec_mon_dom 0.
  forwards update_spec_mon': proj1 @monotonic_fixed_fst_proj update_spec_mon.
  forwards update_spec_dom': @idominated_xZ_proj update_spec_dom.
  simpl in update_spec_mon', update_spec_dom'.
  xcfO (fun n => update_spec_cost (0, Z.log2 (2 * n + 1))). 
  - intros. applys~ update_spec_cost_nonneg. applys Z.log2_nonneg.
  - applys~ @monotonic_comp (fun n => update_spec_cost (0, n)). monotonic_Z_auto~.
  - eapply idominated_transitive.
    applys~ @idominated_comp update_spec_dom'; monotonic_Z_auto~.
    idominated_Z_auto~.
  - xweaken spec. intros ? ? ts. introv W H Rts Bi. unfold Rlist in Rts.
    xgc. xapply~ H.
    { csimpl. applys~ update_spec_mon'. applys~ ts_bound_log. simpl_nonneg~. }
    xextract as HE. unpack HE. hsimpl. exists___~.
Qed.

End Polymorphic'.

End BinaryRandomAccessListSpec.
